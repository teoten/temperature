(ns temperature.app
  (:require [goog.string :as gstring]
            [goog.string.format]
            [reagent.core :as r]
            [reagent.dom  :as dom]))

;; Atoms
(defonce unit (r/atom "f"))
(defonce to-unit (r/atom "c"))
(defonce temp (r/atom 0))


;; Base funcs
(defn convert-temp [degrees from to]
  (let [deg (js/parseFloat degrees)]
    (cond (= from to) deg
          (and (= from "f") (= to "c")) (/ (- deg 32) 1.8)
          (and (= from "c") (= to "f")) (+ (* deg 1.8) 32)
          (and (= from "c") (= to "k")) (+ deg 273.15)
          (and (= from "k") (= to "c")) (- deg 273.15)
          (and (= from "k") (= to "f")) (+ (/ (* (- deg 273.15) 9) 5) 32)
          (and (= from "f") (= to "k")) (+ 273.15 (/ (* 5 (- deg 32)) 9))
          :else 0.0)))


;; UI
(defn temp-options [init-val f-on-change value]
  [:div.row
   [:div.custom-select
    [:select.form-select {:multiple nil
                          :name "duallistbox_demo"
                          :value init-val
                          :onChange f-on-change
                          :class "demo"}
     [:option {:value "c"} "c"]
     [:option {:value "k"} "k"]
     [:option {:value "f"} "f"]]]])



(defn temperature-in
  "Obtain the temperature from the user"
  [value]
  [:div.temp-control {:class "row align-items-start"}
   [:br]
   [:h3 "Convert from"]
   [:div.col-4 [:label {:for "temp"} "Temperature"]]
   [:div.col-6 [:input {:type "number"
                        :id "temp"
                        :value @temp
                        :name "temp"
                        :onChange #(reset! temp (.. % -target -value))}]]
   [:div.col-2 [temp-options @unit #(reset! unit (.. % -target -value))]]])

(defn results
  "Render the results"
  []
  [:div.converted-output {:class "row align-items-start"}
   [:br]
   [:h3 "Converted Value"]
   [:div.col-4 [:label {:for "temp"} "Result"]]
   [:div.col-6 [:span (gstring/format "%.2f" (convert-temp @temp @unit @to-unit))]]
   [:div.col-2 [:span [temp-options @to-unit #(reset! to-unit (.. % -target -value))]]]])

(defn btn-swap []
  (let [current-unit @unit]
    [:div {:class "row align-items-center"}
     [:button
      {:class "btn btn-secondary btn-sm"
       :onClick #(do
                   (reset! unit @to-unit)
                   (reset! to-unit current-unit))}
      \u21c4]]))

;; APP
(defn Application []
  [:div.row
   [:h1 "Temp converter"]
   [:div.col-5
    [temperature-in]]
   [:div.col-2
    [btn-swap]]
   [:div.col-5
    [results]]])

(dom/render [Application] (.-body js/document))


(defn init []
  (Application))

;; TODO
;; Add css
;; Add other units
