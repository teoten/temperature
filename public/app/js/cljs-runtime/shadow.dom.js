goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_25472 = (function (this$){
var x__5350__auto__ = (((this$ == null))?null:this$);
var m__5351__auto__ = (shadow.dom._to_dom[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5351__auto__.call(null,this$));
} else {
var m__5349__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5349__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_25472(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_25483 = (function (this$){
var x__5350__auto__ = (((this$ == null))?null:this$);
var m__5351__auto__ = (shadow.dom._to_svg[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5351__auto__.call(null,this$));
} else {
var m__5349__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5349__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_25483(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__24189 = coll;
var G__24190 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__24189,G__24190) : shadow.dom.lazy_native_coll_seq.call(null,G__24189,G__24190));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__5002__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__24234 = arguments.length;
switch (G__24234) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__24242 = arguments.length;
switch (G__24242) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__24249 = arguments.length;
switch (G__24249) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__24257 = arguments.length;
switch (G__24257) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__24267 = arguments.length;
switch (G__24267) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__24277 = arguments.length;
switch (G__24277) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__5002__auto__ = (!((typeof document !== 'undefined')));
if(or__5002__auto__){
return or__5002__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e24289){if((e24289 instanceof Object)){
var e = e24289;
return console.log("didnt support attachEvent",el,e);
} else {
throw e24289;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__5002__auto__ = (!((typeof document !== 'undefined')));
if(or__5002__auto__){
return or__5002__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__24308 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__24309 = null;
var count__24310 = (0);
var i__24311 = (0);
while(true){
if((i__24311 < count__24310)){
var el = chunk__24309.cljs$core$IIndexed$_nth$arity$2(null,i__24311);
var handler_25594__$1 = ((function (seq__24308,chunk__24309,count__24310,i__24311,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__24308,chunk__24309,count__24310,i__24311,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_25594__$1);


var G__25596 = seq__24308;
var G__25597 = chunk__24309;
var G__25598 = count__24310;
var G__25599 = (i__24311 + (1));
seq__24308 = G__25596;
chunk__24309 = G__25597;
count__24310 = G__25598;
i__24311 = G__25599;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24308);
if(temp__5804__auto__){
var seq__24308__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24308__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24308__$1);
var G__25601 = cljs.core.chunk_rest(seq__24308__$1);
var G__25602 = c__5525__auto__;
var G__25603 = cljs.core.count(c__5525__auto__);
var G__25604 = (0);
seq__24308 = G__25601;
chunk__24309 = G__25602;
count__24310 = G__25603;
i__24311 = G__25604;
continue;
} else {
var el = cljs.core.first(seq__24308__$1);
var handler_25605__$1 = ((function (seq__24308,chunk__24309,count__24310,i__24311,el,seq__24308__$1,temp__5804__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__24308,chunk__24309,count__24310,i__24311,el,seq__24308__$1,temp__5804__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_25605__$1);


var G__25606 = cljs.core.next(seq__24308__$1);
var G__25607 = null;
var G__25608 = (0);
var G__25609 = (0);
seq__24308 = G__25606;
chunk__24309 = G__25607;
count__24310 = G__25608;
i__24311 = G__25609;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__24341 = arguments.length;
switch (G__24341) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__24354 = cljs.core.seq(events);
var chunk__24355 = null;
var count__24356 = (0);
var i__24357 = (0);
while(true){
if((i__24357 < count__24356)){
var vec__24374 = chunk__24355.cljs$core$IIndexed$_nth$arity$2(null,i__24357);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24374,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24374,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__25615 = seq__24354;
var G__25616 = chunk__24355;
var G__25617 = count__24356;
var G__25618 = (i__24357 + (1));
seq__24354 = G__25615;
chunk__24355 = G__25616;
count__24356 = G__25617;
i__24357 = G__25618;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24354);
if(temp__5804__auto__){
var seq__24354__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24354__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24354__$1);
var G__25619 = cljs.core.chunk_rest(seq__24354__$1);
var G__25620 = c__5525__auto__;
var G__25621 = cljs.core.count(c__5525__auto__);
var G__25622 = (0);
seq__24354 = G__25619;
chunk__24355 = G__25620;
count__24356 = G__25621;
i__24357 = G__25622;
continue;
} else {
var vec__24379 = cljs.core.first(seq__24354__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24379,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24379,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__25625 = cljs.core.next(seq__24354__$1);
var G__25626 = null;
var G__25627 = (0);
var G__25628 = (0);
seq__24354 = G__25625;
chunk__24355 = G__25626;
count__24356 = G__25627;
i__24357 = G__25628;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__24385 = cljs.core.seq(styles);
var chunk__24386 = null;
var count__24387 = (0);
var i__24388 = (0);
while(true){
if((i__24388 < count__24387)){
var vec__24403 = chunk__24386.cljs$core$IIndexed$_nth$arity$2(null,i__24388);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24403,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24403,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__25637 = seq__24385;
var G__25638 = chunk__24386;
var G__25639 = count__24387;
var G__25640 = (i__24388 + (1));
seq__24385 = G__25637;
chunk__24386 = G__25638;
count__24387 = G__25639;
i__24388 = G__25640;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24385);
if(temp__5804__auto__){
var seq__24385__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24385__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24385__$1);
var G__25646 = cljs.core.chunk_rest(seq__24385__$1);
var G__25647 = c__5525__auto__;
var G__25648 = cljs.core.count(c__5525__auto__);
var G__25649 = (0);
seq__24385 = G__25646;
chunk__24386 = G__25647;
count__24387 = G__25648;
i__24388 = G__25649;
continue;
} else {
var vec__24409 = cljs.core.first(seq__24385__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24409,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24409,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__25650 = cljs.core.next(seq__24385__$1);
var G__25651 = null;
var G__25652 = (0);
var G__25653 = (0);
seq__24385 = G__25650;
chunk__24386 = G__25651;
count__24387 = G__25652;
i__24388 = G__25653;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__24416_25654 = key;
var G__24416_25655__$1 = (((G__24416_25654 instanceof cljs.core.Keyword))?G__24416_25654.fqn:null);
switch (G__24416_25655__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_25657 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__5002__auto__ = goog.string.startsWith(ks_25657,"data-");
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return goog.string.startsWith(ks_25657,"aria-");
}
})())){
el.setAttribute(ks_25657,value);
} else {
(el[ks_25657] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__24462){
var map__24463 = p__24462;
var map__24463__$1 = cljs.core.__destructure_map(map__24463);
var props = map__24463__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__24463__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__24465 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24465,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24465,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24465,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__24468 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__24468,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__24468;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__24472 = arguments.length;
switch (G__24472) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__24480){
var vec__24483 = p__24480;
var seq__24484 = cljs.core.seq(vec__24483);
var first__24485 = cljs.core.first(seq__24484);
var seq__24484__$1 = cljs.core.next(seq__24484);
var nn = first__24485;
var first__24485__$1 = cljs.core.first(seq__24484__$1);
var seq__24484__$2 = cljs.core.next(seq__24484__$1);
var np = first__24485__$1;
var nc = seq__24484__$2;
var node = vec__24483;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__24488 = nn;
var G__24489 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__24488,G__24489) : create_fn.call(null,G__24488,G__24489));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__24491 = nn;
var G__24492 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__24491,G__24492) : create_fn.call(null,G__24491,G__24492));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__24495 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24495,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24495,(1),null);
var seq__24499_25668 = cljs.core.seq(node_children);
var chunk__24500_25669 = null;
var count__24501_25670 = (0);
var i__24502_25671 = (0);
while(true){
if((i__24502_25671 < count__24501_25670)){
var child_struct_25672 = chunk__24500_25669.cljs$core$IIndexed$_nth$arity$2(null,i__24502_25671);
var children_25673 = shadow.dom.dom_node(child_struct_25672);
if(cljs.core.seq_QMARK_(children_25673)){
var seq__24547_25674 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_25673));
var chunk__24549_25675 = null;
var count__24550_25676 = (0);
var i__24551_25677 = (0);
while(true){
if((i__24551_25677 < count__24550_25676)){
var child_25678 = chunk__24549_25675.cljs$core$IIndexed$_nth$arity$2(null,i__24551_25677);
if(cljs.core.truth_(child_25678)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25678);


var G__25679 = seq__24547_25674;
var G__25680 = chunk__24549_25675;
var G__25681 = count__24550_25676;
var G__25682 = (i__24551_25677 + (1));
seq__24547_25674 = G__25679;
chunk__24549_25675 = G__25680;
count__24550_25676 = G__25681;
i__24551_25677 = G__25682;
continue;
} else {
var G__25683 = seq__24547_25674;
var G__25684 = chunk__24549_25675;
var G__25685 = count__24550_25676;
var G__25686 = (i__24551_25677 + (1));
seq__24547_25674 = G__25683;
chunk__24549_25675 = G__25684;
count__24550_25676 = G__25685;
i__24551_25677 = G__25686;
continue;
}
} else {
var temp__5804__auto___25689 = cljs.core.seq(seq__24547_25674);
if(temp__5804__auto___25689){
var seq__24547_25691__$1 = temp__5804__auto___25689;
if(cljs.core.chunked_seq_QMARK_(seq__24547_25691__$1)){
var c__5525__auto___25692 = cljs.core.chunk_first(seq__24547_25691__$1);
var G__25693 = cljs.core.chunk_rest(seq__24547_25691__$1);
var G__25694 = c__5525__auto___25692;
var G__25695 = cljs.core.count(c__5525__auto___25692);
var G__25696 = (0);
seq__24547_25674 = G__25693;
chunk__24549_25675 = G__25694;
count__24550_25676 = G__25695;
i__24551_25677 = G__25696;
continue;
} else {
var child_25697 = cljs.core.first(seq__24547_25691__$1);
if(cljs.core.truth_(child_25697)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25697);


var G__25700 = cljs.core.next(seq__24547_25691__$1);
var G__25701 = null;
var G__25702 = (0);
var G__25703 = (0);
seq__24547_25674 = G__25700;
chunk__24549_25675 = G__25701;
count__24550_25676 = G__25702;
i__24551_25677 = G__25703;
continue;
} else {
var G__25704 = cljs.core.next(seq__24547_25691__$1);
var G__25705 = null;
var G__25706 = (0);
var G__25707 = (0);
seq__24547_25674 = G__25704;
chunk__24549_25675 = G__25705;
count__24550_25676 = G__25706;
i__24551_25677 = G__25707;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_25673);
}


var G__25708 = seq__24499_25668;
var G__25709 = chunk__24500_25669;
var G__25710 = count__24501_25670;
var G__25711 = (i__24502_25671 + (1));
seq__24499_25668 = G__25708;
chunk__24500_25669 = G__25709;
count__24501_25670 = G__25710;
i__24502_25671 = G__25711;
continue;
} else {
var temp__5804__auto___25712 = cljs.core.seq(seq__24499_25668);
if(temp__5804__auto___25712){
var seq__24499_25713__$1 = temp__5804__auto___25712;
if(cljs.core.chunked_seq_QMARK_(seq__24499_25713__$1)){
var c__5525__auto___25714 = cljs.core.chunk_first(seq__24499_25713__$1);
var G__25715 = cljs.core.chunk_rest(seq__24499_25713__$1);
var G__25716 = c__5525__auto___25714;
var G__25717 = cljs.core.count(c__5525__auto___25714);
var G__25718 = (0);
seq__24499_25668 = G__25715;
chunk__24500_25669 = G__25716;
count__24501_25670 = G__25717;
i__24502_25671 = G__25718;
continue;
} else {
var child_struct_25721 = cljs.core.first(seq__24499_25713__$1);
var children_25722 = shadow.dom.dom_node(child_struct_25721);
if(cljs.core.seq_QMARK_(children_25722)){
var seq__24582_25723 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_25722));
var chunk__24584_25724 = null;
var count__24585_25725 = (0);
var i__24586_25726 = (0);
while(true){
if((i__24586_25726 < count__24585_25725)){
var child_25727 = chunk__24584_25724.cljs$core$IIndexed$_nth$arity$2(null,i__24586_25726);
if(cljs.core.truth_(child_25727)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25727);


var G__25729 = seq__24582_25723;
var G__25730 = chunk__24584_25724;
var G__25731 = count__24585_25725;
var G__25732 = (i__24586_25726 + (1));
seq__24582_25723 = G__25729;
chunk__24584_25724 = G__25730;
count__24585_25725 = G__25731;
i__24586_25726 = G__25732;
continue;
} else {
var G__25734 = seq__24582_25723;
var G__25735 = chunk__24584_25724;
var G__25736 = count__24585_25725;
var G__25737 = (i__24586_25726 + (1));
seq__24582_25723 = G__25734;
chunk__24584_25724 = G__25735;
count__24585_25725 = G__25736;
i__24586_25726 = G__25737;
continue;
}
} else {
var temp__5804__auto___25738__$1 = cljs.core.seq(seq__24582_25723);
if(temp__5804__auto___25738__$1){
var seq__24582_25739__$1 = temp__5804__auto___25738__$1;
if(cljs.core.chunked_seq_QMARK_(seq__24582_25739__$1)){
var c__5525__auto___25740 = cljs.core.chunk_first(seq__24582_25739__$1);
var G__25741 = cljs.core.chunk_rest(seq__24582_25739__$1);
var G__25742 = c__5525__auto___25740;
var G__25743 = cljs.core.count(c__5525__auto___25740);
var G__25744 = (0);
seq__24582_25723 = G__25741;
chunk__24584_25724 = G__25742;
count__24585_25725 = G__25743;
i__24586_25726 = G__25744;
continue;
} else {
var child_25746 = cljs.core.first(seq__24582_25739__$1);
if(cljs.core.truth_(child_25746)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_25746);


var G__25748 = cljs.core.next(seq__24582_25739__$1);
var G__25749 = null;
var G__25750 = (0);
var G__25751 = (0);
seq__24582_25723 = G__25748;
chunk__24584_25724 = G__25749;
count__24585_25725 = G__25750;
i__24586_25726 = G__25751;
continue;
} else {
var G__25752 = cljs.core.next(seq__24582_25739__$1);
var G__25753 = null;
var G__25754 = (0);
var G__25755 = (0);
seq__24582_25723 = G__25752;
chunk__24584_25724 = G__25753;
count__24585_25725 = G__25754;
i__24586_25726 = G__25755;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_25722);
}


var G__25757 = cljs.core.next(seq__24499_25713__$1);
var G__25758 = null;
var G__25759 = (0);
var G__25760 = (0);
seq__24499_25668 = G__25757;
chunk__24500_25669 = G__25758;
count__24501_25670 = G__25759;
i__24502_25671 = G__25760;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__24632 = cljs.core.seq(node);
var chunk__24633 = null;
var count__24634 = (0);
var i__24635 = (0);
while(true){
if((i__24635 < count__24634)){
var n = chunk__24633.cljs$core$IIndexed$_nth$arity$2(null,i__24635);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__25765 = seq__24632;
var G__25766 = chunk__24633;
var G__25767 = count__24634;
var G__25768 = (i__24635 + (1));
seq__24632 = G__25765;
chunk__24633 = G__25766;
count__24634 = G__25767;
i__24635 = G__25768;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24632);
if(temp__5804__auto__){
var seq__24632__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24632__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24632__$1);
var G__25772 = cljs.core.chunk_rest(seq__24632__$1);
var G__25773 = c__5525__auto__;
var G__25774 = cljs.core.count(c__5525__auto__);
var G__25775 = (0);
seq__24632 = G__25772;
chunk__24633 = G__25773;
count__24634 = G__25774;
i__24635 = G__25775;
continue;
} else {
var n = cljs.core.first(seq__24632__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__25776 = cljs.core.next(seq__24632__$1);
var G__25777 = null;
var G__25778 = (0);
var G__25779 = (0);
seq__24632 = G__25776;
chunk__24633 = G__25777;
count__24634 = G__25778;
i__24635 = G__25779;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__24644 = arguments.length;
switch (G__24644) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__24658 = arguments.length;
switch (G__24658) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__24691 = arguments.length;
switch (G__24691) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__5002__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__5732__auto__ = [];
var len__5726__auto___25792 = arguments.length;
var i__5727__auto___25793 = (0);
while(true){
if((i__5727__auto___25793 < len__5726__auto___25792)){
args__5732__auto__.push((arguments[i__5727__auto___25793]));

var G__25794 = (i__5727__auto___25793 + (1));
i__5727__auto___25793 = G__25794;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__24737_25796 = cljs.core.seq(nodes);
var chunk__24738_25797 = null;
var count__24739_25798 = (0);
var i__24740_25799 = (0);
while(true){
if((i__24740_25799 < count__24739_25798)){
var node_25800 = chunk__24738_25797.cljs$core$IIndexed$_nth$arity$2(null,i__24740_25799);
fragment.appendChild(shadow.dom._to_dom(node_25800));


var G__25801 = seq__24737_25796;
var G__25802 = chunk__24738_25797;
var G__25803 = count__24739_25798;
var G__25804 = (i__24740_25799 + (1));
seq__24737_25796 = G__25801;
chunk__24738_25797 = G__25802;
count__24739_25798 = G__25803;
i__24740_25799 = G__25804;
continue;
} else {
var temp__5804__auto___25805 = cljs.core.seq(seq__24737_25796);
if(temp__5804__auto___25805){
var seq__24737_25806__$1 = temp__5804__auto___25805;
if(cljs.core.chunked_seq_QMARK_(seq__24737_25806__$1)){
var c__5525__auto___25807 = cljs.core.chunk_first(seq__24737_25806__$1);
var G__25808 = cljs.core.chunk_rest(seq__24737_25806__$1);
var G__25809 = c__5525__auto___25807;
var G__25810 = cljs.core.count(c__5525__auto___25807);
var G__25811 = (0);
seq__24737_25796 = G__25808;
chunk__24738_25797 = G__25809;
count__24739_25798 = G__25810;
i__24740_25799 = G__25811;
continue;
} else {
var node_25812 = cljs.core.first(seq__24737_25806__$1);
fragment.appendChild(shadow.dom._to_dom(node_25812));


var G__25814 = cljs.core.next(seq__24737_25806__$1);
var G__25815 = null;
var G__25816 = (0);
var G__25817 = (0);
seq__24737_25796 = G__25814;
chunk__24738_25797 = G__25815;
count__24739_25798 = G__25816;
i__24740_25799 = G__25817;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq24716){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq24716));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__24762_25821 = cljs.core.seq(scripts);
var chunk__24763_25822 = null;
var count__24764_25823 = (0);
var i__24765_25824 = (0);
while(true){
if((i__24765_25824 < count__24764_25823)){
var vec__24784_25825 = chunk__24763_25822.cljs$core$IIndexed$_nth$arity$2(null,i__24765_25824);
var script_tag_25826 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24784_25825,(0),null);
var script_body_25827 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24784_25825,(1),null);
eval(script_body_25827);


var G__25829 = seq__24762_25821;
var G__25830 = chunk__24763_25822;
var G__25831 = count__24764_25823;
var G__25832 = (i__24765_25824 + (1));
seq__24762_25821 = G__25829;
chunk__24763_25822 = G__25830;
count__24764_25823 = G__25831;
i__24765_25824 = G__25832;
continue;
} else {
var temp__5804__auto___25833 = cljs.core.seq(seq__24762_25821);
if(temp__5804__auto___25833){
var seq__24762_25834__$1 = temp__5804__auto___25833;
if(cljs.core.chunked_seq_QMARK_(seq__24762_25834__$1)){
var c__5525__auto___25835 = cljs.core.chunk_first(seq__24762_25834__$1);
var G__25836 = cljs.core.chunk_rest(seq__24762_25834__$1);
var G__25837 = c__5525__auto___25835;
var G__25838 = cljs.core.count(c__5525__auto___25835);
var G__25839 = (0);
seq__24762_25821 = G__25836;
chunk__24763_25822 = G__25837;
count__24764_25823 = G__25838;
i__24765_25824 = G__25839;
continue;
} else {
var vec__24789_25840 = cljs.core.first(seq__24762_25834__$1);
var script_tag_25841 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24789_25840,(0),null);
var script_body_25842 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24789_25840,(1),null);
eval(script_body_25842);


var G__25843 = cljs.core.next(seq__24762_25834__$1);
var G__25844 = null;
var G__25845 = (0);
var G__25846 = (0);
seq__24762_25821 = G__25843;
chunk__24763_25822 = G__25844;
count__24764_25823 = G__25845;
i__24765_25824 = G__25846;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__24794){
var vec__24795 = p__24794;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24795,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24795,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__24839 = arguments.length;
switch (G__24839) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__24873 = cljs.core.seq(style_keys);
var chunk__24874 = null;
var count__24875 = (0);
var i__24876 = (0);
while(true){
if((i__24876 < count__24875)){
var it = chunk__24874.cljs$core$IIndexed$_nth$arity$2(null,i__24876);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__25856 = seq__24873;
var G__25857 = chunk__24874;
var G__25858 = count__24875;
var G__25859 = (i__24876 + (1));
seq__24873 = G__25856;
chunk__24874 = G__25857;
count__24875 = G__25858;
i__24876 = G__25859;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__24873);
if(temp__5804__auto__){
var seq__24873__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__24873__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__24873__$1);
var G__25860 = cljs.core.chunk_rest(seq__24873__$1);
var G__25861 = c__5525__auto__;
var G__25862 = cljs.core.count(c__5525__auto__);
var G__25863 = (0);
seq__24873 = G__25860;
chunk__24874 = G__25861;
count__24875 = G__25862;
i__24876 = G__25863;
continue;
} else {
var it = cljs.core.first(seq__24873__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__25864 = cljs.core.next(seq__24873__$1);
var G__25865 = null;
var G__25866 = (0);
var G__25867 = (0);
seq__24873 = G__25864;
chunk__24874 = G__25865;
count__24875 = G__25866;
i__24876 = G__25867;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5300__auto__,k__5301__auto__){
var self__ = this;
var this__5300__auto____$1 = this;
return this__5300__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__5301__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5302__auto__,k24902,else__5303__auto__){
var self__ = this;
var this__5302__auto____$1 = this;
var G__24924 = k24902;
var G__24924__$1 = (((G__24924 instanceof cljs.core.Keyword))?G__24924.fqn:null);
switch (G__24924__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k24902,else__5303__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5320__auto__,f__5321__auto__,init__5322__auto__){
var self__ = this;
var this__5320__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5323__auto__,p__24928){
var vec__24929 = p__24928;
var k__5324__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24929,(0),null);
var v__5325__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__24929,(1),null);
return (f__5321__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5321__auto__.cljs$core$IFn$_invoke$arity$3(ret__5323__auto__,k__5324__auto__,v__5325__auto__) : f__5321__auto__.call(null,ret__5323__auto__,k__5324__auto__,v__5325__auto__));
}),init__5322__auto__,this__5320__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5315__auto__,writer__5316__auto__,opts__5317__auto__){
var self__ = this;
var this__5315__auto____$1 = this;
var pr_pair__5318__auto__ = (function (keyval__5319__auto__){
return cljs.core.pr_sequential_writer(writer__5316__auto__,cljs.core.pr_writer,""," ","",opts__5317__auto__,keyval__5319__auto__);
});
return cljs.core.pr_sequential_writer(writer__5316__auto__,pr_pair__5318__auto__,"#shadow.dom.Coordinate{",", ","}",opts__5317__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__24901){
var self__ = this;
var G__24901__$1 = this;
return (new cljs.core.RecordIter((0),G__24901__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5298__auto__){
var self__ = this;
var this__5298__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5295__auto__){
var self__ = this;
var this__5295__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5304__auto__){
var self__ = this;
var this__5304__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5296__auto__){
var self__ = this;
var this__5296__auto____$1 = this;
var h__5111__auto__ = self__.__hash;
if((!((h__5111__auto__ == null)))){
return h__5111__auto__;
} else {
var h__5111__auto____$1 = (function (coll__5297__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__5297__auto__));
})(this__5296__auto____$1);
(self__.__hash = h__5111__auto____$1);

return h__5111__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this24903,other24904){
var self__ = this;
var this24903__$1 = this;
return (((!((other24904 == null)))) && ((((this24903__$1.constructor === other24904.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24903__$1.x,other24904.x)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24903__$1.y,other24904.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24903__$1.__extmap,other24904.__extmap)))))))));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5310__auto__,k__5311__auto__){
var self__ = this;
var this__5310__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__5311__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5310__auto____$1),self__.__meta),k__5311__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5311__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5307__auto__,k24902){
var self__ = this;
var this__5307__auto____$1 = this;
var G__24953 = k24902;
var G__24953__$1 = (((G__24953 instanceof cljs.core.Keyword))?G__24953.fqn:null);
switch (G__24953__$1) {
case "x":
case "y":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k24902);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5308__auto__,k__5309__auto__,G__24901){
var self__ = this;
var this__5308__auto____$1 = this;
var pred__24958 = cljs.core.keyword_identical_QMARK_;
var expr__24959 = k__5309__auto__;
if(cljs.core.truth_((pred__24958.cljs$core$IFn$_invoke$arity$2 ? pred__24958.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__24959) : pred__24958.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__24959)))){
return (new shadow.dom.Coordinate(G__24901,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__24958.cljs$core$IFn$_invoke$arity$2 ? pred__24958.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__24959) : pred__24958.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__24959)))){
return (new shadow.dom.Coordinate(self__.x,G__24901,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5309__auto__,G__24901),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5313__auto__){
var self__ = this;
var this__5313__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5299__auto__,G__24901){
var self__ = this;
var this__5299__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__24901,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5305__auto__,entry__5306__auto__){
var self__ = this;
var this__5305__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5306__auto__)){
return this__5305__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__5306__auto__,(0)),cljs.core._nth(entry__5306__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5305__auto____$1,entry__5306__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__5346__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__5346__auto__,writer__5347__auto__){
return cljs.core._write(writer__5347__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__24906){
var extmap__5342__auto__ = (function (){var G__24977 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__24906,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__24906)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__24977);
} else {
return G__24977;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__24906),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__24906),null,cljs.core.not_empty(extmap__5342__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5300__auto__,k__5301__auto__){
var self__ = this;
var this__5300__auto____$1 = this;
return this__5300__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__5301__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5302__auto__,k24998,else__5303__auto__){
var self__ = this;
var this__5302__auto____$1 = this;
var G__25010 = k24998;
var G__25010__$1 = (((G__25010 instanceof cljs.core.Keyword))?G__25010.fqn:null);
switch (G__25010__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k24998,else__5303__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5320__auto__,f__5321__auto__,init__5322__auto__){
var self__ = this;
var this__5320__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5323__auto__,p__25016){
var vec__25017 = p__25016;
var k__5324__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25017,(0),null);
var v__5325__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25017,(1),null);
return (f__5321__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5321__auto__.cljs$core$IFn$_invoke$arity$3(ret__5323__auto__,k__5324__auto__,v__5325__auto__) : f__5321__auto__.call(null,ret__5323__auto__,k__5324__auto__,v__5325__auto__));
}),init__5322__auto__,this__5320__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5315__auto__,writer__5316__auto__,opts__5317__auto__){
var self__ = this;
var this__5315__auto____$1 = this;
var pr_pair__5318__auto__ = (function (keyval__5319__auto__){
return cljs.core.pr_sequential_writer(writer__5316__auto__,cljs.core.pr_writer,""," ","",opts__5317__auto__,keyval__5319__auto__);
});
return cljs.core.pr_sequential_writer(writer__5316__auto__,pr_pair__5318__auto__,"#shadow.dom.Size{",", ","}",opts__5317__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__24997){
var self__ = this;
var G__24997__$1 = this;
return (new cljs.core.RecordIter((0),G__24997__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5298__auto__){
var self__ = this;
var this__5298__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5295__auto__){
var self__ = this;
var this__5295__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5304__auto__){
var self__ = this;
var this__5304__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5296__auto__){
var self__ = this;
var this__5296__auto____$1 = this;
var h__5111__auto__ = self__.__hash;
if((!((h__5111__auto__ == null)))){
return h__5111__auto__;
} else {
var h__5111__auto____$1 = (function (coll__5297__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__5297__auto__));
})(this__5296__auto____$1);
(self__.__hash = h__5111__auto____$1);

return h__5111__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this24999,other25000){
var self__ = this;
var this24999__$1 = this;
return (((!((other25000 == null)))) && ((((this24999__$1.constructor === other25000.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24999__$1.w,other25000.w)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24999__$1.h,other25000.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this24999__$1.__extmap,other25000.__extmap)))))))));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5310__auto__,k__5311__auto__){
var self__ = this;
var this__5310__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__5311__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5310__auto____$1),self__.__meta),k__5311__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5311__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5307__auto__,k24998){
var self__ = this;
var this__5307__auto____$1 = this;
var G__25088 = k24998;
var G__25088__$1 = (((G__25088 instanceof cljs.core.Keyword))?G__25088.fqn:null);
switch (G__25088__$1) {
case "w":
case "h":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k24998);

}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5308__auto__,k__5309__auto__,G__24997){
var self__ = this;
var this__5308__auto____$1 = this;
var pred__25094 = cljs.core.keyword_identical_QMARK_;
var expr__25095 = k__5309__auto__;
if(cljs.core.truth_((pred__25094.cljs$core$IFn$_invoke$arity$2 ? pred__25094.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__25095) : pred__25094.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__25095)))){
return (new shadow.dom.Size(G__24997,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__25094.cljs$core$IFn$_invoke$arity$2 ? pred__25094.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__25095) : pred__25094.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__25095)))){
return (new shadow.dom.Size(self__.w,G__24997,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5309__auto__,G__24997),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5313__auto__){
var self__ = this;
var this__5313__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5299__auto__,G__24997){
var self__ = this;
var this__5299__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__24997,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5305__auto__,entry__5306__auto__){
var self__ = this;
var this__5305__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5306__auto__)){
return this__5305__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__5306__auto__,(0)),cljs.core._nth(entry__5306__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5305__auto____$1,entry__5306__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__5346__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__5346__auto__,writer__5347__auto__){
return cljs.core._write(writer__5347__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__25005){
var extmap__5342__auto__ = (function (){var G__25155 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__25005,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__25005)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__25155);
} else {
return G__25155;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__25005),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__25005),null,cljs.core.not_empty(extmap__5342__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__5590__auto__ = opts;
var l__5591__auto__ = a__5590__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__5591__auto__)){
var G__25907 = (i + (1));
var G__25908 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__25907;
ret = G__25908;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__25182){
var vec__25183 = p__25182;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25183,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25183,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__25194 = arguments.length;
switch (G__25194) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5802__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5802__auto__)){
var child = temp__5802__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__25916 = ps;
var G__25917 = (i + (1));
el__$1 = G__25916;
i = G__25917;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__25247 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25247,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25247,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25247,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__25252_25921 = cljs.core.seq(props);
var chunk__25253_25922 = null;
var count__25254_25923 = (0);
var i__25255_25924 = (0);
while(true){
if((i__25255_25924 < count__25254_25923)){
var vec__25272_25925 = chunk__25253_25922.cljs$core$IIndexed$_nth$arity$2(null,i__25255_25924);
var k_25926 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25272_25925,(0),null);
var v_25927 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25272_25925,(1),null);
el.setAttributeNS((function (){var temp__5804__auto__ = cljs.core.namespace(k_25926);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_25926),v_25927);


var G__25928 = seq__25252_25921;
var G__25929 = chunk__25253_25922;
var G__25930 = count__25254_25923;
var G__25931 = (i__25255_25924 + (1));
seq__25252_25921 = G__25928;
chunk__25253_25922 = G__25929;
count__25254_25923 = G__25930;
i__25255_25924 = G__25931;
continue;
} else {
var temp__5804__auto___25932 = cljs.core.seq(seq__25252_25921);
if(temp__5804__auto___25932){
var seq__25252_25933__$1 = temp__5804__auto___25932;
if(cljs.core.chunked_seq_QMARK_(seq__25252_25933__$1)){
var c__5525__auto___25935 = cljs.core.chunk_first(seq__25252_25933__$1);
var G__25936 = cljs.core.chunk_rest(seq__25252_25933__$1);
var G__25937 = c__5525__auto___25935;
var G__25938 = cljs.core.count(c__5525__auto___25935);
var G__25939 = (0);
seq__25252_25921 = G__25936;
chunk__25253_25922 = G__25937;
count__25254_25923 = G__25938;
i__25255_25924 = G__25939;
continue;
} else {
var vec__25275_25944 = cljs.core.first(seq__25252_25933__$1);
var k_25945 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25275_25944,(0),null);
var v_25946 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25275_25944,(1),null);
el.setAttributeNS((function (){var temp__5804__auto____$1 = cljs.core.namespace(k_25945);
if(cljs.core.truth_(temp__5804__auto____$1)){
var ns = temp__5804__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_25945),v_25946);


var G__25947 = cljs.core.next(seq__25252_25933__$1);
var G__25948 = null;
var G__25949 = (0);
var G__25950 = (0);
seq__25252_25921 = G__25947;
chunk__25253_25922 = G__25948;
count__25254_25923 = G__25949;
i__25255_25924 = G__25950;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__25280 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25280,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25280,(1),null);
var seq__25283_25951 = cljs.core.seq(node_children);
var chunk__25285_25952 = null;
var count__25286_25953 = (0);
var i__25287_25954 = (0);
while(true){
if((i__25287_25954 < count__25286_25953)){
var child_struct_25955 = chunk__25285_25952.cljs$core$IIndexed$_nth$arity$2(null,i__25287_25954);
if((!((child_struct_25955 == null)))){
if(typeof child_struct_25955 === 'string'){
var text_25956 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_25956),child_struct_25955].join(''));
} else {
var children_25957 = shadow.dom.svg_node(child_struct_25955);
if(cljs.core.seq_QMARK_(children_25957)){
var seq__25335_25958 = cljs.core.seq(children_25957);
var chunk__25338_25959 = null;
var count__25339_25960 = (0);
var i__25340_25961 = (0);
while(true){
if((i__25340_25961 < count__25339_25960)){
var child_25962 = chunk__25338_25959.cljs$core$IIndexed$_nth$arity$2(null,i__25340_25961);
if(cljs.core.truth_(child_25962)){
node.appendChild(child_25962);


var G__25963 = seq__25335_25958;
var G__25964 = chunk__25338_25959;
var G__25965 = count__25339_25960;
var G__25966 = (i__25340_25961 + (1));
seq__25335_25958 = G__25963;
chunk__25338_25959 = G__25964;
count__25339_25960 = G__25965;
i__25340_25961 = G__25966;
continue;
} else {
var G__25967 = seq__25335_25958;
var G__25968 = chunk__25338_25959;
var G__25969 = count__25339_25960;
var G__25970 = (i__25340_25961 + (1));
seq__25335_25958 = G__25967;
chunk__25338_25959 = G__25968;
count__25339_25960 = G__25969;
i__25340_25961 = G__25970;
continue;
}
} else {
var temp__5804__auto___25971 = cljs.core.seq(seq__25335_25958);
if(temp__5804__auto___25971){
var seq__25335_25972__$1 = temp__5804__auto___25971;
if(cljs.core.chunked_seq_QMARK_(seq__25335_25972__$1)){
var c__5525__auto___25973 = cljs.core.chunk_first(seq__25335_25972__$1);
var G__25974 = cljs.core.chunk_rest(seq__25335_25972__$1);
var G__25975 = c__5525__auto___25973;
var G__25976 = cljs.core.count(c__5525__auto___25973);
var G__25977 = (0);
seq__25335_25958 = G__25974;
chunk__25338_25959 = G__25975;
count__25339_25960 = G__25976;
i__25340_25961 = G__25977;
continue;
} else {
var child_25978 = cljs.core.first(seq__25335_25972__$1);
if(cljs.core.truth_(child_25978)){
node.appendChild(child_25978);


var G__25979 = cljs.core.next(seq__25335_25972__$1);
var G__25980 = null;
var G__25981 = (0);
var G__25982 = (0);
seq__25335_25958 = G__25979;
chunk__25338_25959 = G__25980;
count__25339_25960 = G__25981;
i__25340_25961 = G__25982;
continue;
} else {
var G__25983 = cljs.core.next(seq__25335_25972__$1);
var G__25984 = null;
var G__25985 = (0);
var G__25986 = (0);
seq__25335_25958 = G__25983;
chunk__25338_25959 = G__25984;
count__25339_25960 = G__25985;
i__25340_25961 = G__25986;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_25957);
}
}


var G__25987 = seq__25283_25951;
var G__25988 = chunk__25285_25952;
var G__25989 = count__25286_25953;
var G__25990 = (i__25287_25954 + (1));
seq__25283_25951 = G__25987;
chunk__25285_25952 = G__25988;
count__25286_25953 = G__25989;
i__25287_25954 = G__25990;
continue;
} else {
var G__25991 = seq__25283_25951;
var G__25992 = chunk__25285_25952;
var G__25993 = count__25286_25953;
var G__25994 = (i__25287_25954 + (1));
seq__25283_25951 = G__25991;
chunk__25285_25952 = G__25992;
count__25286_25953 = G__25993;
i__25287_25954 = G__25994;
continue;
}
} else {
var temp__5804__auto___25995 = cljs.core.seq(seq__25283_25951);
if(temp__5804__auto___25995){
var seq__25283_25996__$1 = temp__5804__auto___25995;
if(cljs.core.chunked_seq_QMARK_(seq__25283_25996__$1)){
var c__5525__auto___25997 = cljs.core.chunk_first(seq__25283_25996__$1);
var G__25998 = cljs.core.chunk_rest(seq__25283_25996__$1);
var G__25999 = c__5525__auto___25997;
var G__26000 = cljs.core.count(c__5525__auto___25997);
var G__26001 = (0);
seq__25283_25951 = G__25998;
chunk__25285_25952 = G__25999;
count__25286_25953 = G__26000;
i__25287_25954 = G__26001;
continue;
} else {
var child_struct_26002 = cljs.core.first(seq__25283_25996__$1);
if((!((child_struct_26002 == null)))){
if(typeof child_struct_26002 === 'string'){
var text_26004 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_26004),child_struct_26002].join(''));
} else {
var children_26006 = shadow.dom.svg_node(child_struct_26002);
if(cljs.core.seq_QMARK_(children_26006)){
var seq__25350_26007 = cljs.core.seq(children_26006);
var chunk__25353_26008 = null;
var count__25354_26009 = (0);
var i__25355_26010 = (0);
while(true){
if((i__25355_26010 < count__25354_26009)){
var child_26011 = chunk__25353_26008.cljs$core$IIndexed$_nth$arity$2(null,i__25355_26010);
if(cljs.core.truth_(child_26011)){
node.appendChild(child_26011);


var G__26013 = seq__25350_26007;
var G__26015 = chunk__25353_26008;
var G__26016 = count__25354_26009;
var G__26017 = (i__25355_26010 + (1));
seq__25350_26007 = G__26013;
chunk__25353_26008 = G__26015;
count__25354_26009 = G__26016;
i__25355_26010 = G__26017;
continue;
} else {
var G__26018 = seq__25350_26007;
var G__26019 = chunk__25353_26008;
var G__26020 = count__25354_26009;
var G__26021 = (i__25355_26010 + (1));
seq__25350_26007 = G__26018;
chunk__25353_26008 = G__26019;
count__25354_26009 = G__26020;
i__25355_26010 = G__26021;
continue;
}
} else {
var temp__5804__auto___26022__$1 = cljs.core.seq(seq__25350_26007);
if(temp__5804__auto___26022__$1){
var seq__25350_26023__$1 = temp__5804__auto___26022__$1;
if(cljs.core.chunked_seq_QMARK_(seq__25350_26023__$1)){
var c__5525__auto___26024 = cljs.core.chunk_first(seq__25350_26023__$1);
var G__26025 = cljs.core.chunk_rest(seq__25350_26023__$1);
var G__26026 = c__5525__auto___26024;
var G__26027 = cljs.core.count(c__5525__auto___26024);
var G__26028 = (0);
seq__25350_26007 = G__26025;
chunk__25353_26008 = G__26026;
count__25354_26009 = G__26027;
i__25355_26010 = G__26028;
continue;
} else {
var child_26029 = cljs.core.first(seq__25350_26023__$1);
if(cljs.core.truth_(child_26029)){
node.appendChild(child_26029);


var G__26030 = cljs.core.next(seq__25350_26023__$1);
var G__26031 = null;
var G__26032 = (0);
var G__26033 = (0);
seq__25350_26007 = G__26030;
chunk__25353_26008 = G__26031;
count__25354_26009 = G__26032;
i__25355_26010 = G__26033;
continue;
} else {
var G__26034 = cljs.core.next(seq__25350_26023__$1);
var G__26035 = null;
var G__26036 = (0);
var G__26037 = (0);
seq__25350_26007 = G__26034;
chunk__25353_26008 = G__26035;
count__25354_26009 = G__26036;
i__25355_26010 = G__26037;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_26006);
}
}


var G__26038 = cljs.core.next(seq__25283_25996__$1);
var G__26039 = null;
var G__26040 = (0);
var G__26041 = (0);
seq__25283_25951 = G__26038;
chunk__25285_25952 = G__26039;
count__25286_25953 = G__26040;
i__25287_25954 = G__26041;
continue;
} else {
var G__26042 = cljs.core.next(seq__25283_25996__$1);
var G__26043 = null;
var G__26044 = (0);
var G__26045 = (0);
seq__25283_25951 = G__26042;
chunk__25285_25952 = G__26043;
count__25286_25953 = G__26044;
i__25287_25954 = G__26045;
continue;
}
}
} else {
}
}
break;
}

return node;
});
(shadow.dom.SVGElement["string"] = true);

(shadow.dom._to_svg["string"] = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

(shadow.dom.SVGElement["null"] = true);

(shadow.dom._to_svg["null"] = (function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__5732__auto__ = [];
var len__5726__auto___26047 = arguments.length;
var i__5727__auto___26048 = (0);
while(true){
if((i__5727__auto___26048 < len__5726__auto___26047)){
args__5732__auto__.push((arguments[i__5727__auto___26048]));

var G__26049 = (i__5727__auto___26048 + (1));
i__5727__auto___26048 = G__26049;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq25364){
var G__25365 = cljs.core.first(seq25364);
var seq25364__$1 = cljs.core.next(seq25364);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__25365,seq25364__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__25370 = arguments.length;
switch (G__25370) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__5000__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__5000__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__5000__auto__;
}
})())){
var c__20563__auto___26052 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_25399){
var state_val_25400 = (state_25399[(1)]);
if((state_val_25400 === (1))){
var state_25399__$1 = state_25399;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_25399__$1,(2),once_or_cleanup);
} else {
if((state_val_25400 === (2))){
var inst_25396 = (state_25399[(2)]);
var inst_25397 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_25399__$1 = (function (){var statearr_25416 = state_25399;
(statearr_25416[(7)] = inst_25396);

return statearr_25416;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_25399__$1,inst_25397);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__19581__auto__ = null;
var shadow$dom$state_machine__19581__auto____0 = (function (){
var statearr_25421 = [null,null,null,null,null,null,null,null];
(statearr_25421[(0)] = shadow$dom$state_machine__19581__auto__);

(statearr_25421[(1)] = (1));

return statearr_25421;
});
var shadow$dom$state_machine__19581__auto____1 = (function (state_25399){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_25399);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e25422){var ex__19584__auto__ = e25422;
var statearr_25423_26054 = state_25399;
(statearr_25423_26054[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_25399[(4)]))){
var statearr_25427_26055 = state_25399;
(statearr_25427_26055[(1)] = cljs.core.first((state_25399[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26056 = state_25399;
state_25399 = G__26056;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
shadow$dom$state_machine__19581__auto__ = function(state_25399){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__19581__auto____0.call(this);
case 1:
return shadow$dom$state_machine__19581__auto____1.call(this,state_25399);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__19581__auto____0;
shadow$dom$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__19581__auto____1;
return shadow$dom$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_25428 = f__20564__auto__();
(statearr_25428[(6)] = c__20563__auto___26052);

return statearr_25428;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
