goog.provide('shadow.remote.runtime.shared');
shadow.remote.runtime.shared.init_state = (function shadow$remote$runtime$shared$init_state(client_info){
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),(0),new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.PersistentArrayMap.EMPTY], null);
});
shadow.remote.runtime.shared.now = (function shadow$remote$runtime$shared$now(){
return Date.now();
});
shadow.remote.runtime.shared.get_client_id = (function shadow$remote$runtime$shared$get_client_id(p__19842){
var map__19844 = p__19842;
var map__19844__$1 = cljs.core.__destructure_map(map__19844);
var runtime = map__19844__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19844__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var or__5002__auto__ = new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("runtime has no assigned runtime-id",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null));
}
});
shadow.remote.runtime.shared.relay_msg = (function shadow$remote$runtime$shared$relay_msg(runtime,msg){
var self_id_20152 = shadow.remote.runtime.shared.get_client_id(runtime);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"to","to",192099007).cljs$core$IFn$_invoke$arity$1(msg),self_id_20152)){
shadow.remote.runtime.api.relay_msg(runtime,msg);
} else {
Promise.resolve((1)).then((function (){
var G__19860 = runtime;
var G__19861 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"from","from",1815293044),self_id_20152);
return (shadow.remote.runtime.shared.process.cljs$core$IFn$_invoke$arity$2 ? shadow.remote.runtime.shared.process.cljs$core$IFn$_invoke$arity$2(G__19860,G__19861) : shadow.remote.runtime.shared.process.call(null,G__19860,G__19861));
}));
}

return msg;
});
shadow.remote.runtime.shared.reply = (function shadow$remote$runtime$shared$reply(runtime,p__19865,res){
var map__19869 = p__19865;
var map__19869__$1 = cljs.core.__destructure_map(map__19869);
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19869__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19869__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var res__$1 = (function (){var G__19872 = res;
var G__19872__$1 = (cljs.core.truth_(call_id)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__19872,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id):G__19872);
if(cljs.core.truth_(from)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__19872__$1,new cljs.core.Keyword(null,"to","to",192099007),from);
} else {
return G__19872__$1;
}
})();
return shadow.remote.runtime.api.relay_msg(runtime,res__$1);
});
shadow.remote.runtime.shared.call = (function shadow$remote$runtime$shared$call(var_args){
var G__19903 = arguments.length;
switch (G__19903) {
case 3:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3 = (function (runtime,msg,handlers){
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4(runtime,msg,handlers,(0));
}));

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4 = (function (p__19914,msg,handlers,timeout_after_ms){
var map__19915 = p__19914;
var map__19915__$1 = cljs.core.__destructure_map(map__19915);
var runtime = map__19915__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19915__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
if(cljs.core.map_QMARK_(msg)){
} else {
throw (new Error("Assert failed: (map? msg)"));
}

if(cljs.core.map_QMARK_(handlers)){
} else {
throw (new Error("Assert failed: (map? handlers)"));
}

if(cljs.core.nat_int_QMARK_(timeout_after_ms)){
} else {
throw (new Error("Assert failed: (nat-int? timeout-after-ms)"));
}

var call_id = new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),cljs.core.inc);

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handlers","handlers",79528781),handlers,new cljs.core.Keyword(null,"called-at","called-at",607081160),shadow.remote.runtime.shared.now(),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg,new cljs.core.Keyword(null,"timeout","timeout",-318625318),timeout_after_ms], null));

return shadow.remote.runtime.api.relay_msg(runtime,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id));
}));

(shadow.remote.runtime.shared.call.cljs$lang$maxFixedArity = 4);

shadow.remote.runtime.shared.trigger_BANG_ = (function shadow$remote$runtime$shared$trigger_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___20165 = arguments.length;
var i__5727__auto___20166 = (0);
while(true){
if((i__5727__auto___20166 < len__5726__auto___20165)){
args__5732__auto__.push((arguments[i__5727__auto___20166]));

var G__20167 = (i__5727__auto___20166 + (1));
i__5727__auto___20166 = G__20167;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__19930,ev,args){
var map__19932 = p__19930;
var map__19932__$1 = cljs.core.__destructure_map(map__19932);
var runtime = map__19932__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19932__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var seq__19933 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__19936 = null;
var count__19937 = (0);
var i__19938 = (0);
while(true){
if((i__19938 < count__19937)){
var ext = chunk__19936.cljs$core$IIndexed$_nth$arity$2(null,i__19938);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__20170 = seq__19933;
var G__20171 = chunk__19936;
var G__20172 = count__19937;
var G__20173 = (i__19938 + (1));
seq__19933 = G__20170;
chunk__19936 = G__20171;
count__19937 = G__20172;
i__19938 = G__20173;
continue;
} else {
var G__20174 = seq__19933;
var G__20175 = chunk__19936;
var G__20176 = count__19937;
var G__20177 = (i__19938 + (1));
seq__19933 = G__20174;
chunk__19936 = G__20175;
count__19937 = G__20176;
i__19938 = G__20177;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__19933);
if(temp__5804__auto__){
var seq__19933__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__19933__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__19933__$1);
var G__20178 = cljs.core.chunk_rest(seq__19933__$1);
var G__20179 = c__5525__auto__;
var G__20180 = cljs.core.count(c__5525__auto__);
var G__20181 = (0);
seq__19933 = G__20178;
chunk__19936 = G__20179;
count__19937 = G__20180;
i__19938 = G__20181;
continue;
} else {
var ext = cljs.core.first(seq__19933__$1);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__20185 = cljs.core.next(seq__19933__$1);
var G__20186 = null;
var G__20187 = (0);
var G__20188 = (0);
seq__19933 = G__20185;
chunk__19936 = G__20186;
count__19937 = G__20187;
i__19938 = G__20188;
continue;
} else {
var G__20190 = cljs.core.next(seq__19933__$1);
var G__20191 = null;
var G__20192 = (0);
var G__20193 = (0);
seq__19933 = G__20190;
chunk__19936 = G__20191;
count__19937 = G__20192;
i__19938 = G__20193;
continue;
}
}
} else {
return null;
}
}
break;
}
}));

(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$applyTo = (function (seq19923){
var G__19924 = cljs.core.first(seq19923);
var seq19923__$1 = cljs.core.next(seq19923);
var G__19925 = cljs.core.first(seq19923__$1);
var seq19923__$2 = cljs.core.next(seq19923__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__19924,G__19925,seq19923__$2);
}));

shadow.remote.runtime.shared.welcome = (function shadow$remote$runtime$shared$welcome(p__19959,p__19960){
var map__19961 = p__19959;
var map__19961__$1 = cljs.core.__destructure_map(map__19961);
var runtime = map__19961__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19961__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__19962 = p__19960;
var map__19962__$1 = cljs.core.__destructure_map(map__19962);
var msg = map__19962__$1;
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19962__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc,new cljs.core.Keyword(null,"client-id","client-id",-464622140),client_id);

var map__19969 = cljs.core.deref(state_ref);
var map__19969__$1 = cljs.core.__destructure_map(map__19969);
var client_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19969__$1,new cljs.core.Keyword(null,"client-info","client-info",1958982504));
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19969__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
shadow.remote.runtime.shared.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"hello","hello",-245025397),new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info], null));

return shadow.remote.runtime.shared.trigger_BANG_(runtime,new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125));
});
shadow.remote.runtime.shared.ping = (function shadow$remote$runtime$shared$ping(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"pong","pong",-172484958)], null));
});
shadow.remote.runtime.shared.request_supported_ops = (function shadow$remote$runtime$shared$request_supported_ops(p__19978,msg){
var map__19979 = p__19978;
var map__19979__$1 = cljs.core.__destructure_map(map__19979);
var runtime = map__19979__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19979__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"supported-ops","supported-ops",337914702),new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.disj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.set(cljs.core.keys(new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref)))),new cljs.core.Keyword(null,"welcome","welcome",-578152123),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),new cljs.core.Keyword(null,"tool-disconnect","tool-disconnect",189103996)], 0))], null));
});
shadow.remote.runtime.shared.unknown_relay_op = (function shadow$remote$runtime$shared$unknown_relay_op(msg){
return console.warn("unknown-relay-op",msg);
});
shadow.remote.runtime.shared.unknown_op = (function shadow$remote$runtime$shared$unknown_op(msg){
return console.warn("unknown-op",msg);
});
shadow.remote.runtime.shared.add_extension_STAR_ = (function shadow$remote$runtime$shared$add_extension_STAR_(p__20004,key,p__20005){
var map__20008 = p__20004;
var map__20008__$1 = cljs.core.__destructure_map(map__20008);
var state = map__20008__$1;
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20008__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
var map__20010 = p__20005;
var map__20010__$1 = cljs.core.__destructure_map(map__20010);
var spec = map__20010__$1;
var ops = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20010__$1,new cljs.core.Keyword(null,"ops","ops",1237330063));
if(cljs.core.contains_QMARK_(extensions,key)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("extension already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"spec","spec",347520401),spec], null));
} else {
}

return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null)))){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("op already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"op","op",-1882987955),op_kw], null));
} else {
}

return cljs.core.assoc_in(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null),op_handler);
}),cljs.core.assoc_in(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null),spec),ops);
});
shadow.remote.runtime.shared.add_extension = (function shadow$remote$runtime$shared$add_extension(p__20034,key,spec){
var map__20035 = p__20034;
var map__20035__$1 = cljs.core.__destructure_map(map__20035);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20035__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,shadow.remote.runtime.shared.add_extension_STAR_,key,spec);
});
shadow.remote.runtime.shared.add_defaults = (function shadow$remote$runtime$shared$add_defaults(runtime){
return shadow.remote.runtime.shared.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.shared","defaults","shadow.remote.runtime.shared/defaults",-1821257543),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"welcome","welcome",-578152123),(function (p1__20040_SHARP_){
return shadow.remote.runtime.shared.welcome(runtime,p1__20040_SHARP_);
}),new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),(function (p1__20041_SHARP_){
return shadow.remote.runtime.shared.unknown_relay_op(p1__20041_SHARP_);
}),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),(function (p1__20042_SHARP_){
return shadow.remote.runtime.shared.unknown_op(p1__20042_SHARP_);
}),new cljs.core.Keyword(null,"ping","ping",-1670114784),(function (p1__20043_SHARP_){
return shadow.remote.runtime.shared.ping(runtime,p1__20043_SHARP_);
}),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),(function (p1__20044_SHARP_){
return shadow.remote.runtime.shared.request_supported_ops(runtime,p1__20044_SHARP_);
})], null)], null));
});
shadow.remote.runtime.shared.del_extension_STAR_ = (function shadow$remote$runtime$shared$del_extension_STAR_(state,key){
var ext = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null));
if(cljs.core.not(ext)){
return state;
} else {
return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(state__$1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063)], null),cljs.core.dissoc,op_kw);
}),cljs.core.update.cljs$core$IFn$_invoke$arity$4(state,new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.dissoc,key),new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(ext));
}
});
shadow.remote.runtime.shared.del_extension = (function shadow$remote$runtime$shared$del_extension(p__20074,key){
var map__20076 = p__20074;
var map__20076__$1 = cljs.core.__destructure_map(map__20076);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20076__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(state_ref,shadow.remote.runtime.shared.del_extension_STAR_,key);
});
shadow.remote.runtime.shared.unhandled_call_result = (function shadow$remote$runtime$shared$unhandled_call_result(call_config,msg){
return console.warn("unhandled call result",msg,call_config);
});
shadow.remote.runtime.shared.unhandled_client_not_found = (function shadow$remote$runtime$shared$unhandled_client_not_found(p__20091,msg){
var map__20092 = p__20091;
var map__20092__$1 = cljs.core.__destructure_map(map__20092);
var runtime = map__20092__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20092__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic(runtime,new cljs.core.Keyword(null,"on-client-not-found","on-client-not-found",-642452849),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0));
});
shadow.remote.runtime.shared.reply_unknown_op = (function shadow$remote$runtime$shared$reply_unknown_op(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg], null));
});
shadow.remote.runtime.shared.process = (function shadow$remote$runtime$shared$process(p__20102,p__20103){
var map__20104 = p__20102;
var map__20104__$1 = cljs.core.__destructure_map(map__20104);
var runtime = map__20104__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20104__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__20105 = p__20103;
var map__20105__$1 = cljs.core.__destructure_map(map__20105);
var msg = map__20105__$1;
var op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20105__$1,new cljs.core.Keyword(null,"op","op",-1882987955));
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20105__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var state = cljs.core.deref(state_ref);
var op_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op], null));
if(cljs.core.truth_(call_id)){
var cfg = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null));
var call_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cfg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"handlers","handlers",79528781),op], null));
if(cljs.core.truth_(call_handler)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([call_id], 0));

return (call_handler.cljs$core$IFn$_invoke$arity$1 ? call_handler.cljs$core$IFn$_invoke$arity$1(msg) : call_handler.call(null,msg));
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
return shadow.remote.runtime.shared.unhandled_call_result(cfg,msg);

}
}
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-not-found","client-not-found",-1754042614),op)){
return shadow.remote.runtime.shared.unhandled_client_not_found(runtime,msg);
} else {
return shadow.remote.runtime.shared.reply_unknown_op(runtime,msg);

}
}
}
});
shadow.remote.runtime.shared.run_on_idle = (function shadow$remote$runtime$shared$run_on_idle(state_ref){
var seq__20118 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__20121 = null;
var count__20122 = (0);
var i__20123 = (0);
while(true){
if((i__20123 < count__20122)){
var map__20138 = chunk__20121.cljs$core$IIndexed$_nth$arity$2(null,i__20123);
var map__20138__$1 = cljs.core.__destructure_map(map__20138);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20138__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__20233 = seq__20118;
var G__20234 = chunk__20121;
var G__20235 = count__20122;
var G__20236 = (i__20123 + (1));
seq__20118 = G__20233;
chunk__20121 = G__20234;
count__20122 = G__20235;
i__20123 = G__20236;
continue;
} else {
var G__20237 = seq__20118;
var G__20238 = chunk__20121;
var G__20239 = count__20122;
var G__20240 = (i__20123 + (1));
seq__20118 = G__20237;
chunk__20121 = G__20238;
count__20122 = G__20239;
i__20123 = G__20240;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__20118);
if(temp__5804__auto__){
var seq__20118__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__20118__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__20118__$1);
var G__20242 = cljs.core.chunk_rest(seq__20118__$1);
var G__20243 = c__5525__auto__;
var G__20244 = cljs.core.count(c__5525__auto__);
var G__20245 = (0);
seq__20118 = G__20242;
chunk__20121 = G__20243;
count__20122 = G__20244;
i__20123 = G__20245;
continue;
} else {
var map__20143 = cljs.core.first(seq__20118__$1);
var map__20143__$1 = cljs.core.__destructure_map(map__20143);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20143__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__20249 = cljs.core.next(seq__20118__$1);
var G__20250 = null;
var G__20251 = (0);
var G__20252 = (0);
seq__20118 = G__20249;
chunk__20121 = G__20250;
count__20122 = G__20251;
i__20123 = G__20252;
continue;
} else {
var G__20253 = cljs.core.next(seq__20118__$1);
var G__20254 = null;
var G__20255 = (0);
var G__20256 = (0);
seq__20118 = G__20253;
chunk__20121 = G__20254;
count__20122 = G__20255;
i__20123 = G__20256;
continue;
}
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=shadow.remote.runtime.shared.js.map
