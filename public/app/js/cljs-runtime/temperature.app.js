goog.provide('temperature.app');
if((typeof temperature !== 'undefined') && (typeof temperature.app !== 'undefined') && (typeof temperature.app.unit !== 'undefined')){
} else {
temperature.app.unit = reagent.core.atom.cljs$core$IFn$_invoke$arity$1("f");
}
if((typeof temperature !== 'undefined') && (typeof temperature.app !== 'undefined') && (typeof temperature.app.to_unit !== 'undefined')){
} else {
temperature.app.to_unit = reagent.core.atom.cljs$core$IFn$_invoke$arity$1("c");
}
if((typeof temperature !== 'undefined') && (typeof temperature.app !== 'undefined') && (typeof temperature.app.temp !== 'undefined')){
} else {
temperature.app.temp = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((0));
}
temperature.app.convert_temp = (function temperature$app$convert_temp(degrees,from,to){
var deg = parseFloat(degrees);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,to)){
return deg;
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,"f")) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to,"c")))){
return ((deg - (32)) / 1.8);
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,"c")) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to,"f")))){
return ((deg * 1.8) + (32));
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,"c")) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to,"k")))){
return (deg + 273.15);
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,"k")) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to,"c")))){
return (deg - 273.15);
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,"k")) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to,"f")))){
return ((((deg - 273.15) * (9)) / (5)) + (32));
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(from,"f")) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(to,"k")))){
return (273.15 + (((5) * (deg - (32))) / (9)));
} else {
return 0.0;

}
}
}
}
}
}
}
});
temperature.app.temp_options = (function temperature$app$temp_options(init_val,f_on_change,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.row","div.row",133678515),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.custom-select","div.custom-select",2043386962),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select.form-select","select.form-select",1844412748),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"multiple","multiple",1244445549),null,new cljs.core.Keyword(null,"name","name",1843675177),"duallistbox_demo",new cljs.core.Keyword(null,"value","value",305978217),init_val,new cljs.core.Keyword(null,"onChange","onChange",-312891301),f_on_change,new cljs.core.Keyword(null,"class","class",-2030961996),"demo"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),"c"], null),"c"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),"k"], null),"k"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),"f"], null),"f"], null)], null)], null)], null);
});
/**
 * Obtain the temperature from the user
 */
temperature.app.temperature_in = (function temperature$app$temperature_in(value){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.temp-control","div.temp-control",-321010513),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),"row align-items-start"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Convert from"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-4","div.col-4",-64503036),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"for","for",-1323786319),"temp"], null),"Temperature"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-6","div.col-6",-2040623677),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"number",new cljs.core.Keyword(null,"id","id",-1388402092),"temp",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref(temperature.app.temp),new cljs.core.Keyword(null,"name","name",1843675177),"temp",new cljs.core.Keyword(null,"onChange","onChange",-312891301),(function (p1__18392_SHARP_){
return cljs.core.reset_BANG_(temperature.app.temp,p1__18392_SHARP_.target.value);
})], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-2","div.col-2",-1787809207),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [temperature.app.temp_options,cljs.core.deref(temperature.app.unit),(function (p1__18393_SHARP_){
return cljs.core.reset_BANG_(temperature.app.unit,p1__18393_SHARP_.target.value);
})], null)], null)], null);
});
/**
 * Render the results
 */
temperature.app.results = (function temperature$app$results(){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.converted-output","div.converted-output",-1064113156),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),"row align-items-start"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Converted Value"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-4","div.col-4",-64503036),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"for","for",-1323786319),"temp"], null),"Result"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-6","div.col-6",-2040623677),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),goog.string.format("%.2f",temperature.app.convert_temp(cljs.core.deref(temperature.app.temp),cljs.core.deref(temperature.app.unit),cljs.core.deref(temperature.app.to_unit)))], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-2","div.col-2",-1787809207),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [temperature.app.temp_options,cljs.core.deref(temperature.app.to_unit),(function (p1__18394_SHARP_){
return cljs.core.reset_BANG_(temperature.app.to_unit,p1__18394_SHARP_.target.value);
})], null)], null)], null)], null);
});
temperature.app.btn_swap = (function temperature$app$btn_swap(){
var current_unit = cljs.core.deref(temperature.app.unit);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),"row align-items-center"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),"btn btn-secondary btn-sm",new cljs.core.Keyword(null,"onClick","onClick",-1991238530),(function (){
cljs.core.reset_BANG_(temperature.app.unit,cljs.core.deref(temperature.app.to_unit));

return cljs.core.reset_BANG_(temperature.app.to_unit,current_unit);
})], null),"\u21C4"], null)], null);
});
temperature.app.Application = (function temperature$app$Application(){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.row","div.row",133678515),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Temp converter"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-5","div.col-5",-2108746862),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [temperature.app.temperature_in], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-2","div.col-2",-1787809207),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [temperature.app.btn_swap], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col-5","div.col-5",-2108746862),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [temperature.app.results], null)], null)], null);
});
reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [temperature.app.Application], null),document.body);
temperature.app.init = (function temperature$app$init(){
return temperature.app.Application();
});

//# sourceMappingURL=temperature.app.js.map
