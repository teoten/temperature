goog.provide('shadow.remote.runtime.tap_support');
shadow.remote.runtime.tap_support.tap_subscribe = (function shadow$remote$runtime$tap_support$tap_subscribe(p__25024,p__25025){
var map__25026 = p__25024;
var map__25026__$1 = cljs.core.__destructure_map(map__25026);
var svc = map__25026__$1;
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25026__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
var obj_support = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25026__$1,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25026__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var map__25027 = p__25025;
var map__25027__$1 = cljs.core.__destructure_map(map__25027);
var msg = map__25027__$1;
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25027__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var summary = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25027__$1,new cljs.core.Keyword(null,"summary","summary",380847952));
var history__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25027__$1,new cljs.core.Keyword(null,"history","history",-247395220));
var num = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__25027__$1,new cljs.core.Keyword(null,"num","num",1985240673),(10));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(subs_ref,cljs.core.assoc,from,msg);

if(cljs.core.truth_(history__$1)){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap-subscribed","tap-subscribed",-1882247432),new cljs.core.Keyword(null,"history","history",-247395220),cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (oid){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"oid","oid",-768692334),oid,new cljs.core.Keyword(null,"summary","summary",380847952),shadow.remote.runtime.obj_support.obj_describe_STAR_(obj_support,oid)], null);
}),shadow.remote.runtime.obj_support.get_tap_history(obj_support,num)))], null));
} else {
return null;
}
});
shadow.remote.runtime.tap_support.tap_unsubscribe = (function shadow$remote$runtime$tap_support$tap_unsubscribe(p__25042,p__25043){
var map__25044 = p__25042;
var map__25044__$1 = cljs.core.__destructure_map(map__25044);
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25044__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
var map__25046 = p__25043;
var map__25046__$1 = cljs.core.__destructure_map(map__25046);
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25046__$1,new cljs.core.Keyword(null,"from","from",1815293044));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(subs_ref,cljs.core.dissoc,from);
});
shadow.remote.runtime.tap_support.request_tap_history = (function shadow$remote$runtime$tap_support$request_tap_history(p__25062,p__25064){
var map__25067 = p__25062;
var map__25067__$1 = cljs.core.__destructure_map(map__25067);
var obj_support = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25067__$1,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25067__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var map__25068 = p__25064;
var map__25068__$1 = cljs.core.__destructure_map(map__25068);
var msg = map__25068__$1;
var num = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__25068__$1,new cljs.core.Keyword(null,"num","num",1985240673),(10));
var tap_ids = shadow.remote.runtime.obj_support.get_tap_history(obj_support,num);
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap-history","tap-history",-282803347),new cljs.core.Keyword(null,"oids","oids",-1580877688),tap_ids], null));
});
shadow.remote.runtime.tap_support.tool_disconnect = (function shadow$remote$runtime$tap_support$tool_disconnect(p__25078,tid){
var map__25079 = p__25078;
var map__25079__$1 = cljs.core.__destructure_map(map__25079);
var svc = map__25079__$1;
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25079__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(subs_ref,cljs.core.dissoc,tid);
});
shadow.remote.runtime.tap_support.start = (function shadow$remote$runtime$tap_support$start(runtime,obj_support){
var subs_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var tap_fn = (function shadow$remote$runtime$tap_support$start_$_runtime_tap(obj){
if((!((obj == null)))){
var oid = shadow.remote.runtime.obj_support.register(obj_support,obj,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"from","from",1815293044),new cljs.core.Keyword(null,"tap","tap",-1086702463)], null));
var seq__25104 = cljs.core.seq(cljs.core.deref(subs_ref));
var chunk__25105 = null;
var count__25106 = (0);
var i__25107 = (0);
while(true){
if((i__25107 < count__25106)){
var vec__25136 = chunk__25105.cljs$core$IIndexed$_nth$arity$2(null,i__25107);
var tid = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25136,(0),null);
var tap_config = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25136,(1),null);
shadow.remote.runtime.api.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap","tap",-1086702463),new cljs.core.Keyword(null,"to","to",192099007),tid,new cljs.core.Keyword(null,"oid","oid",-768692334),oid], null));


var G__25174 = seq__25104;
var G__25175 = chunk__25105;
var G__25176 = count__25106;
var G__25177 = (i__25107 + (1));
seq__25104 = G__25174;
chunk__25105 = G__25175;
count__25106 = G__25176;
i__25107 = G__25177;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__25104);
if(temp__5804__auto__){
var seq__25104__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__25104__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__25104__$1);
var G__25178 = cljs.core.chunk_rest(seq__25104__$1);
var G__25179 = c__5525__auto__;
var G__25180 = cljs.core.count(c__5525__auto__);
var G__25181 = (0);
seq__25104 = G__25178;
chunk__25105 = G__25179;
count__25106 = G__25180;
i__25107 = G__25181;
continue;
} else {
var vec__25152 = cljs.core.first(seq__25104__$1);
var tid = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25152,(0),null);
var tap_config = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__25152,(1),null);
shadow.remote.runtime.api.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap","tap",-1086702463),new cljs.core.Keyword(null,"to","to",192099007),tid,new cljs.core.Keyword(null,"oid","oid",-768692334),oid], null));


var G__25186 = cljs.core.next(seq__25104__$1);
var G__25187 = null;
var G__25188 = (0);
var G__25189 = (0);
seq__25104 = G__25186;
chunk__25105 = G__25187;
count__25106 = G__25188;
i__25107 = G__25189;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
});
var svc = new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229),obj_support,new cljs.core.Keyword(null,"tap-fn","tap-fn",1573556461),tap_fn,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911),subs_ref], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.tap-support","ext","shadow.remote.runtime.tap-support/ext",1019069674),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"tap-subscribe","tap-subscribe",411179050),(function (p1__25084_SHARP_){
return shadow.remote.runtime.tap_support.tap_subscribe(svc,p1__25084_SHARP_);
}),new cljs.core.Keyword(null,"tap-unsubscribe","tap-unsubscribe",1183890755),(function (p1__25085_SHARP_){
return shadow.remote.runtime.tap_support.tap_unsubscribe(svc,p1__25085_SHARP_);
}),new cljs.core.Keyword(null,"request-tap-history","request-tap-history",-670837812),(function (p1__25086_SHARP_){
return shadow.remote.runtime.tap_support.request_tap_history(svc,p1__25086_SHARP_);
})], null),new cljs.core.Keyword(null,"on-tool-disconnect","on-tool-disconnect",693464366),(function (p1__25087_SHARP_){
return shadow.remote.runtime.tap_support.tool_disconnect(svc,p1__25087_SHARP_);
})], null));

cljs.core.add_tap(tap_fn);

return svc;
});
shadow.remote.runtime.tap_support.stop = (function shadow$remote$runtime$tap_support$stop(p__25165){
var map__25166 = p__25165;
var map__25166__$1 = cljs.core.__destructure_map(map__25166);
var svc = map__25166__$1;
var tap_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25166__$1,new cljs.core.Keyword(null,"tap-fn","tap-fn",1573556461));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__25166__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
cljs.core.remove_tap(tap_fn);

return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.tap-support","ext","shadow.remote.runtime.tap-support/ext",1019069674));
});

//# sourceMappingURL=shadow.remote.runtime.tap_support.js.map
