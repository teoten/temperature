goog.provide('cljs.core.async');
goog.scope(function(){
  cljs.core.async.goog$module$goog$array = goog.module.get('goog.array');
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async20692 = (function (f,blockable,meta20693){
this.f = f;
this.blockable = blockable;
this.meta20693 = meta20693;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async20692.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_20694,meta20693__$1){
var self__ = this;
var _20694__$1 = this;
return (new cljs.core.async.t_cljs$core$async20692(self__.f,self__.blockable,meta20693__$1));
}));

(cljs.core.async.t_cljs$core$async20692.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_20694){
var self__ = this;
var _20694__$1 = this;
return self__.meta20693;
}));

(cljs.core.async.t_cljs$core$async20692.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async20692.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async20692.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async20692.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async20692.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta20693","meta20693",96064828,null)], null);
}));

(cljs.core.async.t_cljs$core$async20692.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async20692.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async20692");

(cljs.core.async.t_cljs$core$async20692.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async20692");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async20692.
 */
cljs.core.async.__GT_t_cljs$core$async20692 = (function cljs$core$async$__GT_t_cljs$core$async20692(f,blockable,meta20693){
return (new cljs.core.async.t_cljs$core$async20692(f,blockable,meta20693));
});


cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__20686 = arguments.length;
switch (G__20686) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
return (new cljs.core.async.t_cljs$core$async20692(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__20738 = arguments.length;
switch (G__20738) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__20769 = arguments.length;
switch (G__20769) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__20813 = arguments.length;
switch (G__20813) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_24162 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_24162) : fn1.call(null,val_24162));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_24162) : fn1.call(null,val_24162));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__20844 = arguments.length;
switch (G__20844) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5802__auto__)){
var ret = temp__5802__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5802__auto__)){
var retb = temp__5802__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__5593__auto___24173 = n;
var x_24174 = (0);
while(true){
if((x_24174 < n__5593__auto___24173)){
(a[x_24174] = x_24174);

var G__24175 = (x_24174 + (1));
x_24174 = G__24175;
continue;
} else {
}
break;
}

cljs.core.async.goog$module$goog$array.shuffle(a);

return a;
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async20885 = (function (flag,meta20886){
this.flag = flag;
this.meta20886 = meta20886;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async20885.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_20887,meta20886__$1){
var self__ = this;
var _20887__$1 = this;
return (new cljs.core.async.t_cljs$core$async20885(self__.flag,meta20886__$1));
}));

(cljs.core.async.t_cljs$core$async20885.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_20887){
var self__ = this;
var _20887__$1 = this;
return self__.meta20886;
}));

(cljs.core.async.t_cljs$core$async20885.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async20885.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async20885.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async20885.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async20885.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta20886","meta20886",998622898,null)], null);
}));

(cljs.core.async.t_cljs$core$async20885.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async20885.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async20885");

(cljs.core.async.t_cljs$core$async20885.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async20885");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async20885.
 */
cljs.core.async.__GT_t_cljs$core$async20885 = (function cljs$core$async$__GT_t_cljs$core$async20885(flag,meta20886){
return (new cljs.core.async.t_cljs$core$async20885(flag,meta20886));
});


cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
return (new cljs.core.async.t_cljs$core$async20885(flag,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async20941 = (function (flag,cb,meta20942){
this.flag = flag;
this.cb = cb;
this.meta20942 = meta20942;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async20941.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_20943,meta20942__$1){
var self__ = this;
var _20943__$1 = this;
return (new cljs.core.async.t_cljs$core$async20941(self__.flag,self__.cb,meta20942__$1));
}));

(cljs.core.async.t_cljs$core$async20941.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_20943){
var self__ = this;
var _20943__$1 = this;
return self__.meta20942;
}));

(cljs.core.async.t_cljs$core$async20941.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async20941.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async20941.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async20941.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async20941.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta20942","meta20942",-1016803795,null)], null);
}));

(cljs.core.async.t_cljs$core$async20941.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async20941.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async20941");

(cljs.core.async.t_cljs$core$async20941.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async20941");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async20941.
 */
cljs.core.async.__GT_t_cljs$core$async20941 = (function cljs$core$async$__GT_t_cljs$core$async20941(flag,cb,meta20942){
return (new cljs.core.async.t_cljs$core$async20941(flag,cb,meta20942));
});


cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
return (new cljs.core.async.t_cljs$core$async20941(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__20989_SHARP_){
var G__21015 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__20989_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__21015) : fret.call(null,G__21015));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__20991_SHARP_){
var G__21016 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__20991_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__21016) : fret.call(null,G__21016));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__5002__auto__ = wport;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return port;
}
})()], null));
} else {
var G__24185 = (i + (1));
i = G__24185;
continue;
}
} else {
return null;
}
break;
}
})();
var or__5002__auto__ = ret;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5804__auto__ = (function (){var and__5000__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__5000__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__5000__auto__;
}
})();
if(cljs.core.truth_(temp__5804__auto__)){
var got = temp__5804__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___24186 = arguments.length;
var i__5727__auto___24187 = (0);
while(true){
if((i__5727__auto___24187 < len__5726__auto___24186)){
args__5732__auto__.push((arguments[i__5727__auto___24187]));

var G__24188 = (i__5727__auto___24187 + (1));
i__5727__auto___24187 = G__24188;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__21068){
var map__21069 = p__21068;
var map__21069__$1 = cljs.core.__destructure_map(map__21069);
var opts = map__21069__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq21049){
var G__21053 = cljs.core.first(seq21049);
var seq21049__$1 = cljs.core.next(seq21049);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__21053,seq21049__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__21104 = arguments.length;
switch (G__21104) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__20563__auto___24192 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21183){
var state_val_21184 = (state_21183[(1)]);
if((state_val_21184 === (7))){
var inst_21167 = (state_21183[(2)]);
var state_21183__$1 = state_21183;
var statearr_21199_24193 = state_21183__$1;
(statearr_21199_24193[(2)] = inst_21167);

(statearr_21199_24193[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (1))){
var state_21183__$1 = state_21183;
var statearr_21203_24198 = state_21183__$1;
(statearr_21203_24198[(2)] = null);

(statearr_21203_24198[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (4))){
var inst_21138 = (state_21183[(7)]);
var inst_21138__$1 = (state_21183[(2)]);
var inst_21149 = (inst_21138__$1 == null);
var state_21183__$1 = (function (){var statearr_21210 = state_21183;
(statearr_21210[(7)] = inst_21138__$1);

return statearr_21210;
})();
if(cljs.core.truth_(inst_21149)){
var statearr_21212_24199 = state_21183__$1;
(statearr_21212_24199[(1)] = (5));

} else {
var statearr_21214_24200 = state_21183__$1;
(statearr_21214_24200[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (13))){
var state_21183__$1 = state_21183;
var statearr_21221_24202 = state_21183__$1;
(statearr_21221_24202[(2)] = null);

(statearr_21221_24202[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (6))){
var inst_21138 = (state_21183[(7)]);
var state_21183__$1 = state_21183;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_21183__$1,(11),to,inst_21138);
} else {
if((state_val_21184 === (3))){
var inst_21169 = (state_21183[(2)]);
var state_21183__$1 = state_21183;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21183__$1,inst_21169);
} else {
if((state_val_21184 === (12))){
var state_21183__$1 = state_21183;
var statearr_21237_24203 = state_21183__$1;
(statearr_21237_24203[(2)] = null);

(statearr_21237_24203[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (2))){
var state_21183__$1 = state_21183;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21183__$1,(4),from);
} else {
if((state_val_21184 === (11))){
var inst_21160 = (state_21183[(2)]);
var state_21183__$1 = state_21183;
if(cljs.core.truth_(inst_21160)){
var statearr_21238_24204 = state_21183__$1;
(statearr_21238_24204[(1)] = (12));

} else {
var statearr_21239_24205 = state_21183__$1;
(statearr_21239_24205[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (9))){
var state_21183__$1 = state_21183;
var statearr_21242_24206 = state_21183__$1;
(statearr_21242_24206[(2)] = null);

(statearr_21242_24206[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (5))){
var state_21183__$1 = state_21183;
if(cljs.core.truth_(close_QMARK_)){
var statearr_21245_24207 = state_21183__$1;
(statearr_21245_24207[(1)] = (8));

} else {
var statearr_21246_24208 = state_21183__$1;
(statearr_21246_24208[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (14))){
var inst_21165 = (state_21183[(2)]);
var state_21183__$1 = state_21183;
var statearr_21248_24209 = state_21183__$1;
(statearr_21248_24209[(2)] = inst_21165);

(statearr_21248_24209[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (10))){
var inst_21157 = (state_21183[(2)]);
var state_21183__$1 = state_21183;
var statearr_21253_24210 = state_21183__$1;
(statearr_21253_24210[(2)] = inst_21157);

(statearr_21253_24210[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21184 === (8))){
var inst_21153 = cljs.core.async.close_BANG_(to);
var state_21183__$1 = state_21183;
var statearr_21255_24211 = state_21183__$1;
(statearr_21255_24211[(2)] = inst_21153);

(statearr_21255_24211[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_21260 = [null,null,null,null,null,null,null,null];
(statearr_21260[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_21260[(1)] = (1));

return statearr_21260;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_21183){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21183);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21262){var ex__19584__auto__ = e21262;
var statearr_21264_24216 = state_21183;
(statearr_21264_24216[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21183[(4)]))){
var statearr_21267_24217 = state_21183;
(statearr_21267_24217[(1)] = cljs.core.first((state_21183[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24218 = state_21183;
state_21183 = G__24218;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_21183){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_21183);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21272 = f__20564__auto__();
(statearr_21272[(6)] = c__20563__auto___24192);

return statearr_21272;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process__$1 = (function (p__21295){
var vec__21296 = p__21295;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21296,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21296,(1),null);
var job = vec__21296;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__20563__auto___24219 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21314){
var state_val_21315 = (state_21314[(1)]);
if((state_val_21315 === (1))){
var state_21314__$1 = state_21314;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_21314__$1,(2),res,v);
} else {
if((state_val_21315 === (2))){
var inst_21307 = (state_21314[(2)]);
var inst_21312 = cljs.core.async.close_BANG_(res);
var state_21314__$1 = (function (){var statearr_21322 = state_21314;
(statearr_21322[(7)] = inst_21307);

return statearr_21322;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_21314__$1,inst_21312);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0 = (function (){
var statearr_21323 = [null,null,null,null,null,null,null,null];
(statearr_21323[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__);

(statearr_21323[(1)] = (1));

return statearr_21323;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1 = (function (state_21314){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21314);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21325){var ex__19584__auto__ = e21325;
var statearr_21328_24220 = state_21314;
(statearr_21328_24220[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21314[(4)]))){
var statearr_21332_24223 = state_21314;
(statearr_21332_24223[(1)] = cljs.core.first((state_21314[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24224 = state_21314;
state_21314 = G__24224;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = function(state_21314){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1.call(this,state_21314);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21335 = f__20564__auto__();
(statearr_21335[(6)] = c__20563__auto___24219);

return statearr_21335;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__21339){
var vec__21340 = p__21339;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21340,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21340,(1),null);
var job = vec__21340;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__5593__auto___24226 = n;
var __24229 = (0);
while(true){
if((__24229 < n__5593__auto___24226)){
var G__21345_24231 = type;
var G__21345_24232__$1 = (((G__21345_24231 instanceof cljs.core.Keyword))?G__21345_24231.fqn:null);
switch (G__21345_24232__$1) {
case "compute":
var c__20563__auto___24235 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__24229,c__20563__auto___24235,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async){
return (function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = ((function (__24229,c__20563__auto___24235,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async){
return (function (state_21369){
var state_val_21370 = (state_21369[(1)]);
if((state_val_21370 === (1))){
var state_21369__$1 = state_21369;
var statearr_21375_24236 = state_21369__$1;
(statearr_21375_24236[(2)] = null);

(statearr_21375_24236[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21370 === (2))){
var state_21369__$1 = state_21369;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21369__$1,(4),jobs);
} else {
if((state_val_21370 === (3))){
var inst_21367 = (state_21369[(2)]);
var state_21369__$1 = state_21369;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21369__$1,inst_21367);
} else {
if((state_val_21370 === (4))){
var inst_21359 = (state_21369[(2)]);
var inst_21360 = process__$1(inst_21359);
var state_21369__$1 = state_21369;
if(cljs.core.truth_(inst_21360)){
var statearr_21380_24237 = state_21369__$1;
(statearr_21380_24237[(1)] = (5));

} else {
var statearr_21384_24238 = state_21369__$1;
(statearr_21384_24238[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21370 === (5))){
var state_21369__$1 = state_21369;
var statearr_21386_24239 = state_21369__$1;
(statearr_21386_24239[(2)] = null);

(statearr_21386_24239[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21370 === (6))){
var state_21369__$1 = state_21369;
var statearr_21390_24241 = state_21369__$1;
(statearr_21390_24241[(2)] = null);

(statearr_21390_24241[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21370 === (7))){
var inst_21365 = (state_21369[(2)]);
var state_21369__$1 = state_21369;
var statearr_21394_24243 = state_21369__$1;
(statearr_21394_24243[(2)] = inst_21365);

(statearr_21394_24243[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__24229,c__20563__auto___24235,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async))
;
return ((function (__24229,switch__19580__auto__,c__20563__auto___24235,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0 = (function (){
var statearr_21400 = [null,null,null,null,null,null,null];
(statearr_21400[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__);

(statearr_21400[(1)] = (1));

return statearr_21400;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1 = (function (state_21369){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21369);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21402){var ex__19584__auto__ = e21402;
var statearr_21403_24244 = state_21369;
(statearr_21403_24244[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21369[(4)]))){
var statearr_21404_24245 = state_21369;
(statearr_21404_24245[(1)] = cljs.core.first((state_21369[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24246 = state_21369;
state_21369 = G__24246;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = function(state_21369){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1.call(this,state_21369);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__;
})()
;})(__24229,switch__19580__auto__,c__20563__auto___24235,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async))
})();
var state__20565__auto__ = (function (){var statearr_21411 = f__20564__auto__();
(statearr_21411[(6)] = c__20563__auto___24235);

return statearr_21411;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
});})(__24229,c__20563__auto___24235,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async))
);


break;
case "async":
var c__20563__auto___24248 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__24229,c__20563__auto___24248,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async){
return (function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = ((function (__24229,c__20563__auto___24248,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async){
return (function (state_21429){
var state_val_21430 = (state_21429[(1)]);
if((state_val_21430 === (1))){
var state_21429__$1 = state_21429;
var statearr_21437_24250 = state_21429__$1;
(statearr_21437_24250[(2)] = null);

(statearr_21437_24250[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21430 === (2))){
var state_21429__$1 = state_21429;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21429__$1,(4),jobs);
} else {
if((state_val_21430 === (3))){
var inst_21427 = (state_21429[(2)]);
var state_21429__$1 = state_21429;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21429__$1,inst_21427);
} else {
if((state_val_21430 === (4))){
var inst_21414 = (state_21429[(2)]);
var inst_21417 = async(inst_21414);
var state_21429__$1 = state_21429;
if(cljs.core.truth_(inst_21417)){
var statearr_21442_24251 = state_21429__$1;
(statearr_21442_24251[(1)] = (5));

} else {
var statearr_21444_24252 = state_21429__$1;
(statearr_21444_24252[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21430 === (5))){
var state_21429__$1 = state_21429;
var statearr_21445_24253 = state_21429__$1;
(statearr_21445_24253[(2)] = null);

(statearr_21445_24253[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21430 === (6))){
var state_21429__$1 = state_21429;
var statearr_21447_24254 = state_21429__$1;
(statearr_21447_24254[(2)] = null);

(statearr_21447_24254[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21430 === (7))){
var inst_21425 = (state_21429[(2)]);
var state_21429__$1 = state_21429;
var statearr_21448_24256 = state_21429__$1;
(statearr_21448_24256[(2)] = inst_21425);

(statearr_21448_24256[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__24229,c__20563__auto___24248,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async))
;
return ((function (__24229,switch__19580__auto__,c__20563__auto___24248,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0 = (function (){
var statearr_21450 = [null,null,null,null,null,null,null];
(statearr_21450[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__);

(statearr_21450[(1)] = (1));

return statearr_21450;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1 = (function (state_21429){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21429);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21459){var ex__19584__auto__ = e21459;
var statearr_21460_24258 = state_21429;
(statearr_21460_24258[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21429[(4)]))){
var statearr_21466_24259 = state_21429;
(statearr_21466_24259[(1)] = cljs.core.first((state_21429[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24260 = state_21429;
state_21429 = G__24260;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = function(state_21429){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1.call(this,state_21429);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__;
})()
;})(__24229,switch__19580__auto__,c__20563__auto___24248,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async))
})();
var state__20565__auto__ = (function (){var statearr_21470 = f__20564__auto__();
(statearr_21470[(6)] = c__20563__auto___24248);

return statearr_21470;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
});})(__24229,c__20563__auto___24248,G__21345_24231,G__21345_24232__$1,n__5593__auto___24226,jobs,results,process__$1,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__21345_24232__$1)].join('')));

}

var G__24262 = (__24229 + (1));
__24229 = G__24262;
continue;
} else {
}
break;
}

var c__20563__auto___24263 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21510){
var state_val_21511 = (state_21510[(1)]);
if((state_val_21511 === (7))){
var inst_21506 = (state_21510[(2)]);
var state_21510__$1 = state_21510;
var statearr_21516_24264 = state_21510__$1;
(statearr_21516_24264[(2)] = inst_21506);

(statearr_21516_24264[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21511 === (1))){
var state_21510__$1 = state_21510;
var statearr_21517_24265 = state_21510__$1;
(statearr_21517_24265[(2)] = null);

(statearr_21517_24265[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21511 === (4))){
var inst_21485 = (state_21510[(7)]);
var inst_21485__$1 = (state_21510[(2)]);
var inst_21486 = (inst_21485__$1 == null);
var state_21510__$1 = (function (){var statearr_21522 = state_21510;
(statearr_21522[(7)] = inst_21485__$1);

return statearr_21522;
})();
if(cljs.core.truth_(inst_21486)){
var statearr_21527_24268 = state_21510__$1;
(statearr_21527_24268[(1)] = (5));

} else {
var statearr_21532_24269 = state_21510__$1;
(statearr_21532_24269[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21511 === (6))){
var inst_21490 = (state_21510[(8)]);
var inst_21485 = (state_21510[(7)]);
var inst_21490__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_21497 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_21498 = [inst_21485,inst_21490__$1];
var inst_21499 = (new cljs.core.PersistentVector(null,2,(5),inst_21497,inst_21498,null));
var state_21510__$1 = (function (){var statearr_21534 = state_21510;
(statearr_21534[(8)] = inst_21490__$1);

return statearr_21534;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_21510__$1,(8),jobs,inst_21499);
} else {
if((state_val_21511 === (3))){
var inst_21508 = (state_21510[(2)]);
var state_21510__$1 = state_21510;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21510__$1,inst_21508);
} else {
if((state_val_21511 === (2))){
var state_21510__$1 = state_21510;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21510__$1,(4),from);
} else {
if((state_val_21511 === (9))){
var inst_21503 = (state_21510[(2)]);
var state_21510__$1 = (function (){var statearr_21543 = state_21510;
(statearr_21543[(9)] = inst_21503);

return statearr_21543;
})();
var statearr_21545_24270 = state_21510__$1;
(statearr_21545_24270[(2)] = null);

(statearr_21545_24270[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21511 === (5))){
var inst_21488 = cljs.core.async.close_BANG_(jobs);
var state_21510__$1 = state_21510;
var statearr_21558_24271 = state_21510__$1;
(statearr_21558_24271[(2)] = inst_21488);

(statearr_21558_24271[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21511 === (8))){
var inst_21490 = (state_21510[(8)]);
var inst_21501 = (state_21510[(2)]);
var state_21510__$1 = (function (){var statearr_21563 = state_21510;
(statearr_21563[(10)] = inst_21501);

return statearr_21563;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_21510__$1,(9),results,inst_21490);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0 = (function (){
var statearr_21569 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_21569[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__);

(statearr_21569[(1)] = (1));

return statearr_21569;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1 = (function (state_21510){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21510);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21570){var ex__19584__auto__ = e21570;
var statearr_21571_24272 = state_21510;
(statearr_21571_24272[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21510[(4)]))){
var statearr_21575_24273 = state_21510;
(statearr_21575_24273[(1)] = cljs.core.first((state_21510[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24275 = state_21510;
state_21510 = G__24275;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = function(state_21510){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1.call(this,state_21510);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21582 = f__20564__auto__();
(statearr_21582[(6)] = c__20563__auto___24263);

return statearr_21582;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


var c__20563__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21625){
var state_val_21626 = (state_21625[(1)]);
if((state_val_21626 === (7))){
var inst_21621 = (state_21625[(2)]);
var state_21625__$1 = state_21625;
var statearr_21630_24280 = state_21625__$1;
(statearr_21630_24280[(2)] = inst_21621);

(statearr_21630_24280[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (20))){
var state_21625__$1 = state_21625;
var statearr_21631_24282 = state_21625__$1;
(statearr_21631_24282[(2)] = null);

(statearr_21631_24282[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (1))){
var state_21625__$1 = state_21625;
var statearr_21632_24284 = state_21625__$1;
(statearr_21632_24284[(2)] = null);

(statearr_21632_24284[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (4))){
var inst_21587 = (state_21625[(7)]);
var inst_21587__$1 = (state_21625[(2)]);
var inst_21588 = (inst_21587__$1 == null);
var state_21625__$1 = (function (){var statearr_21633 = state_21625;
(statearr_21633[(7)] = inst_21587__$1);

return statearr_21633;
})();
if(cljs.core.truth_(inst_21588)){
var statearr_21634_24285 = state_21625__$1;
(statearr_21634_24285[(1)] = (5));

} else {
var statearr_21635_24286 = state_21625__$1;
(statearr_21635_24286[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (15))){
var inst_21603 = (state_21625[(8)]);
var state_21625__$1 = state_21625;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_21625__$1,(18),to,inst_21603);
} else {
if((state_val_21626 === (21))){
var inst_21616 = (state_21625[(2)]);
var state_21625__$1 = state_21625;
var statearr_21637_24287 = state_21625__$1;
(statearr_21637_24287[(2)] = inst_21616);

(statearr_21637_24287[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (13))){
var inst_21618 = (state_21625[(2)]);
var state_21625__$1 = (function (){var statearr_21638 = state_21625;
(statearr_21638[(9)] = inst_21618);

return statearr_21638;
})();
var statearr_21640_24288 = state_21625__$1;
(statearr_21640_24288[(2)] = null);

(statearr_21640_24288[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (6))){
var inst_21587 = (state_21625[(7)]);
var state_21625__$1 = state_21625;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21625__$1,(11),inst_21587);
} else {
if((state_val_21626 === (17))){
var inst_21611 = (state_21625[(2)]);
var state_21625__$1 = state_21625;
if(cljs.core.truth_(inst_21611)){
var statearr_21642_24290 = state_21625__$1;
(statearr_21642_24290[(1)] = (19));

} else {
var statearr_21646_24295 = state_21625__$1;
(statearr_21646_24295[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (3))){
var inst_21623 = (state_21625[(2)]);
var state_21625__$1 = state_21625;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21625__$1,inst_21623);
} else {
if((state_val_21626 === (12))){
var inst_21600 = (state_21625[(10)]);
var state_21625__$1 = state_21625;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21625__$1,(14),inst_21600);
} else {
if((state_val_21626 === (2))){
var state_21625__$1 = state_21625;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21625__$1,(4),results);
} else {
if((state_val_21626 === (19))){
var state_21625__$1 = state_21625;
var statearr_21647_24296 = state_21625__$1;
(statearr_21647_24296[(2)] = null);

(statearr_21647_24296[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (11))){
var inst_21600 = (state_21625[(2)]);
var state_21625__$1 = (function (){var statearr_21648 = state_21625;
(statearr_21648[(10)] = inst_21600);

return statearr_21648;
})();
var statearr_21649_24297 = state_21625__$1;
(statearr_21649_24297[(2)] = null);

(statearr_21649_24297[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (9))){
var state_21625__$1 = state_21625;
var statearr_21651_24301 = state_21625__$1;
(statearr_21651_24301[(2)] = null);

(statearr_21651_24301[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (5))){
var state_21625__$1 = state_21625;
if(cljs.core.truth_(close_QMARK_)){
var statearr_21657_24302 = state_21625__$1;
(statearr_21657_24302[(1)] = (8));

} else {
var statearr_21658_24303 = state_21625__$1;
(statearr_21658_24303[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (14))){
var inst_21603 = (state_21625[(8)]);
var inst_21605 = (state_21625[(11)]);
var inst_21603__$1 = (state_21625[(2)]);
var inst_21604 = (inst_21603__$1 == null);
var inst_21605__$1 = cljs.core.not(inst_21604);
var state_21625__$1 = (function (){var statearr_21659 = state_21625;
(statearr_21659[(8)] = inst_21603__$1);

(statearr_21659[(11)] = inst_21605__$1);

return statearr_21659;
})();
if(inst_21605__$1){
var statearr_21660_24307 = state_21625__$1;
(statearr_21660_24307[(1)] = (15));

} else {
var statearr_21661_24312 = state_21625__$1;
(statearr_21661_24312[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (16))){
var inst_21605 = (state_21625[(11)]);
var state_21625__$1 = state_21625;
var statearr_21686_24313 = state_21625__$1;
(statearr_21686_24313[(2)] = inst_21605);

(statearr_21686_24313[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (10))){
var inst_21594 = (state_21625[(2)]);
var state_21625__$1 = state_21625;
var statearr_21687_24318 = state_21625__$1;
(statearr_21687_24318[(2)] = inst_21594);

(statearr_21687_24318[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (18))){
var inst_21608 = (state_21625[(2)]);
var state_21625__$1 = state_21625;
var statearr_21688_24319 = state_21625__$1;
(statearr_21688_24319[(2)] = inst_21608);

(statearr_21688_24319[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21626 === (8))){
var inst_21591 = cljs.core.async.close_BANG_(to);
var state_21625__$1 = state_21625;
var statearr_21689_24320 = state_21625__$1;
(statearr_21689_24320[(2)] = inst_21591);

(statearr_21689_24320[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0 = (function (){
var statearr_21695 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21695[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__);

(statearr_21695[(1)] = (1));

return statearr_21695;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1 = (function (state_21625){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21625);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21698){var ex__19584__auto__ = e21698;
var statearr_21699_24324 = state_21625;
(statearr_21699_24324[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21625[(4)]))){
var statearr_21700_24325 = state_21625;
(statearr_21700_24325[(1)] = cljs.core.first((state_21625[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24329 = state_21625;
state_21625 = G__24329;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__ = function(state_21625){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1.call(this,state_21625);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19581__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21704 = f__20564__auto__();
(statearr_21704[(6)] = c__20563__auto__);

return statearr_21704;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

return c__20563__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). The
 *   presumption is that af will return immediately, having launched some
 *   asynchronous operation whose completion/callback will put results on
 *   the channel, then close! it. Outputs will be returned in order
 *   relative to the inputs. By default, the to channel will be closed
 *   when the from channel closes, but can be determined by the close?
 *   parameter. Will stop consuming the from channel if the to channel
 *   closes. See also pipeline, pipeline-blocking.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__21710 = arguments.length;
switch (G__21710) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__21720 = arguments.length;
switch (G__21720) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__21725 = arguments.length;
switch (G__21725) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__20563__auto___24337 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21757){
var state_val_21758 = (state_21757[(1)]);
if((state_val_21758 === (7))){
var inst_21752 = (state_21757[(2)]);
var state_21757__$1 = state_21757;
var statearr_21760_24339 = state_21757__$1;
(statearr_21760_24339[(2)] = inst_21752);

(statearr_21760_24339[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (1))){
var state_21757__$1 = state_21757;
var statearr_21761_24340 = state_21757__$1;
(statearr_21761_24340[(2)] = null);

(statearr_21761_24340[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (4))){
var inst_21733 = (state_21757[(7)]);
var inst_21733__$1 = (state_21757[(2)]);
var inst_21734 = (inst_21733__$1 == null);
var state_21757__$1 = (function (){var statearr_21762 = state_21757;
(statearr_21762[(7)] = inst_21733__$1);

return statearr_21762;
})();
if(cljs.core.truth_(inst_21734)){
var statearr_21763_24342 = state_21757__$1;
(statearr_21763_24342[(1)] = (5));

} else {
var statearr_21770_24343 = state_21757__$1;
(statearr_21770_24343[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (13))){
var state_21757__$1 = state_21757;
var statearr_21772_24344 = state_21757__$1;
(statearr_21772_24344[(2)] = null);

(statearr_21772_24344[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (6))){
var inst_21733 = (state_21757[(7)]);
var inst_21739 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_21733) : p.call(null,inst_21733));
var state_21757__$1 = state_21757;
if(cljs.core.truth_(inst_21739)){
var statearr_21774_24345 = state_21757__$1;
(statearr_21774_24345[(1)] = (9));

} else {
var statearr_21775_24346 = state_21757__$1;
(statearr_21775_24346[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (3))){
var inst_21754 = (state_21757[(2)]);
var state_21757__$1 = state_21757;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21757__$1,inst_21754);
} else {
if((state_val_21758 === (12))){
var state_21757__$1 = state_21757;
var statearr_21776_24347 = state_21757__$1;
(statearr_21776_24347[(2)] = null);

(statearr_21776_24347[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (2))){
var state_21757__$1 = state_21757;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21757__$1,(4),ch);
} else {
if((state_val_21758 === (11))){
var inst_21733 = (state_21757[(7)]);
var inst_21743 = (state_21757[(2)]);
var state_21757__$1 = state_21757;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_21757__$1,(8),inst_21743,inst_21733);
} else {
if((state_val_21758 === (9))){
var state_21757__$1 = state_21757;
var statearr_21780_24348 = state_21757__$1;
(statearr_21780_24348[(2)] = tc);

(statearr_21780_24348[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (5))){
var inst_21736 = cljs.core.async.close_BANG_(tc);
var inst_21737 = cljs.core.async.close_BANG_(fc);
var state_21757__$1 = (function (){var statearr_21781 = state_21757;
(statearr_21781[(8)] = inst_21736);

return statearr_21781;
})();
var statearr_21783_24349 = state_21757__$1;
(statearr_21783_24349[(2)] = inst_21737);

(statearr_21783_24349[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (14))){
var inst_21750 = (state_21757[(2)]);
var state_21757__$1 = state_21757;
var statearr_21786_24350 = state_21757__$1;
(statearr_21786_24350[(2)] = inst_21750);

(statearr_21786_24350[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (10))){
var state_21757__$1 = state_21757;
var statearr_21788_24351 = state_21757__$1;
(statearr_21788_24351[(2)] = fc);

(statearr_21788_24351[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21758 === (8))){
var inst_21745 = (state_21757[(2)]);
var state_21757__$1 = state_21757;
if(cljs.core.truth_(inst_21745)){
var statearr_21792_24352 = state_21757__$1;
(statearr_21792_24352[(1)] = (12));

} else {
var statearr_21794_24353 = state_21757__$1;
(statearr_21794_24353[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_21796 = [null,null,null,null,null,null,null,null,null];
(statearr_21796[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_21796[(1)] = (1));

return statearr_21796;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_21757){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21757);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21797){var ex__19584__auto__ = e21797;
var statearr_21798_24358 = state_21757;
(statearr_21798_24358[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21757[(4)]))){
var statearr_21800_24362 = state_21757;
(statearr_21800_24362[(1)] = cljs.core.first((state_21757[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24363 = state_21757;
state_21757 = G__24363;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_21757){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_21757);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21804 = f__20564__auto__();
(statearr_21804[(6)] = c__20563__auto___24337);

return statearr_21804;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__20563__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21874){
var state_val_21875 = (state_21874[(1)]);
if((state_val_21875 === (7))){
var inst_21870 = (state_21874[(2)]);
var state_21874__$1 = state_21874;
var statearr_21883_24371 = state_21874__$1;
(statearr_21883_24371[(2)] = inst_21870);

(statearr_21883_24371[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (1))){
var inst_21836 = init;
var inst_21844 = inst_21836;
var state_21874__$1 = (function (){var statearr_21884 = state_21874;
(statearr_21884[(7)] = inst_21844);

return statearr_21884;
})();
var statearr_21885_24373 = state_21874__$1;
(statearr_21885_24373[(2)] = null);

(statearr_21885_24373[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (4))){
var inst_21856 = (state_21874[(8)]);
var inst_21856__$1 = (state_21874[(2)]);
var inst_21857 = (inst_21856__$1 == null);
var state_21874__$1 = (function (){var statearr_21887 = state_21874;
(statearr_21887[(8)] = inst_21856__$1);

return statearr_21887;
})();
if(cljs.core.truth_(inst_21857)){
var statearr_21888_24377 = state_21874__$1;
(statearr_21888_24377[(1)] = (5));

} else {
var statearr_21889_24378 = state_21874__$1;
(statearr_21889_24378[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (6))){
var inst_21856 = (state_21874[(8)]);
var inst_21860 = (state_21874[(9)]);
var inst_21844 = (state_21874[(7)]);
var inst_21860__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_21844,inst_21856) : f.call(null,inst_21844,inst_21856));
var inst_21861 = cljs.core.reduced_QMARK_(inst_21860__$1);
var state_21874__$1 = (function (){var statearr_21896 = state_21874;
(statearr_21896[(9)] = inst_21860__$1);

return statearr_21896;
})();
if(inst_21861){
var statearr_21898_24382 = state_21874__$1;
(statearr_21898_24382[(1)] = (8));

} else {
var statearr_21901_24383 = state_21874__$1;
(statearr_21901_24383[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (3))){
var inst_21872 = (state_21874[(2)]);
var state_21874__$1 = state_21874;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21874__$1,inst_21872);
} else {
if((state_val_21875 === (2))){
var state_21874__$1 = state_21874;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21874__$1,(4),ch);
} else {
if((state_val_21875 === (9))){
var inst_21860 = (state_21874[(9)]);
var inst_21844 = inst_21860;
var state_21874__$1 = (function (){var statearr_21909 = state_21874;
(statearr_21909[(7)] = inst_21844);

return statearr_21909;
})();
var statearr_21910_24384 = state_21874__$1;
(statearr_21910_24384[(2)] = null);

(statearr_21910_24384[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (5))){
var inst_21844 = (state_21874[(7)]);
var state_21874__$1 = state_21874;
var statearr_21911_24389 = state_21874__$1;
(statearr_21911_24389[(2)] = inst_21844);

(statearr_21911_24389[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (10))){
var inst_21868 = (state_21874[(2)]);
var state_21874__$1 = state_21874;
var statearr_21912_24390 = state_21874__$1;
(statearr_21912_24390[(2)] = inst_21868);

(statearr_21912_24390[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21875 === (8))){
var inst_21860 = (state_21874[(9)]);
var inst_21864 = cljs.core.deref(inst_21860);
var state_21874__$1 = state_21874;
var statearr_21913_24395 = state_21874__$1;
(statearr_21913_24395[(2)] = inst_21864);

(statearr_21913_24395[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__19581__auto__ = null;
var cljs$core$async$reduce_$_state_machine__19581__auto____0 = (function (){
var statearr_21918 = [null,null,null,null,null,null,null,null,null,null];
(statearr_21918[(0)] = cljs$core$async$reduce_$_state_machine__19581__auto__);

(statearr_21918[(1)] = (1));

return statearr_21918;
});
var cljs$core$async$reduce_$_state_machine__19581__auto____1 = (function (state_21874){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21874);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21920){var ex__19584__auto__ = e21920;
var statearr_21921_24399 = state_21874;
(statearr_21921_24399[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21874[(4)]))){
var statearr_21922_24400 = state_21874;
(statearr_21922_24400[(1)] = cljs.core.first((state_21874[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24401 = state_21874;
state_21874 = G__24401;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__19581__auto__ = function(state_21874){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__19581__auto____1.call(this,state_21874);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__19581__auto____0;
cljs$core$async$reduce_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__19581__auto____1;
return cljs$core$async$reduce_$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21925 = f__20564__auto__();
(statearr_21925[(6)] = c__20563__auto__);

return statearr_21925;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

return c__20563__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__20563__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_21934){
var state_val_21935 = (state_21934[(1)]);
if((state_val_21935 === (1))){
var inst_21929 = cljs.core.async.reduce(f__$1,init,ch);
var state_21934__$1 = state_21934;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_21934__$1,(2),inst_21929);
} else {
if((state_val_21935 === (2))){
var inst_21931 = (state_21934[(2)]);
var inst_21932 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_21931) : f__$1.call(null,inst_21931));
var state_21934__$1 = state_21934;
return cljs.core.async.impl.ioc_helpers.return_chan(state_21934__$1,inst_21932);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__19581__auto__ = null;
var cljs$core$async$transduce_$_state_machine__19581__auto____0 = (function (){
var statearr_21960 = [null,null,null,null,null,null,null];
(statearr_21960[(0)] = cljs$core$async$transduce_$_state_machine__19581__auto__);

(statearr_21960[(1)] = (1));

return statearr_21960;
});
var cljs$core$async$transduce_$_state_machine__19581__auto____1 = (function (state_21934){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_21934);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e21961){var ex__19584__auto__ = e21961;
var statearr_21962_24412 = state_21934;
(statearr_21962_24412[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_21934[(4)]))){
var statearr_21963_24413 = state_21934;
(statearr_21963_24413[(1)] = cljs.core.first((state_21934[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24414 = state_21934;
state_21934 = G__24414;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__19581__auto__ = function(state_21934){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__19581__auto____1.call(this,state_21934);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__19581__auto____0;
cljs$core$async$transduce_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__19581__auto____1;
return cljs$core$async$transduce_$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_21981 = f__20564__auto__();
(statearr_21981[(6)] = c__20563__auto__);

return statearr_21981;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

return c__20563__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__21989 = arguments.length;
switch (G__21989) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__20563__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_22024){
var state_val_22025 = (state_22024[(1)]);
if((state_val_22025 === (7))){
var inst_22006 = (state_22024[(2)]);
var state_22024__$1 = state_22024;
var statearr_22029_24417 = state_22024__$1;
(statearr_22029_24417[(2)] = inst_22006);

(statearr_22029_24417[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (1))){
var inst_21999 = cljs.core.seq(coll);
var inst_22000 = inst_21999;
var state_22024__$1 = (function (){var statearr_22034 = state_22024;
(statearr_22034[(7)] = inst_22000);

return statearr_22034;
})();
var statearr_22035_24418 = state_22024__$1;
(statearr_22035_24418[(2)] = null);

(statearr_22035_24418[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (4))){
var inst_22000 = (state_22024[(7)]);
var inst_22004 = cljs.core.first(inst_22000);
var state_22024__$1 = state_22024;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22024__$1,(7),ch,inst_22004);
} else {
if((state_val_22025 === (13))){
var inst_22018 = (state_22024[(2)]);
var state_22024__$1 = state_22024;
var statearr_22041_24422 = state_22024__$1;
(statearr_22041_24422[(2)] = inst_22018);

(statearr_22041_24422[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (6))){
var inst_22009 = (state_22024[(2)]);
var state_22024__$1 = state_22024;
if(cljs.core.truth_(inst_22009)){
var statearr_22043_24426 = state_22024__$1;
(statearr_22043_24426[(1)] = (8));

} else {
var statearr_22044_24427 = state_22024__$1;
(statearr_22044_24427[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (3))){
var inst_22022 = (state_22024[(2)]);
var state_22024__$1 = state_22024;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22024__$1,inst_22022);
} else {
if((state_val_22025 === (12))){
var state_22024__$1 = state_22024;
var statearr_22046_24428 = state_22024__$1;
(statearr_22046_24428[(2)] = null);

(statearr_22046_24428[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (2))){
var inst_22000 = (state_22024[(7)]);
var state_22024__$1 = state_22024;
if(cljs.core.truth_(inst_22000)){
var statearr_22047_24429 = state_22024__$1;
(statearr_22047_24429[(1)] = (4));

} else {
var statearr_22049_24430 = state_22024__$1;
(statearr_22049_24430[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (11))){
var inst_22015 = cljs.core.async.close_BANG_(ch);
var state_22024__$1 = state_22024;
var statearr_22050_24431 = state_22024__$1;
(statearr_22050_24431[(2)] = inst_22015);

(statearr_22050_24431[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (9))){
var state_22024__$1 = state_22024;
if(cljs.core.truth_(close_QMARK_)){
var statearr_22052_24432 = state_22024__$1;
(statearr_22052_24432[(1)] = (11));

} else {
var statearr_22053_24433 = state_22024__$1;
(statearr_22053_24433[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (5))){
var inst_22000 = (state_22024[(7)]);
var state_22024__$1 = state_22024;
var statearr_22055_24437 = state_22024__$1;
(statearr_22055_24437[(2)] = inst_22000);

(statearr_22055_24437[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (10))){
var inst_22020 = (state_22024[(2)]);
var state_22024__$1 = state_22024;
var statearr_22056_24438 = state_22024__$1;
(statearr_22056_24438[(2)] = inst_22020);

(statearr_22056_24438[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22025 === (8))){
var inst_22000 = (state_22024[(7)]);
var inst_22011 = cljs.core.next(inst_22000);
var inst_22000__$1 = inst_22011;
var state_22024__$1 = (function (){var statearr_22057 = state_22024;
(statearr_22057[(7)] = inst_22000__$1);

return statearr_22057;
})();
var statearr_22059_24442 = state_22024__$1;
(statearr_22059_24442[(2)] = null);

(statearr_22059_24442[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_22060 = [null,null,null,null,null,null,null,null];
(statearr_22060[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_22060[(1)] = (1));

return statearr_22060;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_22024){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_22024);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e22061){var ex__19584__auto__ = e22061;
var statearr_22062_24443 = state_22024;
(statearr_22062_24443[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_22024[(4)]))){
var statearr_22063_24447 = state_22024;
(statearr_22063_24447[(1)] = cljs.core.first((state_22024[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24450 = state_22024;
state_22024 = G__24450;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_22024){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_22024);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_22064 = f__20564__auto__();
(statearr_22064[(6)] = c__20563__auto__);

return statearr_22064;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

return c__20563__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__22067 = arguments.length;
switch (G__22067) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_24456 = (function (_){
var x__5350__auto__ = (((_ == null))?null:_);
var m__5351__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5351__auto__.call(null,_));
} else {
var m__5349__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5349__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_24456(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_24460 = (function (m,ch,close_QMARK_){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5351__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__5349__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5349__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_24460(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_24464 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null,m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_24464(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_24469 = (function (m){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5351__auto__.call(null,m));
} else {
var m__5349__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5349__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_24469(m);
}
});


/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22096 = (function (ch,cs,meta22097){
this.ch = ch;
this.cs = cs;
this.meta22097 = meta22097;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22098,meta22097__$1){
var self__ = this;
var _22098__$1 = this;
return (new cljs.core.async.t_cljs$core$async22096(self__.ch,self__.cs,meta22097__$1));
}));

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22098){
var self__ = this;
var _22098__$1 = this;
return self__.meta22097;
}));

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async22096.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async22096.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta22097","meta22097",1755457454,null)], null);
}));

(cljs.core.async.t_cljs$core$async22096.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22096.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22096");

(cljs.core.async.t_cljs$core$async22096.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22096");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22096.
 */
cljs.core.async.__GT_t_cljs$core$async22096 = (function cljs$core$async$__GT_t_cljs$core$async22096(ch,cs,meta22097){
return (new cljs.core.async.t_cljs$core$async22096(ch,cs,meta22097));
});


/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (new cljs.core.async.t_cljs$core$async22096(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__20563__auto___24478 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_22252){
var state_val_22253 = (state_22252[(1)]);
if((state_val_22253 === (7))){
var inst_22248 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22254_24479 = state_22252__$1;
(statearr_22254_24479[(2)] = inst_22248);

(statearr_22254_24479[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (20))){
var inst_22144 = (state_22252[(7)]);
var inst_22156 = cljs.core.first(inst_22144);
var inst_22157 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22156,(0),null);
var inst_22158 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22156,(1),null);
var state_22252__$1 = (function (){var statearr_22255 = state_22252;
(statearr_22255[(8)] = inst_22157);

return statearr_22255;
})();
if(cljs.core.truth_(inst_22158)){
var statearr_22256_24481 = state_22252__$1;
(statearr_22256_24481[(1)] = (22));

} else {
var statearr_22257_24482 = state_22252__$1;
(statearr_22257_24482[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (27))){
var inst_22190 = (state_22252[(9)]);
var inst_22192 = (state_22252[(10)]);
var inst_22197 = (state_22252[(11)]);
var inst_22110 = (state_22252[(12)]);
var inst_22197__$1 = cljs.core._nth(inst_22190,inst_22192);
var inst_22198 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_22197__$1,inst_22110,done);
var state_22252__$1 = (function (){var statearr_22258 = state_22252;
(statearr_22258[(11)] = inst_22197__$1);

return statearr_22258;
})();
if(cljs.core.truth_(inst_22198)){
var statearr_22259_24486 = state_22252__$1;
(statearr_22259_24486[(1)] = (30));

} else {
var statearr_22260_24487 = state_22252__$1;
(statearr_22260_24487[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (1))){
var state_22252__$1 = state_22252;
var statearr_22261_24490 = state_22252__$1;
(statearr_22261_24490[(2)] = null);

(statearr_22261_24490[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (24))){
var inst_22144 = (state_22252[(7)]);
var inst_22163 = (state_22252[(2)]);
var inst_22165 = cljs.core.next(inst_22144);
var inst_22120 = inst_22165;
var inst_22121 = null;
var inst_22122 = (0);
var inst_22123 = (0);
var state_22252__$1 = (function (){var statearr_22263 = state_22252;
(statearr_22263[(13)] = inst_22163);

(statearr_22263[(14)] = inst_22122);

(statearr_22263[(15)] = inst_22121);

(statearr_22263[(16)] = inst_22123);

(statearr_22263[(17)] = inst_22120);

return statearr_22263;
})();
var statearr_22264_24493 = state_22252__$1;
(statearr_22264_24493[(2)] = null);

(statearr_22264_24493[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (39))){
var state_22252__$1 = state_22252;
var statearr_22268_24494 = state_22252__$1;
(statearr_22268_24494[(2)] = null);

(statearr_22268_24494[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (4))){
var inst_22110 = (state_22252[(12)]);
var inst_22110__$1 = (state_22252[(2)]);
var inst_22111 = (inst_22110__$1 == null);
var state_22252__$1 = (function (){var statearr_22269 = state_22252;
(statearr_22269[(12)] = inst_22110__$1);

return statearr_22269;
})();
if(cljs.core.truth_(inst_22111)){
var statearr_22270_24498 = state_22252__$1;
(statearr_22270_24498[(1)] = (5));

} else {
var statearr_22271_24503 = state_22252__$1;
(statearr_22271_24503[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (15))){
var inst_22122 = (state_22252[(14)]);
var inst_22121 = (state_22252[(15)]);
var inst_22123 = (state_22252[(16)]);
var inst_22120 = (state_22252[(17)]);
var inst_22140 = (state_22252[(2)]);
var inst_22141 = (inst_22123 + (1));
var tmp22265 = inst_22122;
var tmp22266 = inst_22121;
var tmp22267 = inst_22120;
var inst_22120__$1 = tmp22267;
var inst_22121__$1 = tmp22266;
var inst_22122__$1 = tmp22265;
var inst_22123__$1 = inst_22141;
var state_22252__$1 = (function (){var statearr_22277 = state_22252;
(statearr_22277[(18)] = inst_22140);

(statearr_22277[(14)] = inst_22122__$1);

(statearr_22277[(15)] = inst_22121__$1);

(statearr_22277[(16)] = inst_22123__$1);

(statearr_22277[(17)] = inst_22120__$1);

return statearr_22277;
})();
var statearr_22281_24510 = state_22252__$1;
(statearr_22281_24510[(2)] = null);

(statearr_22281_24510[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (21))){
var inst_22168 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22286_24511 = state_22252__$1;
(statearr_22286_24511[(2)] = inst_22168);

(statearr_22286_24511[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (31))){
var inst_22197 = (state_22252[(11)]);
var inst_22201 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_22197);
var state_22252__$1 = state_22252;
var statearr_22289_24513 = state_22252__$1;
(statearr_22289_24513[(2)] = inst_22201);

(statearr_22289_24513[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (32))){
var inst_22189 = (state_22252[(19)]);
var inst_22190 = (state_22252[(9)]);
var inst_22191 = (state_22252[(20)]);
var inst_22192 = (state_22252[(10)]);
var inst_22203 = (state_22252[(2)]);
var inst_22204 = (inst_22192 + (1));
var tmp22283 = inst_22189;
var tmp22284 = inst_22190;
var tmp22285 = inst_22191;
var inst_22189__$1 = tmp22283;
var inst_22190__$1 = tmp22284;
var inst_22191__$1 = tmp22285;
var inst_22192__$1 = inst_22204;
var state_22252__$1 = (function (){var statearr_22290 = state_22252;
(statearr_22290[(19)] = inst_22189__$1);

(statearr_22290[(9)] = inst_22190__$1);

(statearr_22290[(21)] = inst_22203);

(statearr_22290[(20)] = inst_22191__$1);

(statearr_22290[(10)] = inst_22192__$1);

return statearr_22290;
})();
var statearr_22291_24514 = state_22252__$1;
(statearr_22291_24514[(2)] = null);

(statearr_22291_24514[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (40))){
var inst_22219 = (state_22252[(22)]);
var inst_22223 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_22219);
var state_22252__$1 = state_22252;
var statearr_22292_24515 = state_22252__$1;
(statearr_22292_24515[(2)] = inst_22223);

(statearr_22292_24515[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (33))){
var inst_22207 = (state_22252[(23)]);
var inst_22209 = cljs.core.chunked_seq_QMARK_(inst_22207);
var state_22252__$1 = state_22252;
if(inst_22209){
var statearr_22307_24516 = state_22252__$1;
(statearr_22307_24516[(1)] = (36));

} else {
var statearr_22308_24517 = state_22252__$1;
(statearr_22308_24517[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (13))){
var inst_22134 = (state_22252[(24)]);
var inst_22137 = cljs.core.async.close_BANG_(inst_22134);
var state_22252__$1 = state_22252;
var statearr_22309_24518 = state_22252__$1;
(statearr_22309_24518[(2)] = inst_22137);

(statearr_22309_24518[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (22))){
var inst_22157 = (state_22252[(8)]);
var inst_22160 = cljs.core.async.close_BANG_(inst_22157);
var state_22252__$1 = state_22252;
var statearr_22310_24519 = state_22252__$1;
(statearr_22310_24519[(2)] = inst_22160);

(statearr_22310_24519[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (36))){
var inst_22207 = (state_22252[(23)]);
var inst_22214 = cljs.core.chunk_first(inst_22207);
var inst_22215 = cljs.core.chunk_rest(inst_22207);
var inst_22216 = cljs.core.count(inst_22214);
var inst_22189 = inst_22215;
var inst_22190 = inst_22214;
var inst_22191 = inst_22216;
var inst_22192 = (0);
var state_22252__$1 = (function (){var statearr_22318 = state_22252;
(statearr_22318[(19)] = inst_22189);

(statearr_22318[(9)] = inst_22190);

(statearr_22318[(20)] = inst_22191);

(statearr_22318[(10)] = inst_22192);

return statearr_22318;
})();
var statearr_22322_24521 = state_22252__$1;
(statearr_22322_24521[(2)] = null);

(statearr_22322_24521[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (41))){
var inst_22207 = (state_22252[(23)]);
var inst_22225 = (state_22252[(2)]);
var inst_22226 = cljs.core.next(inst_22207);
var inst_22189 = inst_22226;
var inst_22190 = null;
var inst_22191 = (0);
var inst_22192 = (0);
var state_22252__$1 = (function (){var statearr_22323 = state_22252;
(statearr_22323[(19)] = inst_22189);

(statearr_22323[(9)] = inst_22190);

(statearr_22323[(20)] = inst_22191);

(statearr_22323[(10)] = inst_22192);

(statearr_22323[(25)] = inst_22225);

return statearr_22323;
})();
var statearr_22329_24531 = state_22252__$1;
(statearr_22329_24531[(2)] = null);

(statearr_22329_24531[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (43))){
var state_22252__$1 = state_22252;
var statearr_22331_24532 = state_22252__$1;
(statearr_22331_24532[(2)] = null);

(statearr_22331_24532[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (29))){
var inst_22236 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22332_24533 = state_22252__$1;
(statearr_22332_24533[(2)] = inst_22236);

(statearr_22332_24533[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (44))){
var inst_22245 = (state_22252[(2)]);
var state_22252__$1 = (function (){var statearr_22333 = state_22252;
(statearr_22333[(26)] = inst_22245);

return statearr_22333;
})();
var statearr_22334_24534 = state_22252__$1;
(statearr_22334_24534[(2)] = null);

(statearr_22334_24534[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (6))){
var inst_22180 = (state_22252[(27)]);
var inst_22179 = cljs.core.deref(cs);
var inst_22180__$1 = cljs.core.keys(inst_22179);
var inst_22181 = cljs.core.count(inst_22180__$1);
var inst_22182 = cljs.core.reset_BANG_(dctr,inst_22181);
var inst_22188 = cljs.core.seq(inst_22180__$1);
var inst_22189 = inst_22188;
var inst_22190 = null;
var inst_22191 = (0);
var inst_22192 = (0);
var state_22252__$1 = (function (){var statearr_22339 = state_22252;
(statearr_22339[(19)] = inst_22189);

(statearr_22339[(27)] = inst_22180__$1);

(statearr_22339[(9)] = inst_22190);

(statearr_22339[(20)] = inst_22191);

(statearr_22339[(10)] = inst_22192);

(statearr_22339[(28)] = inst_22182);

return statearr_22339;
})();
var statearr_22344_24535 = state_22252__$1;
(statearr_22344_24535[(2)] = null);

(statearr_22344_24535[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (28))){
var inst_22189 = (state_22252[(19)]);
var inst_22207 = (state_22252[(23)]);
var inst_22207__$1 = cljs.core.seq(inst_22189);
var state_22252__$1 = (function (){var statearr_22348 = state_22252;
(statearr_22348[(23)] = inst_22207__$1);

return statearr_22348;
})();
if(inst_22207__$1){
var statearr_22349_24536 = state_22252__$1;
(statearr_22349_24536[(1)] = (33));

} else {
var statearr_22350_24537 = state_22252__$1;
(statearr_22350_24537[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (25))){
var inst_22191 = (state_22252[(20)]);
var inst_22192 = (state_22252[(10)]);
var inst_22194 = (inst_22192 < inst_22191);
var inst_22195 = inst_22194;
var state_22252__$1 = state_22252;
if(cljs.core.truth_(inst_22195)){
var statearr_22354_24538 = state_22252__$1;
(statearr_22354_24538[(1)] = (27));

} else {
var statearr_22355_24539 = state_22252__$1;
(statearr_22355_24539[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (34))){
var state_22252__$1 = state_22252;
var statearr_22356_24540 = state_22252__$1;
(statearr_22356_24540[(2)] = null);

(statearr_22356_24540[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (17))){
var state_22252__$1 = state_22252;
var statearr_22358_24541 = state_22252__$1;
(statearr_22358_24541[(2)] = null);

(statearr_22358_24541[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (3))){
var inst_22250 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22252__$1,inst_22250);
} else {
if((state_val_22253 === (12))){
var inst_22173 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22362_24543 = state_22252__$1;
(statearr_22362_24543[(2)] = inst_22173);

(statearr_22362_24543[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (2))){
var state_22252__$1 = state_22252;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22252__$1,(4),ch);
} else {
if((state_val_22253 === (23))){
var state_22252__$1 = state_22252;
var statearr_22367_24545 = state_22252__$1;
(statearr_22367_24545[(2)] = null);

(statearr_22367_24545[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (35))){
var inst_22232 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22371_24546 = state_22252__$1;
(statearr_22371_24546[(2)] = inst_22232);

(statearr_22371_24546[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (19))){
var inst_22144 = (state_22252[(7)]);
var inst_22148 = cljs.core.chunk_first(inst_22144);
var inst_22149 = cljs.core.chunk_rest(inst_22144);
var inst_22150 = cljs.core.count(inst_22148);
var inst_22120 = inst_22149;
var inst_22121 = inst_22148;
var inst_22122 = inst_22150;
var inst_22123 = (0);
var state_22252__$1 = (function (){var statearr_22375 = state_22252;
(statearr_22375[(14)] = inst_22122);

(statearr_22375[(15)] = inst_22121);

(statearr_22375[(16)] = inst_22123);

(statearr_22375[(17)] = inst_22120);

return statearr_22375;
})();
var statearr_22376_24554 = state_22252__$1;
(statearr_22376_24554[(2)] = null);

(statearr_22376_24554[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (11))){
var inst_22144 = (state_22252[(7)]);
var inst_22120 = (state_22252[(17)]);
var inst_22144__$1 = cljs.core.seq(inst_22120);
var state_22252__$1 = (function (){var statearr_22381 = state_22252;
(statearr_22381[(7)] = inst_22144__$1);

return statearr_22381;
})();
if(inst_22144__$1){
var statearr_22382_24559 = state_22252__$1;
(statearr_22382_24559[(1)] = (16));

} else {
var statearr_22383_24560 = state_22252__$1;
(statearr_22383_24560[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (9))){
var inst_22176 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22384_24561 = state_22252__$1;
(statearr_22384_24561[(2)] = inst_22176);

(statearr_22384_24561[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (5))){
var inst_22118 = cljs.core.deref(cs);
var inst_22119 = cljs.core.seq(inst_22118);
var inst_22120 = inst_22119;
var inst_22121 = null;
var inst_22122 = (0);
var inst_22123 = (0);
var state_22252__$1 = (function (){var statearr_22385 = state_22252;
(statearr_22385[(14)] = inst_22122);

(statearr_22385[(15)] = inst_22121);

(statearr_22385[(16)] = inst_22123);

(statearr_22385[(17)] = inst_22120);

return statearr_22385;
})();
var statearr_22386_24562 = state_22252__$1;
(statearr_22386_24562[(2)] = null);

(statearr_22386_24562[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (14))){
var state_22252__$1 = state_22252;
var statearr_22389_24563 = state_22252__$1;
(statearr_22389_24563[(2)] = null);

(statearr_22389_24563[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (45))){
var inst_22242 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22390_24564 = state_22252__$1;
(statearr_22390_24564[(2)] = inst_22242);

(statearr_22390_24564[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (26))){
var inst_22180 = (state_22252[(27)]);
var inst_22238 = (state_22252[(2)]);
var inst_22239 = cljs.core.seq(inst_22180);
var state_22252__$1 = (function (){var statearr_22394 = state_22252;
(statearr_22394[(29)] = inst_22238);

return statearr_22394;
})();
if(inst_22239){
var statearr_22395_24565 = state_22252__$1;
(statearr_22395_24565[(1)] = (42));

} else {
var statearr_22396_24566 = state_22252__$1;
(statearr_22396_24566[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (16))){
var inst_22144 = (state_22252[(7)]);
var inst_22146 = cljs.core.chunked_seq_QMARK_(inst_22144);
var state_22252__$1 = state_22252;
if(inst_22146){
var statearr_22397_24568 = state_22252__$1;
(statearr_22397_24568[(1)] = (19));

} else {
var statearr_22398_24569 = state_22252__$1;
(statearr_22398_24569[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (38))){
var inst_22229 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22399_24571 = state_22252__$1;
(statearr_22399_24571[(2)] = inst_22229);

(statearr_22399_24571[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (30))){
var state_22252__$1 = state_22252;
var statearr_22400_24572 = state_22252__$1;
(statearr_22400_24572[(2)] = null);

(statearr_22400_24572[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (10))){
var inst_22121 = (state_22252[(15)]);
var inst_22123 = (state_22252[(16)]);
var inst_22133 = cljs.core._nth(inst_22121,inst_22123);
var inst_22134 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22133,(0),null);
var inst_22135 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22133,(1),null);
var state_22252__$1 = (function (){var statearr_22401 = state_22252;
(statearr_22401[(24)] = inst_22134);

return statearr_22401;
})();
if(cljs.core.truth_(inst_22135)){
var statearr_22402_24573 = state_22252__$1;
(statearr_22402_24573[(1)] = (13));

} else {
var statearr_22403_24574 = state_22252__$1;
(statearr_22403_24574[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (18))){
var inst_22171 = (state_22252[(2)]);
var state_22252__$1 = state_22252;
var statearr_22404_24575 = state_22252__$1;
(statearr_22404_24575[(2)] = inst_22171);

(statearr_22404_24575[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (42))){
var state_22252__$1 = state_22252;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22252__$1,(45),dchan);
} else {
if((state_val_22253 === (37))){
var inst_22207 = (state_22252[(23)]);
var inst_22219 = (state_22252[(22)]);
var inst_22110 = (state_22252[(12)]);
var inst_22219__$1 = cljs.core.first(inst_22207);
var inst_22220 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_22219__$1,inst_22110,done);
var state_22252__$1 = (function (){var statearr_22405 = state_22252;
(statearr_22405[(22)] = inst_22219__$1);

return statearr_22405;
})();
if(cljs.core.truth_(inst_22220)){
var statearr_22406_24576 = state_22252__$1;
(statearr_22406_24576[(1)] = (39));

} else {
var statearr_22407_24577 = state_22252__$1;
(statearr_22407_24577[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22253 === (8))){
var inst_22122 = (state_22252[(14)]);
var inst_22123 = (state_22252[(16)]);
var inst_22126 = (inst_22123 < inst_22122);
var inst_22127 = inst_22126;
var state_22252__$1 = state_22252;
if(cljs.core.truth_(inst_22127)){
var statearr_22408_24580 = state_22252__$1;
(statearr_22408_24580[(1)] = (10));

} else {
var statearr_22409_24581 = state_22252__$1;
(statearr_22409_24581[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__19581__auto__ = null;
var cljs$core$async$mult_$_state_machine__19581__auto____0 = (function (){
var statearr_22410 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22410[(0)] = cljs$core$async$mult_$_state_machine__19581__auto__);

(statearr_22410[(1)] = (1));

return statearr_22410;
});
var cljs$core$async$mult_$_state_machine__19581__auto____1 = (function (state_22252){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_22252);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e22414){var ex__19584__auto__ = e22414;
var statearr_22415_24592 = state_22252;
(statearr_22415_24592[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_22252[(4)]))){
var statearr_22416_24593 = state_22252;
(statearr_22416_24593[(1)] = cljs.core.first((state_22252[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24595 = state_22252;
state_22252 = G__24595;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__19581__auto__ = function(state_22252){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__19581__auto____1.call(this,state_22252);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__19581__auto____0;
cljs$core$async$mult_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__19581__auto____1;
return cljs$core$async$mult_$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_22417 = f__20564__auto__();
(statearr_22417[(6)] = c__20563__auto___24478);

return statearr_22417;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__22419 = arguments.length;
switch (G__22419) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_24603 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null,m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_24603(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_24605 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null,m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_24605(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_24609 = (function (m){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5351__auto__.call(null,m));
} else {
var m__5349__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5349__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_24609(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_24610 = (function (m,state_map){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5351__auto__.call(null,m,state_map));
} else {
var m__5349__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5349__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_24610(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_24611 = (function (m,mode){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5351__auto__.call(null,m,mode));
} else {
var m__5349__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5349__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_24611(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___24615 = arguments.length;
var i__5727__auto___24616 = (0);
while(true){
if((i__5727__auto___24616 < len__5726__auto___24615)){
args__5732__auto__.push((arguments[i__5727__auto___24616]));

var G__24617 = (i__5727__auto___24616 + (1));
i__5727__auto___24616 = G__24617;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((3) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__5733__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__22464){
var map__22465 = p__22464;
var map__22465__$1 = cljs.core.__destructure_map(map__22465);
var opts = map__22465__$1;
var statearr_22466_24619 = state;
(statearr_22466_24619[(1)] = cont_block);


var temp__5804__auto__ = cljs.core.async.do_alts((function (val){
var statearr_22467_24621 = state;
(statearr_22467_24621[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5804__auto__)){
var cb = temp__5804__auto__;
var statearr_22468_24622 = state;
(statearr_22468_24622[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq22456){
var G__22457 = cljs.core.first(seq22456);
var seq22456__$1 = cljs.core.next(seq22456);
var G__22458 = cljs.core.first(seq22456__$1);
var seq22456__$2 = cljs.core.next(seq22456__$1);
var G__22459 = cljs.core.first(seq22456__$2);
var seq22456__$3 = cljs.core.next(seq22456__$2);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__22457,G__22458,G__22459,seq22456__$3);
}));


/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22472 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta22473){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta22473 = meta22473;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22474,meta22473__$1){
var self__ = this;
var _22474__$1 = this;
return (new cljs.core.async.t_cljs$core$async22472(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta22473__$1));
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22474){
var self__ = this;
var _22474__$1 = this;
return self__.meta22473;
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async22472.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async22472.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta22473","meta22473",-1927419474,null)], null);
}));

(cljs.core.async.t_cljs$core$async22472.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22472.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22472");

(cljs.core.async.t_cljs$core$async22472.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22472");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22472.
 */
cljs.core.async.__GT_t_cljs$core$async22472 = (function cljs$core$async$__GT_t_cljs$core$async22472(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta22473){
return (new cljs.core.async.t_cljs$core$async22472(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta22473));
});


/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (new cljs.core.async.t_cljs$core$async22472(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
var c__20563__auto___24637 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_22569){
var state_val_22570 = (state_22569[(1)]);
if((state_val_22570 === (7))){
var inst_22508 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
if(cljs.core.truth_(inst_22508)){
var statearr_22571_24638 = state_22569__$1;
(statearr_22571_24638[(1)] = (8));

} else {
var statearr_22572_24639 = state_22569__$1;
(statearr_22572_24639[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (20))){
var inst_22501 = (state_22569[(7)]);
var state_22569__$1 = state_22569;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22569__$1,(23),out,inst_22501);
} else {
if((state_val_22570 === (1))){
var inst_22484 = calc_state();
var inst_22485 = cljs.core.__destructure_map(inst_22484);
var inst_22486 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22485,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_22487 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22485,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_22488 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22485,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_22489 = inst_22484;
var state_22569__$1 = (function (){var statearr_22574 = state_22569;
(statearr_22574[(8)] = inst_22489);

(statearr_22574[(9)] = inst_22486);

(statearr_22574[(10)] = inst_22488);

(statearr_22574[(11)] = inst_22487);

return statearr_22574;
})();
var statearr_22575_24641 = state_22569__$1;
(statearr_22575_24641[(2)] = null);

(statearr_22575_24641[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (24))){
var inst_22492 = (state_22569[(12)]);
var inst_22489 = inst_22492;
var state_22569__$1 = (function (){var statearr_22576 = state_22569;
(statearr_22576[(8)] = inst_22489);

return statearr_22576;
})();
var statearr_22577_24643 = state_22569__$1;
(statearr_22577_24643[(2)] = null);

(statearr_22577_24643[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (4))){
var inst_22501 = (state_22569[(7)]);
var inst_22503 = (state_22569[(13)]);
var inst_22500 = (state_22569[(2)]);
var inst_22501__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22500,(0),null);
var inst_22502 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_22500,(1),null);
var inst_22503__$1 = (inst_22501__$1 == null);
var state_22569__$1 = (function (){var statearr_22578 = state_22569;
(statearr_22578[(14)] = inst_22502);

(statearr_22578[(7)] = inst_22501__$1);

(statearr_22578[(13)] = inst_22503__$1);

return statearr_22578;
})();
if(cljs.core.truth_(inst_22503__$1)){
var statearr_22579_24645 = state_22569__$1;
(statearr_22579_24645[(1)] = (5));

} else {
var statearr_22580_24646 = state_22569__$1;
(statearr_22580_24646[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (15))){
var inst_22534 = (state_22569[(15)]);
var inst_22493 = (state_22569[(16)]);
var inst_22534__$1 = cljs.core.empty_QMARK_(inst_22493);
var state_22569__$1 = (function (){var statearr_22581 = state_22569;
(statearr_22581[(15)] = inst_22534__$1);

return statearr_22581;
})();
if(inst_22534__$1){
var statearr_22582_24647 = state_22569__$1;
(statearr_22582_24647[(1)] = (17));

} else {
var statearr_22583_24648 = state_22569__$1;
(statearr_22583_24648[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (21))){
var inst_22492 = (state_22569[(12)]);
var inst_22489 = inst_22492;
var state_22569__$1 = (function (){var statearr_22584 = state_22569;
(statearr_22584[(8)] = inst_22489);

return statearr_22584;
})();
var statearr_22585_24649 = state_22569__$1;
(statearr_22585_24649[(2)] = null);

(statearr_22585_24649[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (13))){
var inst_22515 = (state_22569[(2)]);
var inst_22522 = calc_state();
var inst_22489 = inst_22522;
var state_22569__$1 = (function (){var statearr_22586 = state_22569;
(statearr_22586[(8)] = inst_22489);

(statearr_22586[(17)] = inst_22515);

return statearr_22586;
})();
var statearr_22587_24652 = state_22569__$1;
(statearr_22587_24652[(2)] = null);

(statearr_22587_24652[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (22))){
var inst_22556 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
var statearr_22591_24653 = state_22569__$1;
(statearr_22591_24653[(2)] = inst_22556);

(statearr_22591_24653[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (6))){
var inst_22502 = (state_22569[(14)]);
var inst_22506 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_22502,change);
var state_22569__$1 = state_22569;
var statearr_22599_24654 = state_22569__$1;
(statearr_22599_24654[(2)] = inst_22506);

(statearr_22599_24654[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (25))){
var state_22569__$1 = state_22569;
var statearr_22600_24655 = state_22569__$1;
(statearr_22600_24655[(2)] = null);

(statearr_22600_24655[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (17))){
var inst_22502 = (state_22569[(14)]);
var inst_22494 = (state_22569[(18)]);
var inst_22536 = (inst_22494.cljs$core$IFn$_invoke$arity$1 ? inst_22494.cljs$core$IFn$_invoke$arity$1(inst_22502) : inst_22494.call(null,inst_22502));
var inst_22537 = cljs.core.not(inst_22536);
var state_22569__$1 = state_22569;
var statearr_22604_24657 = state_22569__$1;
(statearr_22604_24657[(2)] = inst_22537);

(statearr_22604_24657[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (3))){
var inst_22567 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22569__$1,inst_22567);
} else {
if((state_val_22570 === (12))){
var state_22569__$1 = state_22569;
var statearr_22608_24660 = state_22569__$1;
(statearr_22608_24660[(2)] = null);

(statearr_22608_24660[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (2))){
var inst_22492 = (state_22569[(12)]);
var inst_22489 = (state_22569[(8)]);
var inst_22492__$1 = cljs.core.__destructure_map(inst_22489);
var inst_22493 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22492__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_22494 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22492__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_22495 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22492__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_22569__$1 = (function (){var statearr_22609 = state_22569;
(statearr_22609[(12)] = inst_22492__$1);

(statearr_22609[(16)] = inst_22493);

(statearr_22609[(18)] = inst_22494);

return statearr_22609;
})();
return cljs.core.async.ioc_alts_BANG_(state_22569__$1,(4),inst_22495);
} else {
if((state_val_22570 === (23))){
var inst_22545 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
if(cljs.core.truth_(inst_22545)){
var statearr_22610_24661 = state_22569__$1;
(statearr_22610_24661[(1)] = (24));

} else {
var statearr_22611_24663 = state_22569__$1;
(statearr_22611_24663[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (19))){
var inst_22540 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
var statearr_22612_24666 = state_22569__$1;
(statearr_22612_24666[(2)] = inst_22540);

(statearr_22612_24666[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (11))){
var inst_22502 = (state_22569[(14)]);
var inst_22512 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_22502);
var state_22569__$1 = state_22569;
var statearr_22615_24667 = state_22569__$1;
(statearr_22615_24667[(2)] = inst_22512);

(statearr_22615_24667[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (9))){
var inst_22531 = (state_22569[(19)]);
var inst_22493 = (state_22569[(16)]);
var inst_22502 = (state_22569[(14)]);
var inst_22531__$1 = (inst_22493.cljs$core$IFn$_invoke$arity$1 ? inst_22493.cljs$core$IFn$_invoke$arity$1(inst_22502) : inst_22493.call(null,inst_22502));
var state_22569__$1 = (function (){var statearr_22616 = state_22569;
(statearr_22616[(19)] = inst_22531__$1);

return statearr_22616;
})();
if(cljs.core.truth_(inst_22531__$1)){
var statearr_22617_24668 = state_22569__$1;
(statearr_22617_24668[(1)] = (14));

} else {
var statearr_22618_24669 = state_22569__$1;
(statearr_22618_24669[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (5))){
var inst_22503 = (state_22569[(13)]);
var state_22569__$1 = state_22569;
var statearr_22619_24670 = state_22569__$1;
(statearr_22619_24670[(2)] = inst_22503);

(statearr_22619_24670[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (14))){
var inst_22531 = (state_22569[(19)]);
var state_22569__$1 = state_22569;
var statearr_22620_24671 = state_22569__$1;
(statearr_22620_24671[(2)] = inst_22531);

(statearr_22620_24671[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (26))){
var inst_22550 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
var statearr_22622_24675 = state_22569__$1;
(statearr_22622_24675[(2)] = inst_22550);

(statearr_22622_24675[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (16))){
var inst_22542 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
if(cljs.core.truth_(inst_22542)){
var statearr_22623_24676 = state_22569__$1;
(statearr_22623_24676[(1)] = (20));

} else {
var statearr_22624_24678 = state_22569__$1;
(statearr_22624_24678[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (10))){
var inst_22559 = (state_22569[(2)]);
var state_22569__$1 = state_22569;
var statearr_22626_24681 = state_22569__$1;
(statearr_22626_24681[(2)] = inst_22559);

(statearr_22626_24681[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (18))){
var inst_22534 = (state_22569[(15)]);
var state_22569__$1 = state_22569;
var statearr_22627_24682 = state_22569__$1;
(statearr_22627_24682[(2)] = inst_22534);

(statearr_22627_24682[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22570 === (8))){
var inst_22501 = (state_22569[(7)]);
var inst_22510 = (inst_22501 == null);
var state_22569__$1 = state_22569;
if(cljs.core.truth_(inst_22510)){
var statearr_22628_24683 = state_22569__$1;
(statearr_22628_24683[(1)] = (11));

} else {
var statearr_22629_24684 = state_22569__$1;
(statearr_22629_24684[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__19581__auto__ = null;
var cljs$core$async$mix_$_state_machine__19581__auto____0 = (function (){
var statearr_22630 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22630[(0)] = cljs$core$async$mix_$_state_machine__19581__auto__);

(statearr_22630[(1)] = (1));

return statearr_22630;
});
var cljs$core$async$mix_$_state_machine__19581__auto____1 = (function (state_22569){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_22569);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e22637){var ex__19584__auto__ = e22637;
var statearr_22638_24686 = state_22569;
(statearr_22638_24686[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_22569[(4)]))){
var statearr_22639_24687 = state_22569;
(statearr_22639_24687[(1)] = cljs.core.first((state_22569[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24688 = state_22569;
state_22569 = G__24688;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__19581__auto__ = function(state_22569){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__19581__auto____1.call(this,state_22569);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__19581__auto____0;
cljs$core$async$mix_$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__19581__auto____1;
return cljs$core$async$mix_$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_22640 = f__20564__auto__();
(statearr_22640[(6)] = c__20563__auto___24637);

return statearr_22640;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_24689 = (function (p,v,ch,close_QMARK_){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5351__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__5349__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5349__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_24689(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_24692 = (function (p,v,ch){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5351__auto__.call(null,p,v,ch));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5349__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_24692(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_24693 = (function() {
var G__24694 = null;
var G__24694__1 = (function (p){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5351__auto__.call(null,p));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5349__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__24694__2 = (function (p,v){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5351__auto__.call(null,p,v));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5349__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__24694 = function(p,v){
switch(arguments.length){
case 1:
return G__24694__1.call(this,p);
case 2:
return G__24694__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__24694.cljs$core$IFn$_invoke$arity$1 = G__24694__1;
G__24694.cljs$core$IFn$_invoke$arity$2 = G__24694__2;
return G__24694;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__22689 = arguments.length;
switch (G__22689) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_24693(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_24693(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);



/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22707 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta22708){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta22708 = meta22708;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_22709,meta22708__$1){
var self__ = this;
var _22709__$1 = this;
return (new cljs.core.async.t_cljs$core$async22707(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta22708__$1));
}));

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_22709){
var self__ = this;
var _22709__$1 = this;
return self__.meta22708;
}));

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5804__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5804__auto__)){
var m = temp__5804__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async22707.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async22707.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta22708","meta22708",-1847846778,null)], null);
}));

(cljs.core.async.t_cljs$core$async22707.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async22707.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22707");

(cljs.core.async.t_cljs$core$async22707.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async22707");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async22707.
 */
cljs.core.async.__GT_t_cljs$core$async22707 = (function cljs$core$async$__GT_t_cljs$core$async22707(ch,topic_fn,buf_fn,mults,ensure_mult,meta22708){
return (new cljs.core.async.t_cljs$core$async22707(ch,topic_fn,buf_fn,mults,ensure_mult,meta22708));
});


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__22705 = arguments.length;
switch (G__22705) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__5002__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__22698_SHARP_){
if(cljs.core.truth_((p1__22698_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__22698_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__22698_SHARP_.call(null,topic)))){
return p1__22698_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__22698_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (new cljs.core.async.t_cljs$core$async22707(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
var c__20563__auto___24710 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_22803){
var state_val_22804 = (state_22803[(1)]);
if((state_val_22804 === (7))){
var inst_22799 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
var statearr_22805_24711 = state_22803__$1;
(statearr_22805_24711[(2)] = inst_22799);

(statearr_22805_24711[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (20))){
var state_22803__$1 = state_22803;
var statearr_22806_24714 = state_22803__$1;
(statearr_22806_24714[(2)] = null);

(statearr_22806_24714[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (1))){
var state_22803__$1 = state_22803;
var statearr_22807_24717 = state_22803__$1;
(statearr_22807_24717[(2)] = null);

(statearr_22807_24717[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (24))){
var inst_22781 = (state_22803[(7)]);
var inst_22790 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_22781);
var state_22803__$1 = state_22803;
var statearr_22810_24719 = state_22803__$1;
(statearr_22810_24719[(2)] = inst_22790);

(statearr_22810_24719[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (4))){
var inst_22723 = (state_22803[(8)]);
var inst_22723__$1 = (state_22803[(2)]);
var inst_22727 = (inst_22723__$1 == null);
var state_22803__$1 = (function (){var statearr_22814 = state_22803;
(statearr_22814[(8)] = inst_22723__$1);

return statearr_22814;
})();
if(cljs.core.truth_(inst_22727)){
var statearr_22815_24722 = state_22803__$1;
(statearr_22815_24722[(1)] = (5));

} else {
var statearr_22816_24723 = state_22803__$1;
(statearr_22816_24723[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (15))){
var inst_22775 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
var statearr_22819_24725 = state_22803__$1;
(statearr_22819_24725[(2)] = inst_22775);

(statearr_22819_24725[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (21))){
var inst_22795 = (state_22803[(2)]);
var state_22803__$1 = (function (){var statearr_22820 = state_22803;
(statearr_22820[(9)] = inst_22795);

return statearr_22820;
})();
var statearr_22821_24726 = state_22803__$1;
(statearr_22821_24726[(2)] = null);

(statearr_22821_24726[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (13))){
var inst_22757 = (state_22803[(10)]);
var inst_22759 = cljs.core.chunked_seq_QMARK_(inst_22757);
var state_22803__$1 = state_22803;
if(inst_22759){
var statearr_22825_24731 = state_22803__$1;
(statearr_22825_24731[(1)] = (16));

} else {
var statearr_22826_24732 = state_22803__$1;
(statearr_22826_24732[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (22))){
var inst_22787 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
if(cljs.core.truth_(inst_22787)){
var statearr_22827_24741 = state_22803__$1;
(statearr_22827_24741[(1)] = (23));

} else {
var statearr_22828_24742 = state_22803__$1;
(statearr_22828_24742[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (6))){
var inst_22783 = (state_22803[(11)]);
var inst_22781 = (state_22803[(7)]);
var inst_22723 = (state_22803[(8)]);
var inst_22781__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_22723) : topic_fn.call(null,inst_22723));
var inst_22782 = cljs.core.deref(mults);
var inst_22783__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_22782,inst_22781__$1);
var state_22803__$1 = (function (){var statearr_22832 = state_22803;
(statearr_22832[(11)] = inst_22783__$1);

(statearr_22832[(7)] = inst_22781__$1);

return statearr_22832;
})();
if(cljs.core.truth_(inst_22783__$1)){
var statearr_22833_24746 = state_22803__$1;
(statearr_22833_24746[(1)] = (19));

} else {
var statearr_22835_24747 = state_22803__$1;
(statearr_22835_24747[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (25))){
var inst_22792 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
var statearr_22836_24752 = state_22803__$1;
(statearr_22836_24752[(2)] = inst_22792);

(statearr_22836_24752[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (17))){
var inst_22757 = (state_22803[(10)]);
var inst_22766 = cljs.core.first(inst_22757);
var inst_22767 = cljs.core.async.muxch_STAR_(inst_22766);
var inst_22768 = cljs.core.async.close_BANG_(inst_22767);
var inst_22769 = cljs.core.next(inst_22757);
var inst_22736 = inst_22769;
var inst_22737 = null;
var inst_22738 = (0);
var inst_22739 = (0);
var state_22803__$1 = (function (){var statearr_22837 = state_22803;
(statearr_22837[(12)] = inst_22739);

(statearr_22837[(13)] = inst_22736);

(statearr_22837[(14)] = inst_22768);

(statearr_22837[(15)] = inst_22738);

(statearr_22837[(16)] = inst_22737);

return statearr_22837;
})();
var statearr_22839_24753 = state_22803__$1;
(statearr_22839_24753[(2)] = null);

(statearr_22839_24753[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (3))){
var inst_22801 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22803__$1,inst_22801);
} else {
if((state_val_22804 === (12))){
var inst_22777 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
var statearr_22840_24754 = state_22803__$1;
(statearr_22840_24754[(2)] = inst_22777);

(statearr_22840_24754[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (2))){
var state_22803__$1 = state_22803;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22803__$1,(4),ch);
} else {
if((state_val_22804 === (23))){
var state_22803__$1 = state_22803;
var statearr_22841_24755 = state_22803__$1;
(statearr_22841_24755[(2)] = null);

(statearr_22841_24755[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (19))){
var inst_22783 = (state_22803[(11)]);
var inst_22723 = (state_22803[(8)]);
var inst_22785 = cljs.core.async.muxch_STAR_(inst_22783);
var state_22803__$1 = state_22803;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22803__$1,(22),inst_22785,inst_22723);
} else {
if((state_val_22804 === (11))){
var inst_22736 = (state_22803[(13)]);
var inst_22757 = (state_22803[(10)]);
var inst_22757__$1 = cljs.core.seq(inst_22736);
var state_22803__$1 = (function (){var statearr_22845 = state_22803;
(statearr_22845[(10)] = inst_22757__$1);

return statearr_22845;
})();
if(inst_22757__$1){
var statearr_22848_24756 = state_22803__$1;
(statearr_22848_24756[(1)] = (13));

} else {
var statearr_22849_24757 = state_22803__$1;
(statearr_22849_24757[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (9))){
var inst_22779 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
var statearr_22850_24758 = state_22803__$1;
(statearr_22850_24758[(2)] = inst_22779);

(statearr_22850_24758[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (5))){
var inst_22733 = cljs.core.deref(mults);
var inst_22734 = cljs.core.vals(inst_22733);
var inst_22735 = cljs.core.seq(inst_22734);
var inst_22736 = inst_22735;
var inst_22737 = null;
var inst_22738 = (0);
var inst_22739 = (0);
var state_22803__$1 = (function (){var statearr_22856 = state_22803;
(statearr_22856[(12)] = inst_22739);

(statearr_22856[(13)] = inst_22736);

(statearr_22856[(15)] = inst_22738);

(statearr_22856[(16)] = inst_22737);

return statearr_22856;
})();
var statearr_22857_24759 = state_22803__$1;
(statearr_22857_24759[(2)] = null);

(statearr_22857_24759[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (14))){
var state_22803__$1 = state_22803;
var statearr_22861_24760 = state_22803__$1;
(statearr_22861_24760[(2)] = null);

(statearr_22861_24760[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (16))){
var inst_22757 = (state_22803[(10)]);
var inst_22761 = cljs.core.chunk_first(inst_22757);
var inst_22762 = cljs.core.chunk_rest(inst_22757);
var inst_22763 = cljs.core.count(inst_22761);
var inst_22736 = inst_22762;
var inst_22737 = inst_22761;
var inst_22738 = inst_22763;
var inst_22739 = (0);
var state_22803__$1 = (function (){var statearr_22862 = state_22803;
(statearr_22862[(12)] = inst_22739);

(statearr_22862[(13)] = inst_22736);

(statearr_22862[(15)] = inst_22738);

(statearr_22862[(16)] = inst_22737);

return statearr_22862;
})();
var statearr_22863_24761 = state_22803__$1;
(statearr_22863_24761[(2)] = null);

(statearr_22863_24761[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (10))){
var inst_22739 = (state_22803[(12)]);
var inst_22736 = (state_22803[(13)]);
var inst_22738 = (state_22803[(15)]);
var inst_22737 = (state_22803[(16)]);
var inst_22744 = cljs.core._nth(inst_22737,inst_22739);
var inst_22746 = cljs.core.async.muxch_STAR_(inst_22744);
var inst_22747 = cljs.core.async.close_BANG_(inst_22746);
var inst_22754 = (inst_22739 + (1));
var tmp22858 = inst_22736;
var tmp22859 = inst_22738;
var tmp22860 = inst_22737;
var inst_22736__$1 = tmp22858;
var inst_22737__$1 = tmp22860;
var inst_22738__$1 = tmp22859;
var inst_22739__$1 = inst_22754;
var state_22803__$1 = (function (){var statearr_22868 = state_22803;
(statearr_22868[(17)] = inst_22747);

(statearr_22868[(12)] = inst_22739__$1);

(statearr_22868[(13)] = inst_22736__$1);

(statearr_22868[(15)] = inst_22738__$1);

(statearr_22868[(16)] = inst_22737__$1);

return statearr_22868;
})();
var statearr_22869_24770 = state_22803__$1;
(statearr_22869_24770[(2)] = null);

(statearr_22869_24770[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (18))){
var inst_22772 = (state_22803[(2)]);
var state_22803__$1 = state_22803;
var statearr_22882_24771 = state_22803__$1;
(statearr_22882_24771[(2)] = inst_22772);

(statearr_22882_24771[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22804 === (8))){
var inst_22739 = (state_22803[(12)]);
var inst_22738 = (state_22803[(15)]);
var inst_22741 = (inst_22739 < inst_22738);
var inst_22742 = inst_22741;
var state_22803__$1 = state_22803;
if(cljs.core.truth_(inst_22742)){
var statearr_22883_24773 = state_22803__$1;
(statearr_22883_24773[(1)] = (10));

} else {
var statearr_22884_24775 = state_22803__$1;
(statearr_22884_24775[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_22885 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22885[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_22885[(1)] = (1));

return statearr_22885;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_22803){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_22803);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e22887){var ex__19584__auto__ = e22887;
var statearr_22888_24780 = state_22803;
(statearr_22888_24780[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_22803[(4)]))){
var statearr_22889_24782 = state_22803;
(statearr_22889_24782[(1)] = cljs.core.first((state_22803[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24783 = state_22803;
state_22803 = G__24783;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_22803){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_22803);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_22890 = f__20564__auto__();
(statearr_22890[(6)] = c__20563__auto___24710);

return statearr_22890;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__22892 = arguments.length;
switch (G__22892) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__22907 = arguments.length;
switch (G__22907) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__22915 = arguments.length;
switch (G__22915) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
if((cnt === (0))){
cljs.core.async.close_BANG_(out);
} else {
var c__20563__auto___24800 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_22972){
var state_val_22973 = (state_22972[(1)]);
if((state_val_22973 === (7))){
var state_22972__$1 = state_22972;
var statearr_22977_24801 = state_22972__$1;
(statearr_22977_24801[(2)] = null);

(statearr_22977_24801[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (1))){
var state_22972__$1 = state_22972;
var statearr_22978_24808 = state_22972__$1;
(statearr_22978_24808[(2)] = null);

(statearr_22978_24808[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (4))){
var inst_22931 = (state_22972[(7)]);
var inst_22930 = (state_22972[(8)]);
var inst_22933 = (inst_22931 < inst_22930);
var state_22972__$1 = state_22972;
if(cljs.core.truth_(inst_22933)){
var statearr_22982_24812 = state_22972__$1;
(statearr_22982_24812[(1)] = (6));

} else {
var statearr_22983_24813 = state_22972__$1;
(statearr_22983_24813[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (15))){
var inst_22957 = (state_22972[(9)]);
var inst_22962 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_22957);
var state_22972__$1 = state_22972;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_22972__$1,(17),out,inst_22962);
} else {
if((state_val_22973 === (13))){
var inst_22957 = (state_22972[(9)]);
var inst_22957__$1 = (state_22972[(2)]);
var inst_22958 = cljs.core.some(cljs.core.nil_QMARK_,inst_22957__$1);
var state_22972__$1 = (function (){var statearr_22989 = state_22972;
(statearr_22989[(9)] = inst_22957__$1);

return statearr_22989;
})();
if(cljs.core.truth_(inst_22958)){
var statearr_22995_24826 = state_22972__$1;
(statearr_22995_24826[(1)] = (14));

} else {
var statearr_22996_24827 = state_22972__$1;
(statearr_22996_24827[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (6))){
var state_22972__$1 = state_22972;
var statearr_22997_24828 = state_22972__$1;
(statearr_22997_24828[(2)] = null);

(statearr_22997_24828[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (17))){
var inst_22964 = (state_22972[(2)]);
var state_22972__$1 = (function (){var statearr_23001 = state_22972;
(statearr_23001[(10)] = inst_22964);

return statearr_23001;
})();
var statearr_23002_24833 = state_22972__$1;
(statearr_23002_24833[(2)] = null);

(statearr_23002_24833[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (3))){
var inst_22969 = (state_22972[(2)]);
var state_22972__$1 = state_22972;
return cljs.core.async.impl.ioc_helpers.return_chan(state_22972__$1,inst_22969);
} else {
if((state_val_22973 === (12))){
var _ = (function (){var statearr_23008 = state_22972;
(statearr_23008[(4)] = cljs.core.rest((state_22972[(4)])));

return statearr_23008;
})();
var state_22972__$1 = state_22972;
var ex22999 = (state_22972__$1[(2)]);
var statearr_23009_24834 = state_22972__$1;
(statearr_23009_24834[(5)] = ex22999);


if((ex22999 instanceof Object)){
var statearr_23010_24835 = state_22972__$1;
(statearr_23010_24835[(1)] = (11));

(statearr_23010_24835[(5)] = null);

} else {
throw ex22999;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (2))){
var inst_22929 = cljs.core.reset_BANG_(dctr,cnt);
var inst_22930 = cnt;
var inst_22931 = (0);
var state_22972__$1 = (function (){var statearr_23014 = state_22972;
(statearr_23014[(11)] = inst_22929);

(statearr_23014[(7)] = inst_22931);

(statearr_23014[(8)] = inst_22930);

return statearr_23014;
})();
var statearr_23015_24837 = state_22972__$1;
(statearr_23015_24837[(2)] = null);

(statearr_23015_24837[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (11))){
var inst_22935 = (state_22972[(2)]);
var inst_22936 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_22972__$1 = (function (){var statearr_23017 = state_22972;
(statearr_23017[(12)] = inst_22935);

return statearr_23017;
})();
var statearr_23021_24838 = state_22972__$1;
(statearr_23021_24838[(2)] = inst_22936);

(statearr_23021_24838[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (9))){
var inst_22931 = (state_22972[(7)]);
var _ = (function (){var statearr_23025 = state_22972;
(statearr_23025[(4)] = cljs.core.cons((12),(state_22972[(4)])));

return statearr_23025;
})();
var inst_22943 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_22931) : chs__$1.call(null,inst_22931));
var inst_22944 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_22931) : done.call(null,inst_22931));
var inst_22945 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_22943,inst_22944);
var ___$1 = (function (){var statearr_23026 = state_22972;
(statearr_23026[(4)] = cljs.core.rest((state_22972[(4)])));

return statearr_23026;
})();
var state_22972__$1 = state_22972;
var statearr_23027_24844 = state_22972__$1;
(statearr_23027_24844[(2)] = inst_22945);

(statearr_23027_24844[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (5))){
var inst_22955 = (state_22972[(2)]);
var state_22972__$1 = (function (){var statearr_23028 = state_22972;
(statearr_23028[(13)] = inst_22955);

return statearr_23028;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_22972__$1,(13),dchan);
} else {
if((state_val_22973 === (14))){
var inst_22960 = cljs.core.async.close_BANG_(out);
var state_22972__$1 = state_22972;
var statearr_23029_24845 = state_22972__$1;
(statearr_23029_24845[(2)] = inst_22960);

(statearr_23029_24845[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (16))){
var inst_22967 = (state_22972[(2)]);
var state_22972__$1 = state_22972;
var statearr_23034_24846 = state_22972__$1;
(statearr_23034_24846[(2)] = inst_22967);

(statearr_23034_24846[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (10))){
var inst_22931 = (state_22972[(7)]);
var inst_22948 = (state_22972[(2)]);
var inst_22949 = (inst_22931 + (1));
var inst_22931__$1 = inst_22949;
var state_22972__$1 = (function (){var statearr_23038 = state_22972;
(statearr_23038[(14)] = inst_22948);

(statearr_23038[(7)] = inst_22931__$1);

return statearr_23038;
})();
var statearr_23039_24847 = state_22972__$1;
(statearr_23039_24847[(2)] = null);

(statearr_23039_24847[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22973 === (8))){
var inst_22953 = (state_22972[(2)]);
var state_22972__$1 = state_22972;
var statearr_23040_24848 = state_22972__$1;
(statearr_23040_24848[(2)] = inst_22953);

(statearr_23040_24848[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_23044 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23044[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_23044[(1)] = (1));

return statearr_23044;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_22972){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_22972);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23048){var ex__19584__auto__ = e23048;
var statearr_23049_24851 = state_22972;
(statearr_23049_24851[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_22972[(4)]))){
var statearr_23050_24852 = state_22972;
(statearr_23050_24852[(1)] = cljs.core.first((state_22972[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24853 = state_22972;
state_22972 = G__24853;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_22972){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_22972);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_23051 = f__20564__auto__();
(statearr_23051[(6)] = c__20563__auto___24800);

return statearr_23051;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

}

return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__23057 = arguments.length;
switch (G__23057) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__20563__auto___24857 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_23092){
var state_val_23093 = (state_23092[(1)]);
if((state_val_23093 === (7))){
var inst_23072 = (state_23092[(7)]);
var inst_23071 = (state_23092[(8)]);
var inst_23071__$1 = (state_23092[(2)]);
var inst_23072__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_23071__$1,(0),null);
var inst_23073 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_23071__$1,(1),null);
var inst_23074 = (inst_23072__$1 == null);
var state_23092__$1 = (function (){var statearr_23097 = state_23092;
(statearr_23097[(7)] = inst_23072__$1);

(statearr_23097[(8)] = inst_23071__$1);

(statearr_23097[(9)] = inst_23073);

return statearr_23097;
})();
if(cljs.core.truth_(inst_23074)){
var statearr_23098_24858 = state_23092__$1;
(statearr_23098_24858[(1)] = (8));

} else {
var statearr_23099_24859 = state_23092__$1;
(statearr_23099_24859[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (1))){
var inst_23061 = cljs.core.vec(chs);
var inst_23062 = inst_23061;
var state_23092__$1 = (function (){var statearr_23100 = state_23092;
(statearr_23100[(10)] = inst_23062);

return statearr_23100;
})();
var statearr_23101_24861 = state_23092__$1;
(statearr_23101_24861[(2)] = null);

(statearr_23101_24861[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (4))){
var inst_23062 = (state_23092[(10)]);
var state_23092__$1 = state_23092;
return cljs.core.async.ioc_alts_BANG_(state_23092__$1,(7),inst_23062);
} else {
if((state_val_23093 === (6))){
var inst_23088 = (state_23092[(2)]);
var state_23092__$1 = state_23092;
var statearr_23102_24865 = state_23092__$1;
(statearr_23102_24865[(2)] = inst_23088);

(statearr_23102_24865[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (3))){
var inst_23090 = (state_23092[(2)]);
var state_23092__$1 = state_23092;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23092__$1,inst_23090);
} else {
if((state_val_23093 === (2))){
var inst_23062 = (state_23092[(10)]);
var inst_23064 = cljs.core.count(inst_23062);
var inst_23065 = (inst_23064 > (0));
var state_23092__$1 = state_23092;
if(cljs.core.truth_(inst_23065)){
var statearr_23104_24866 = state_23092__$1;
(statearr_23104_24866[(1)] = (4));

} else {
var statearr_23105_24867 = state_23092__$1;
(statearr_23105_24867[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (11))){
var inst_23062 = (state_23092[(10)]);
var inst_23081 = (state_23092[(2)]);
var tmp23103 = inst_23062;
var inst_23062__$1 = tmp23103;
var state_23092__$1 = (function (){var statearr_23106 = state_23092;
(statearr_23106[(11)] = inst_23081);

(statearr_23106[(10)] = inst_23062__$1);

return statearr_23106;
})();
var statearr_23107_24868 = state_23092__$1;
(statearr_23107_24868[(2)] = null);

(statearr_23107_24868[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (9))){
var inst_23072 = (state_23092[(7)]);
var state_23092__$1 = state_23092;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23092__$1,(11),out,inst_23072);
} else {
if((state_val_23093 === (5))){
var inst_23086 = cljs.core.async.close_BANG_(out);
var state_23092__$1 = state_23092;
var statearr_23112_24869 = state_23092__$1;
(statearr_23112_24869[(2)] = inst_23086);

(statearr_23112_24869[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (10))){
var inst_23084 = (state_23092[(2)]);
var state_23092__$1 = state_23092;
var statearr_23120_24871 = state_23092__$1;
(statearr_23120_24871[(2)] = inst_23084);

(statearr_23120_24871[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23093 === (8))){
var inst_23072 = (state_23092[(7)]);
var inst_23062 = (state_23092[(10)]);
var inst_23071 = (state_23092[(8)]);
var inst_23073 = (state_23092[(9)]);
var inst_23076 = (function (){var cs = inst_23062;
var vec__23067 = inst_23071;
var v = inst_23072;
var c = inst_23073;
return (function (p1__23052_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__23052_SHARP_);
});
})();
var inst_23077 = cljs.core.filterv(inst_23076,inst_23062);
var inst_23062__$1 = inst_23077;
var state_23092__$1 = (function (){var statearr_23121 = state_23092;
(statearr_23121[(10)] = inst_23062__$1);

return statearr_23121;
})();
var statearr_23122_24877 = state_23092__$1;
(statearr_23122_24877[(2)] = null);

(statearr_23122_24877[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_23126 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23126[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_23126[(1)] = (1));

return statearr_23126;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_23092){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_23092);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23127){var ex__19584__auto__ = e23127;
var statearr_23128_24878 = state_23092;
(statearr_23128_24878[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_23092[(4)]))){
var statearr_23132_24879 = state_23092;
(statearr_23132_24879[(1)] = cljs.core.first((state_23092[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24880 = state_23092;
state_23092 = G__24880;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_23092){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_23092);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_23151 = f__20564__auto__();
(statearr_23151[(6)] = c__20563__auto___24857);

return statearr_23151;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__23174 = arguments.length;
switch (G__23174) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__20563__auto___24889 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_23199){
var state_val_23200 = (state_23199[(1)]);
if((state_val_23200 === (7))){
var inst_23181 = (state_23199[(7)]);
var inst_23181__$1 = (state_23199[(2)]);
var inst_23182 = (inst_23181__$1 == null);
var inst_23183 = cljs.core.not(inst_23182);
var state_23199__$1 = (function (){var statearr_23201 = state_23199;
(statearr_23201[(7)] = inst_23181__$1);

return statearr_23201;
})();
if(inst_23183){
var statearr_23206_24898 = state_23199__$1;
(statearr_23206_24898[(1)] = (8));

} else {
var statearr_23207_24899 = state_23199__$1;
(statearr_23207_24899[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (1))){
var inst_23176 = (0);
var state_23199__$1 = (function (){var statearr_23208 = state_23199;
(statearr_23208[(8)] = inst_23176);

return statearr_23208;
})();
var statearr_23213_24900 = state_23199__$1;
(statearr_23213_24900[(2)] = null);

(statearr_23213_24900[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (4))){
var state_23199__$1 = state_23199;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23199__$1,(7),ch);
} else {
if((state_val_23200 === (6))){
var inst_23194 = (state_23199[(2)]);
var state_23199__$1 = state_23199;
var statearr_23214_24905 = state_23199__$1;
(statearr_23214_24905[(2)] = inst_23194);

(statearr_23214_24905[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (3))){
var inst_23196 = (state_23199[(2)]);
var inst_23197 = cljs.core.async.close_BANG_(out);
var state_23199__$1 = (function (){var statearr_23215 = state_23199;
(statearr_23215[(9)] = inst_23196);

return statearr_23215;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_23199__$1,inst_23197);
} else {
if((state_val_23200 === (2))){
var inst_23176 = (state_23199[(8)]);
var inst_23178 = (inst_23176 < n);
var state_23199__$1 = state_23199;
if(cljs.core.truth_(inst_23178)){
var statearr_23216_24907 = state_23199__$1;
(statearr_23216_24907[(1)] = (4));

} else {
var statearr_23217_24908 = state_23199__$1;
(statearr_23217_24908[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (11))){
var inst_23176 = (state_23199[(8)]);
var inst_23186 = (state_23199[(2)]);
var inst_23187 = (inst_23176 + (1));
var inst_23176__$1 = inst_23187;
var state_23199__$1 = (function (){var statearr_23218 = state_23199;
(statearr_23218[(8)] = inst_23176__$1);

(statearr_23218[(10)] = inst_23186);

return statearr_23218;
})();
var statearr_23219_24909 = state_23199__$1;
(statearr_23219_24909[(2)] = null);

(statearr_23219_24909[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (9))){
var state_23199__$1 = state_23199;
var statearr_23220_24911 = state_23199__$1;
(statearr_23220_24911[(2)] = null);

(statearr_23220_24911[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (5))){
var state_23199__$1 = state_23199;
var statearr_23221_24913 = state_23199__$1;
(statearr_23221_24913[(2)] = null);

(statearr_23221_24913[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (10))){
var inst_23191 = (state_23199[(2)]);
var state_23199__$1 = state_23199;
var statearr_23225_24914 = state_23199__$1;
(statearr_23225_24914[(2)] = inst_23191);

(statearr_23225_24914[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23200 === (8))){
var inst_23181 = (state_23199[(7)]);
var state_23199__$1 = state_23199;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23199__$1,(11),out,inst_23181);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_23228 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_23228[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_23228[(1)] = (1));

return statearr_23228;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_23199){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_23199);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23229){var ex__19584__auto__ = e23229;
var statearr_23230_24917 = state_23199;
(statearr_23230_24917[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_23199[(4)]))){
var statearr_23231_24920 = state_23199;
(statearr_23231_24920[(1)] = cljs.core.first((state_23199[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24923 = state_23199;
state_23199 = G__24923;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_23199){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_23199);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_23232 = f__20564__auto__();
(statearr_23232[(6)] = c__20563__auto___24889);

return statearr_23232;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);


/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23244 = (function (f,ch,meta23235,_,fn1,meta23245){
this.f = f;
this.ch = ch;
this.meta23235 = meta23235;
this._ = _;
this.fn1 = fn1;
this.meta23245 = meta23245;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23244.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23246,meta23245__$1){
var self__ = this;
var _23246__$1 = this;
return (new cljs.core.async.t_cljs$core$async23244(self__.f,self__.ch,self__.meta23235,self__._,self__.fn1,meta23245__$1));
}));

(cljs.core.async.t_cljs$core$async23244.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23246){
var self__ = this;
var _23246__$1 = this;
return self__.meta23245;
}));

(cljs.core.async.t_cljs$core$async23244.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23244.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async23244.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async23244.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__23233_SHARP_){
var G__23248 = (((p1__23233_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__23233_SHARP_) : self__.f.call(null,p1__23233_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__23248) : f1.call(null,G__23248));
});
}));

(cljs.core.async.t_cljs$core$async23244.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23235","meta23235",1568502035,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async23234","cljs.core.async/t_cljs$core$async23234",-1226976426,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta23245","meta23245",-1224671812,null)], null);
}));

(cljs.core.async.t_cljs$core$async23244.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23244.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23244");

(cljs.core.async.t_cljs$core$async23244.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23244");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23244.
 */
cljs.core.async.__GT_t_cljs$core$async23244 = (function cljs$core$async$__GT_t_cljs$core$async23244(f,ch,meta23235,_,fn1,meta23245){
return (new cljs.core.async.t_cljs$core$async23244(f,ch,meta23235,_,fn1,meta23245));
});



/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23234 = (function (f,ch,meta23235){
this.f = f;
this.ch = ch;
this.meta23235 = meta23235;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23236,meta23235__$1){
var self__ = this;
var _23236__$1 = this;
return (new cljs.core.async.t_cljs$core$async23234(self__.f,self__.ch,meta23235__$1));
}));

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23236){
var self__ = this;
var _23236__$1 = this;
return self__.meta23235;
}));

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(new cljs.core.async.t_cljs$core$async23244(self__.f,self__.ch,self__.meta23235,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY)));
if(cljs.core.truth_((function (){var and__5000__auto__ = ret;
if(cljs.core.truth_(and__5000__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__5000__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__23261 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__23261) : self__.f.call(null,G__23261));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23234.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async23234.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23235","meta23235",1568502035,null)], null);
}));

(cljs.core.async.t_cljs$core$async23234.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23234.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23234");

(cljs.core.async.t_cljs$core$async23234.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23234");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23234.
 */
cljs.core.async.__GT_t_cljs$core$async23234 = (function cljs$core$async$__GT_t_cljs$core$async23234(f,ch,meta23235){
return (new cljs.core.async.t_cljs$core$async23234(f,ch,meta23235));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
return (new cljs.core.async.t_cljs$core$async23234(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23262 = (function (f,ch,meta23263){
this.f = f;
this.ch = ch;
this.meta23263 = meta23263;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23264,meta23263__$1){
var self__ = this;
var _23264__$1 = this;
return (new cljs.core.async.t_cljs$core$async23262(self__.f,self__.ch,meta23263__$1));
}));

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23264){
var self__ = this;
var _23264__$1 = this;
return self__.meta23263;
}));

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23262.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async23262.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23263","meta23263",1351815134,null)], null);
}));

(cljs.core.async.t_cljs$core$async23262.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23262.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23262");

(cljs.core.async.t_cljs$core$async23262.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23262");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23262.
 */
cljs.core.async.__GT_t_cljs$core$async23262 = (function cljs$core$async$__GT_t_cljs$core$async23262(f,ch,meta23263){
return (new cljs.core.async.t_cljs$core$async23262(f,ch,meta23263));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
return (new cljs.core.async.t_cljs$core$async23262(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23268 = (function (p,ch,meta23269){
this.p = p;
this.ch = ch;
this.meta23269 = meta23269;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23270,meta23269__$1){
var self__ = this;
var _23270__$1 = this;
return (new cljs.core.async.t_cljs$core$async23268(self__.p,self__.ch,meta23269__$1));
}));

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23270){
var self__ = this;
var _23270__$1 = this;
return self__.meta23269;
}));

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async23268.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async23268.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23269","meta23269",84113633,null)], null);
}));

(cljs.core.async.t_cljs$core$async23268.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async23268.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23268");

(cljs.core.async.t_cljs$core$async23268.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async23268");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async23268.
 */
cljs.core.async.__GT_t_cljs$core$async23268 = (function cljs$core$async$__GT_t_cljs$core$async23268(p,ch,meta23269){
return (new cljs.core.async.t_cljs$core$async23268(p,ch,meta23269));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
return (new cljs.core.async.t_cljs$core$async23268(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__23286 = arguments.length;
switch (G__23286) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__20563__auto___24945 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_23311){
var state_val_23312 = (state_23311[(1)]);
if((state_val_23312 === (7))){
var inst_23307 = (state_23311[(2)]);
var state_23311__$1 = state_23311;
var statearr_23317_24946 = state_23311__$1;
(statearr_23317_24946[(2)] = inst_23307);

(statearr_23317_24946[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (1))){
var state_23311__$1 = state_23311;
var statearr_23318_24947 = state_23311__$1;
(statearr_23318_24947[(2)] = null);

(statearr_23318_24947[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (4))){
var inst_23293 = (state_23311[(7)]);
var inst_23293__$1 = (state_23311[(2)]);
var inst_23294 = (inst_23293__$1 == null);
var state_23311__$1 = (function (){var statearr_23319 = state_23311;
(statearr_23319[(7)] = inst_23293__$1);

return statearr_23319;
})();
if(cljs.core.truth_(inst_23294)){
var statearr_23320_24948 = state_23311__$1;
(statearr_23320_24948[(1)] = (5));

} else {
var statearr_23321_24949 = state_23311__$1;
(statearr_23321_24949[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (6))){
var inst_23293 = (state_23311[(7)]);
var inst_23298 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_23293) : p.call(null,inst_23293));
var state_23311__$1 = state_23311;
if(cljs.core.truth_(inst_23298)){
var statearr_23322_24950 = state_23311__$1;
(statearr_23322_24950[(1)] = (8));

} else {
var statearr_23327_24951 = state_23311__$1;
(statearr_23327_24951[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (3))){
var inst_23309 = (state_23311[(2)]);
var state_23311__$1 = state_23311;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23311__$1,inst_23309);
} else {
if((state_val_23312 === (2))){
var state_23311__$1 = state_23311;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23311__$1,(4),ch);
} else {
if((state_val_23312 === (11))){
var inst_23301 = (state_23311[(2)]);
var state_23311__$1 = state_23311;
var statearr_23331_24952 = state_23311__$1;
(statearr_23331_24952[(2)] = inst_23301);

(statearr_23331_24952[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (9))){
var state_23311__$1 = state_23311;
var statearr_23333_24954 = state_23311__$1;
(statearr_23333_24954[(2)] = null);

(statearr_23333_24954[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (5))){
var inst_23296 = cljs.core.async.close_BANG_(out);
var state_23311__$1 = state_23311;
var statearr_23335_24955 = state_23311__$1;
(statearr_23335_24955[(2)] = inst_23296);

(statearr_23335_24955[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (10))){
var inst_23304 = (state_23311[(2)]);
var state_23311__$1 = (function (){var statearr_23338 = state_23311;
(statearr_23338[(8)] = inst_23304);

return statearr_23338;
})();
var statearr_23343_24957 = state_23311__$1;
(statearr_23343_24957[(2)] = null);

(statearr_23343_24957[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23312 === (8))){
var inst_23293 = (state_23311[(7)]);
var state_23311__$1 = state_23311;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23311__$1,(11),out,inst_23293);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_23351 = [null,null,null,null,null,null,null,null,null];
(statearr_23351[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_23351[(1)] = (1));

return statearr_23351;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_23311){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_23311);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23352){var ex__19584__auto__ = e23352;
var statearr_23353_24961 = state_23311;
(statearr_23353_24961[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_23311[(4)]))){
var statearr_23354_24962 = state_23311;
(statearr_23354_24962[(1)] = cljs.core.first((state_23311[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24963 = state_23311;
state_23311 = G__24963;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_23311){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_23311);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_23361 = f__20564__auto__();
(statearr_23361[(6)] = c__20563__auto___24945);

return statearr_23361;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__23378 = arguments.length;
switch (G__23378) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__20563__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_23560){
var state_val_23561 = (state_23560[(1)]);
if((state_val_23561 === (7))){
var inst_23542 = (state_23560[(2)]);
var state_23560__$1 = state_23560;
var statearr_23578_24966 = state_23560__$1;
(statearr_23578_24966[(2)] = inst_23542);

(statearr_23578_24966[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (20))){
var inst_23481 = (state_23560[(7)]);
var inst_23516 = (state_23560[(2)]);
var inst_23521 = cljs.core.next(inst_23481);
var inst_23446 = inst_23521;
var inst_23447 = null;
var inst_23448 = (0);
var inst_23449 = (0);
var state_23560__$1 = (function (){var statearr_23582 = state_23560;
(statearr_23582[(8)] = inst_23448);

(statearr_23582[(9)] = inst_23516);

(statearr_23582[(10)] = inst_23449);

(statearr_23582[(11)] = inst_23446);

(statearr_23582[(12)] = inst_23447);

return statearr_23582;
})();
var statearr_23587_24967 = state_23560__$1;
(statearr_23587_24967[(2)] = null);

(statearr_23587_24967[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (1))){
var state_23560__$1 = state_23560;
var statearr_23588_24969 = state_23560__$1;
(statearr_23588_24969[(2)] = null);

(statearr_23588_24969[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (4))){
var inst_23422 = (state_23560[(13)]);
var inst_23422__$1 = (state_23560[(2)]);
var inst_23427 = (inst_23422__$1 == null);
var state_23560__$1 = (function (){var statearr_23592 = state_23560;
(statearr_23592[(13)] = inst_23422__$1);

return statearr_23592;
})();
if(cljs.core.truth_(inst_23427)){
var statearr_23597_24971 = state_23560__$1;
(statearr_23597_24971[(1)] = (5));

} else {
var statearr_23599_24972 = state_23560__$1;
(statearr_23599_24972[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (15))){
var state_23560__$1 = state_23560;
var statearr_23612_24973 = state_23560__$1;
(statearr_23612_24973[(2)] = null);

(statearr_23612_24973[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (21))){
var state_23560__$1 = state_23560;
var statearr_23615_24974 = state_23560__$1;
(statearr_23615_24974[(2)] = null);

(statearr_23615_24974[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (13))){
var inst_23448 = (state_23560[(8)]);
var inst_23449 = (state_23560[(10)]);
var inst_23446 = (state_23560[(11)]);
var inst_23447 = (state_23560[(12)]);
var inst_23463 = (state_23560[(2)]);
var inst_23469 = (inst_23449 + (1));
var tmp23608 = inst_23448;
var tmp23609 = inst_23446;
var tmp23610 = inst_23447;
var inst_23446__$1 = tmp23609;
var inst_23447__$1 = tmp23610;
var inst_23448__$1 = tmp23608;
var inst_23449__$1 = inst_23469;
var state_23560__$1 = (function (){var statearr_23621 = state_23560;
(statearr_23621[(8)] = inst_23448__$1);

(statearr_23621[(14)] = inst_23463);

(statearr_23621[(10)] = inst_23449__$1);

(statearr_23621[(11)] = inst_23446__$1);

(statearr_23621[(12)] = inst_23447__$1);

return statearr_23621;
})();
var statearr_23622_24975 = state_23560__$1;
(statearr_23622_24975[(2)] = null);

(statearr_23622_24975[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (22))){
var state_23560__$1 = state_23560;
var statearr_23626_24976 = state_23560__$1;
(statearr_23626_24976[(2)] = null);

(statearr_23626_24976[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (6))){
var inst_23422 = (state_23560[(13)]);
var inst_23444 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_23422) : f.call(null,inst_23422));
var inst_23445 = cljs.core.seq(inst_23444);
var inst_23446 = inst_23445;
var inst_23447 = null;
var inst_23448 = (0);
var inst_23449 = (0);
var state_23560__$1 = (function (){var statearr_23631 = state_23560;
(statearr_23631[(8)] = inst_23448);

(statearr_23631[(10)] = inst_23449);

(statearr_23631[(11)] = inst_23446);

(statearr_23631[(12)] = inst_23447);

return statearr_23631;
})();
var statearr_23636_24979 = state_23560__$1;
(statearr_23636_24979[(2)] = null);

(statearr_23636_24979[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (17))){
var inst_23481 = (state_23560[(7)]);
var inst_23509 = cljs.core.chunk_first(inst_23481);
var inst_23510 = cljs.core.chunk_rest(inst_23481);
var inst_23511 = cljs.core.count(inst_23509);
var inst_23446 = inst_23510;
var inst_23447 = inst_23509;
var inst_23448 = inst_23511;
var inst_23449 = (0);
var state_23560__$1 = (function (){var statearr_23647 = state_23560;
(statearr_23647[(8)] = inst_23448);

(statearr_23647[(10)] = inst_23449);

(statearr_23647[(11)] = inst_23446);

(statearr_23647[(12)] = inst_23447);

return statearr_23647;
})();
var statearr_23648_24981 = state_23560__$1;
(statearr_23648_24981[(2)] = null);

(statearr_23648_24981[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (3))){
var inst_23544 = (state_23560[(2)]);
var state_23560__$1 = state_23560;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23560__$1,inst_23544);
} else {
if((state_val_23561 === (12))){
var inst_23532 = (state_23560[(2)]);
var state_23560__$1 = state_23560;
var statearr_23651_24983 = state_23560__$1;
(statearr_23651_24983[(2)] = inst_23532);

(statearr_23651_24983[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (2))){
var state_23560__$1 = state_23560;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23560__$1,(4),in$);
} else {
if((state_val_23561 === (23))){
var inst_23540 = (state_23560[(2)]);
var state_23560__$1 = state_23560;
var statearr_23657_24984 = state_23560__$1;
(statearr_23657_24984[(2)] = inst_23540);

(statearr_23657_24984[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (19))){
var inst_23527 = (state_23560[(2)]);
var state_23560__$1 = state_23560;
var statearr_23662_24985 = state_23560__$1;
(statearr_23662_24985[(2)] = inst_23527);

(statearr_23662_24985[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (11))){
var inst_23481 = (state_23560[(7)]);
var inst_23446 = (state_23560[(11)]);
var inst_23481__$1 = cljs.core.seq(inst_23446);
var state_23560__$1 = (function (){var statearr_23665 = state_23560;
(statearr_23665[(7)] = inst_23481__$1);

return statearr_23665;
})();
if(inst_23481__$1){
var statearr_23666_24986 = state_23560__$1;
(statearr_23666_24986[(1)] = (14));

} else {
var statearr_23672_24987 = state_23560__$1;
(statearr_23672_24987[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (9))){
var inst_23534 = (state_23560[(2)]);
var inst_23535 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_23560__$1 = (function (){var statearr_23683 = state_23560;
(statearr_23683[(15)] = inst_23534);

return statearr_23683;
})();
if(cljs.core.truth_(inst_23535)){
var statearr_23684_24988 = state_23560__$1;
(statearr_23684_24988[(1)] = (21));

} else {
var statearr_23691_24989 = state_23560__$1;
(statearr_23691_24989[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (5))){
var inst_23438 = cljs.core.async.close_BANG_(out);
var state_23560__$1 = state_23560;
var statearr_23696_24990 = state_23560__$1;
(statearr_23696_24990[(2)] = inst_23438);

(statearr_23696_24990[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (14))){
var inst_23481 = (state_23560[(7)]);
var inst_23494 = cljs.core.chunked_seq_QMARK_(inst_23481);
var state_23560__$1 = state_23560;
if(inst_23494){
var statearr_23698_24992 = state_23560__$1;
(statearr_23698_24992[(1)] = (17));

} else {
var statearr_23705_24993 = state_23560__$1;
(statearr_23705_24993[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (16))){
var inst_23530 = (state_23560[(2)]);
var state_23560__$1 = state_23560;
var statearr_23707_24995 = state_23560__$1;
(statearr_23707_24995[(2)] = inst_23530);

(statearr_23707_24995[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23561 === (10))){
var inst_23449 = (state_23560[(10)]);
var inst_23447 = (state_23560[(12)]);
var inst_23461 = cljs.core._nth(inst_23447,inst_23449);
var state_23560__$1 = state_23560;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23560__$1,(13),out,inst_23461);
} else {
if((state_val_23561 === (18))){
var inst_23481 = (state_23560[(7)]);
var inst_23514 = cljs.core.first(inst_23481);
var state_23560__$1 = state_23560;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23560__$1,(20),out,inst_23514);
} else {
if((state_val_23561 === (8))){
var inst_23448 = (state_23560[(8)]);
var inst_23449 = (state_23560[(10)]);
var inst_23451 = (inst_23449 < inst_23448);
var inst_23456 = inst_23451;
var state_23560__$1 = state_23560;
if(cljs.core.truth_(inst_23456)){
var statearr_23712_25001 = state_23560__$1;
(statearr_23712_25001[(1)] = (10));

} else {
var statearr_23713_25003 = state_23560__$1;
(statearr_23713_25003[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__19581__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__19581__auto____0 = (function (){
var statearr_23715 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23715[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__19581__auto__);

(statearr_23715[(1)] = (1));

return statearr_23715;
});
var cljs$core$async$mapcat_STAR__$_state_machine__19581__auto____1 = (function (state_23560){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_23560);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23728){var ex__19584__auto__ = e23728;
var statearr_23735_25006 = state_23560;
(statearr_23735_25006[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_23560[(4)]))){
var statearr_23745_25007 = state_23560;
(statearr_23745_25007[(1)] = cljs.core.first((state_23560[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25008 = state_23560;
state_23560 = G__25008;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__19581__auto__ = function(state_23560){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__19581__auto____1.call(this,state_23560);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__19581__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__19581__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_23757 = f__20564__auto__();
(statearr_23757[(6)] = c__20563__auto__);

return statearr_23757;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));

return c__20563__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__23790 = arguments.length;
switch (G__23790) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__23809 = arguments.length;
switch (G__23809) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__23822 = arguments.length;
switch (G__23822) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__20563__auto___25013 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_23865){
var state_val_23866 = (state_23865[(1)]);
if((state_val_23866 === (7))){
var inst_23860 = (state_23865[(2)]);
var state_23865__$1 = state_23865;
var statearr_23870_25014 = state_23865__$1;
(statearr_23870_25014[(2)] = inst_23860);

(statearr_23870_25014[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (1))){
var inst_23842 = null;
var state_23865__$1 = (function (){var statearr_23871 = state_23865;
(statearr_23871[(7)] = inst_23842);

return statearr_23871;
})();
var statearr_23872_25015 = state_23865__$1;
(statearr_23872_25015[(2)] = null);

(statearr_23872_25015[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (4))){
var inst_23845 = (state_23865[(8)]);
var inst_23845__$1 = (state_23865[(2)]);
var inst_23846 = (inst_23845__$1 == null);
var inst_23847 = cljs.core.not(inst_23846);
var state_23865__$1 = (function (){var statearr_23873 = state_23865;
(statearr_23873[(8)] = inst_23845__$1);

return statearr_23873;
})();
if(inst_23847){
var statearr_23878_25020 = state_23865__$1;
(statearr_23878_25020[(1)] = (5));

} else {
var statearr_23879_25021 = state_23865__$1;
(statearr_23879_25021[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (6))){
var state_23865__$1 = state_23865;
var statearr_23880_25022 = state_23865__$1;
(statearr_23880_25022[(2)] = null);

(statearr_23880_25022[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (3))){
var inst_23862 = (state_23865[(2)]);
var inst_23863 = cljs.core.async.close_BANG_(out);
var state_23865__$1 = (function (){var statearr_23882 = state_23865;
(statearr_23882[(9)] = inst_23862);

return statearr_23882;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_23865__$1,inst_23863);
} else {
if((state_val_23866 === (2))){
var state_23865__$1 = state_23865;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23865__$1,(4),ch);
} else {
if((state_val_23866 === (11))){
var inst_23845 = (state_23865[(8)]);
var inst_23854 = (state_23865[(2)]);
var inst_23842 = inst_23845;
var state_23865__$1 = (function (){var statearr_23883 = state_23865;
(statearr_23883[(10)] = inst_23854);

(statearr_23883[(7)] = inst_23842);

return statearr_23883;
})();
var statearr_23884_25028 = state_23865__$1;
(statearr_23884_25028[(2)] = null);

(statearr_23884_25028[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (9))){
var inst_23845 = (state_23865[(8)]);
var state_23865__$1 = state_23865;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23865__$1,(11),out,inst_23845);
} else {
if((state_val_23866 === (5))){
var inst_23845 = (state_23865[(8)]);
var inst_23842 = (state_23865[(7)]);
var inst_23849 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_23845,inst_23842);
var state_23865__$1 = state_23865;
if(inst_23849){
var statearr_23886_25029 = state_23865__$1;
(statearr_23886_25029[(1)] = (8));

} else {
var statearr_23887_25030 = state_23865__$1;
(statearr_23887_25030[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (10))){
var inst_23857 = (state_23865[(2)]);
var state_23865__$1 = state_23865;
var statearr_23888_25031 = state_23865__$1;
(statearr_23888_25031[(2)] = inst_23857);

(statearr_23888_25031[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23866 === (8))){
var inst_23842 = (state_23865[(7)]);
var tmp23885 = inst_23842;
var inst_23842__$1 = tmp23885;
var state_23865__$1 = (function (){var statearr_23892 = state_23865;
(statearr_23892[(7)] = inst_23842__$1);

return statearr_23892;
})();
var statearr_23893_25032 = state_23865__$1;
(statearr_23893_25032[(2)] = null);

(statearr_23893_25032[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_23898 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_23898[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_23898[(1)] = (1));

return statearr_23898;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_23865){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_23865);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23899){var ex__19584__auto__ = e23899;
var statearr_23900_25033 = state_23865;
(statearr_23900_25033[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_23865[(4)]))){
var statearr_23901_25036 = state_23865;
(statearr_23901_25036[(1)] = cljs.core.first((state_23865[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25037 = state_23865;
state_23865 = G__25037;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_23865){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_23865);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_23905 = f__20564__auto__();
(statearr_23905[(6)] = c__20563__auto___25013);

return statearr_23905;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__23908 = arguments.length;
switch (G__23908) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__20563__auto___25041 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_23953){
var state_val_23954 = (state_23953[(1)]);
if((state_val_23954 === (7))){
var inst_23949 = (state_23953[(2)]);
var state_23953__$1 = state_23953;
var statearr_23959_25045 = state_23953__$1;
(statearr_23959_25045[(2)] = inst_23949);

(statearr_23959_25045[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (1))){
var inst_23909 = (new Array(n));
var inst_23910 = inst_23909;
var inst_23911 = (0);
var state_23953__$1 = (function (){var statearr_23960 = state_23953;
(statearr_23960[(7)] = inst_23910);

(statearr_23960[(8)] = inst_23911);

return statearr_23960;
})();
var statearr_23961_25049 = state_23953__$1;
(statearr_23961_25049[(2)] = null);

(statearr_23961_25049[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (4))){
var inst_23914 = (state_23953[(9)]);
var inst_23914__$1 = (state_23953[(2)]);
var inst_23915 = (inst_23914__$1 == null);
var inst_23916 = cljs.core.not(inst_23915);
var state_23953__$1 = (function (){var statearr_23962 = state_23953;
(statearr_23962[(9)] = inst_23914__$1);

return statearr_23962;
})();
if(inst_23916){
var statearr_23963_25052 = state_23953__$1;
(statearr_23963_25052[(1)] = (5));

} else {
var statearr_23967_25053 = state_23953__$1;
(statearr_23967_25053[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (15))){
var inst_23943 = (state_23953[(2)]);
var state_23953__$1 = state_23953;
var statearr_23968_25054 = state_23953__$1;
(statearr_23968_25054[(2)] = inst_23943);

(statearr_23968_25054[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (13))){
var state_23953__$1 = state_23953;
var statearr_23973_25055 = state_23953__$1;
(statearr_23973_25055[(2)] = null);

(statearr_23973_25055[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (6))){
var inst_23911 = (state_23953[(8)]);
var inst_23933 = (inst_23911 > (0));
var state_23953__$1 = state_23953;
if(cljs.core.truth_(inst_23933)){
var statearr_23974_25057 = state_23953__$1;
(statearr_23974_25057[(1)] = (12));

} else {
var statearr_23975_25058 = state_23953__$1;
(statearr_23975_25058[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (3))){
var inst_23951 = (state_23953[(2)]);
var state_23953__$1 = state_23953;
return cljs.core.async.impl.ioc_helpers.return_chan(state_23953__$1,inst_23951);
} else {
if((state_val_23954 === (12))){
var inst_23910 = (state_23953[(7)]);
var inst_23941 = cljs.core.vec(inst_23910);
var state_23953__$1 = state_23953;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23953__$1,(15),out,inst_23941);
} else {
if((state_val_23954 === (2))){
var state_23953__$1 = state_23953;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_23953__$1,(4),ch);
} else {
if((state_val_23954 === (11))){
var inst_23927 = (state_23953[(2)]);
var inst_23928 = (new Array(n));
var inst_23910 = inst_23928;
var inst_23911 = (0);
var state_23953__$1 = (function (){var statearr_23982 = state_23953;
(statearr_23982[(7)] = inst_23910);

(statearr_23982[(8)] = inst_23911);

(statearr_23982[(10)] = inst_23927);

return statearr_23982;
})();
var statearr_23983_25069 = state_23953__$1;
(statearr_23983_25069[(2)] = null);

(statearr_23983_25069[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (9))){
var inst_23910 = (state_23953[(7)]);
var inst_23925 = cljs.core.vec(inst_23910);
var state_23953__$1 = state_23953;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_23953__$1,(11),out,inst_23925);
} else {
if((state_val_23954 === (5))){
var inst_23914 = (state_23953[(9)]);
var inst_23910 = (state_23953[(7)]);
var inst_23919 = (state_23953[(11)]);
var inst_23911 = (state_23953[(8)]);
var inst_23918 = (inst_23910[inst_23911] = inst_23914);
var inst_23919__$1 = (inst_23911 + (1));
var inst_23920 = (inst_23919__$1 < n);
var state_23953__$1 = (function (){var statearr_23984 = state_23953;
(statearr_23984[(11)] = inst_23919__$1);

(statearr_23984[(12)] = inst_23918);

return statearr_23984;
})();
if(cljs.core.truth_(inst_23920)){
var statearr_23985_25071 = state_23953__$1;
(statearr_23985_25071[(1)] = (8));

} else {
var statearr_23986_25072 = state_23953__$1;
(statearr_23986_25072[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (14))){
var inst_23946 = (state_23953[(2)]);
var inst_23947 = cljs.core.async.close_BANG_(out);
var state_23953__$1 = (function (){var statearr_23988 = state_23953;
(statearr_23988[(13)] = inst_23946);

return statearr_23988;
})();
var statearr_23989_25073 = state_23953__$1;
(statearr_23989_25073[(2)] = inst_23947);

(statearr_23989_25073[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (10))){
var inst_23931 = (state_23953[(2)]);
var state_23953__$1 = state_23953;
var statearr_23990_25075 = state_23953__$1;
(statearr_23990_25075[(2)] = inst_23931);

(statearr_23990_25075[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23954 === (8))){
var inst_23910 = (state_23953[(7)]);
var inst_23919 = (state_23953[(11)]);
var tmp23987 = inst_23910;
var inst_23910__$1 = tmp23987;
var inst_23911 = inst_23919;
var state_23953__$1 = (function (){var statearr_23991 = state_23953;
(statearr_23991[(7)] = inst_23910__$1);

(statearr_23991[(8)] = inst_23911);

return statearr_23991;
})();
var statearr_23992_25076 = state_23953__$1;
(statearr_23992_25076[(2)] = null);

(statearr_23992_25076[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_23994 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23994[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_23994[(1)] = (1));

return statearr_23994;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_23953){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_23953);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e23997){var ex__19584__auto__ = e23997;
var statearr_23998_25080 = state_23953;
(statearr_23998_25080[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_23953[(4)]))){
var statearr_23999_25081 = state_23953;
(statearr_23999_25081[(1)] = cljs.core.first((state_23953[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25082 = state_23953;
state_23953 = G__25082;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_23953){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_23953);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_24009 = f__20564__auto__();
(statearr_24009[(6)] = c__20563__auto___25041);

return statearr_24009;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__24015 = arguments.length;
switch (G__24015) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__20563__auto___25089 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__20564__auto__ = (function (){var switch__19580__auto__ = (function (state_24060){
var state_val_24061 = (state_24060[(1)]);
if((state_val_24061 === (7))){
var inst_24056 = (state_24060[(2)]);
var state_24060__$1 = state_24060;
var statearr_24062_25090 = state_24060__$1;
(statearr_24062_25090[(2)] = inst_24056);

(statearr_24062_25090[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (1))){
var inst_24016 = [];
var inst_24017 = inst_24016;
var inst_24018 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_24060__$1 = (function (){var statearr_24063 = state_24060;
(statearr_24063[(7)] = inst_24017);

(statearr_24063[(8)] = inst_24018);

return statearr_24063;
})();
var statearr_24064_25091 = state_24060__$1;
(statearr_24064_25091[(2)] = null);

(statearr_24064_25091[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (4))){
var inst_24021 = (state_24060[(9)]);
var inst_24021__$1 = (state_24060[(2)]);
var inst_24022 = (inst_24021__$1 == null);
var inst_24023 = cljs.core.not(inst_24022);
var state_24060__$1 = (function (){var statearr_24065 = state_24060;
(statearr_24065[(9)] = inst_24021__$1);

return statearr_24065;
})();
if(inst_24023){
var statearr_24066_25092 = state_24060__$1;
(statearr_24066_25092[(1)] = (5));

} else {
var statearr_24067_25093 = state_24060__$1;
(statearr_24067_25093[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (15))){
var inst_24017 = (state_24060[(7)]);
var inst_24048 = cljs.core.vec(inst_24017);
var state_24060__$1 = state_24060;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_24060__$1,(18),out,inst_24048);
} else {
if((state_val_24061 === (13))){
var inst_24043 = (state_24060[(2)]);
var state_24060__$1 = state_24060;
var statearr_24068_25101 = state_24060__$1;
(statearr_24068_25101[(2)] = inst_24043);

(statearr_24068_25101[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (6))){
var inst_24017 = (state_24060[(7)]);
var inst_24045 = inst_24017.length;
var inst_24046 = (inst_24045 > (0));
var state_24060__$1 = state_24060;
if(cljs.core.truth_(inst_24046)){
var statearr_24069_25109 = state_24060__$1;
(statearr_24069_25109[(1)] = (15));

} else {
var statearr_24070_25111 = state_24060__$1;
(statearr_24070_25111[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (17))){
var inst_24053 = (state_24060[(2)]);
var inst_24054 = cljs.core.async.close_BANG_(out);
var state_24060__$1 = (function (){var statearr_24071 = state_24060;
(statearr_24071[(10)] = inst_24053);

return statearr_24071;
})();
var statearr_24072_25114 = state_24060__$1;
(statearr_24072_25114[(2)] = inst_24054);

(statearr_24072_25114[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (3))){
var inst_24058 = (state_24060[(2)]);
var state_24060__$1 = state_24060;
return cljs.core.async.impl.ioc_helpers.return_chan(state_24060__$1,inst_24058);
} else {
if((state_val_24061 === (12))){
var inst_24017 = (state_24060[(7)]);
var inst_24036 = cljs.core.vec(inst_24017);
var state_24060__$1 = state_24060;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_24060__$1,(14),out,inst_24036);
} else {
if((state_val_24061 === (2))){
var state_24060__$1 = state_24060;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_24060__$1,(4),ch);
} else {
if((state_val_24061 === (11))){
var inst_24017 = (state_24060[(7)]);
var inst_24025 = (state_24060[(11)]);
var inst_24021 = (state_24060[(9)]);
var inst_24033 = inst_24017.push(inst_24021);
var tmp24073 = inst_24017;
var inst_24017__$1 = tmp24073;
var inst_24018 = inst_24025;
var state_24060__$1 = (function (){var statearr_24074 = state_24060;
(statearr_24074[(7)] = inst_24017__$1);

(statearr_24074[(8)] = inst_24018);

(statearr_24074[(12)] = inst_24033);

return statearr_24074;
})();
var statearr_24075_25121 = state_24060__$1;
(statearr_24075_25121[(2)] = null);

(statearr_24075_25121[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (9))){
var inst_24018 = (state_24060[(8)]);
var inst_24029 = cljs.core.keyword_identical_QMARK_(inst_24018,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var state_24060__$1 = state_24060;
var statearr_24076_25122 = state_24060__$1;
(statearr_24076_25122[(2)] = inst_24029);

(statearr_24076_25122[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (5))){
var inst_24026 = (state_24060[(13)]);
var inst_24025 = (state_24060[(11)]);
var inst_24018 = (state_24060[(8)]);
var inst_24021 = (state_24060[(9)]);
var inst_24025__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_24021) : f.call(null,inst_24021));
var inst_24026__$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_24025__$1,inst_24018);
var state_24060__$1 = (function (){var statearr_24086 = state_24060;
(statearr_24086[(13)] = inst_24026__$1);

(statearr_24086[(11)] = inst_24025__$1);

return statearr_24086;
})();
if(inst_24026__$1){
var statearr_24087_25124 = state_24060__$1;
(statearr_24087_25124[(1)] = (8));

} else {
var statearr_24088_25129 = state_24060__$1;
(statearr_24088_25129[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (14))){
var inst_24025 = (state_24060[(11)]);
var inst_24021 = (state_24060[(9)]);
var inst_24038 = (state_24060[(2)]);
var inst_24039 = [];
var inst_24040 = inst_24039.push(inst_24021);
var inst_24017 = inst_24039;
var inst_24018 = inst_24025;
var state_24060__$1 = (function (){var statearr_24089 = state_24060;
(statearr_24089[(7)] = inst_24017);

(statearr_24089[(8)] = inst_24018);

(statearr_24089[(14)] = inst_24040);

(statearr_24089[(15)] = inst_24038);

return statearr_24089;
})();
var statearr_24094_25130 = state_24060__$1;
(statearr_24094_25130[(2)] = null);

(statearr_24094_25130[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (16))){
var state_24060__$1 = state_24060;
var statearr_24095_25131 = state_24060__$1;
(statearr_24095_25131[(2)] = null);

(statearr_24095_25131[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (10))){
var inst_24031 = (state_24060[(2)]);
var state_24060__$1 = state_24060;
if(cljs.core.truth_(inst_24031)){
var statearr_24096_25132 = state_24060__$1;
(statearr_24096_25132[(1)] = (11));

} else {
var statearr_24097_25133 = state_24060__$1;
(statearr_24097_25133[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (18))){
var inst_24050 = (state_24060[(2)]);
var state_24060__$1 = state_24060;
var statearr_24098_25134 = state_24060__$1;
(statearr_24098_25134[(2)] = inst_24050);

(statearr_24098_25134[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24061 === (8))){
var inst_24026 = (state_24060[(13)]);
var state_24060__$1 = state_24060;
var statearr_24099_25135 = state_24060__$1;
(statearr_24099_25135[(2)] = inst_24026);

(statearr_24099_25135[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__19581__auto__ = null;
var cljs$core$async$state_machine__19581__auto____0 = (function (){
var statearr_24100 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24100[(0)] = cljs$core$async$state_machine__19581__auto__);

(statearr_24100[(1)] = (1));

return statearr_24100;
});
var cljs$core$async$state_machine__19581__auto____1 = (function (state_24060){
while(true){
var ret_value__19582__auto__ = (function (){try{while(true){
var result__19583__auto__ = switch__19580__auto__(state_24060);
if(cljs.core.keyword_identical_QMARK_(result__19583__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19583__auto__;
}
break;
}
}catch (e24101){var ex__19584__auto__ = e24101;
var statearr_24102_25144 = state_24060;
(statearr_24102_25144[(2)] = ex__19584__auto__);


if(cljs.core.seq((state_24060[(4)]))){
var statearr_24103_25145 = state_24060;
(statearr_24103_25145[(1)] = cljs.core.first((state_24060[(4)])));

} else {
throw ex__19584__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__19582__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25146 = state_24060;
state_24060 = G__25146;
continue;
} else {
return ret_value__19582__auto__;
}
break;
}
});
cljs$core$async$state_machine__19581__auto__ = function(state_24060){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19581__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19581__auto____1.call(this,state_24060);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19581__auto____0;
cljs$core$async$state_machine__19581__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19581__auto____1;
return cljs$core$async$state_machine__19581__auto__;
})()
})();
var state__20565__auto__ = (function (){var statearr_24104 = f__20564__auto__();
(statearr_24104[(6)] = c__20563__auto___25089);

return statearr_24104;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__20565__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
