goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__5732__auto__ = [];
var len__5726__auto___26908 = arguments.length;
var i__5727__auto___26909 = (0);
while(true){
if((i__5727__auto___26909 < len__5726__auto___26908)){
args__5732__auto__.push((arguments[i__5727__auto___26909]));

var G__26910 = (i__5727__auto___26909 + (1));
i__5727__auto___26909 = G__26910;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq26628){
var G__26629 = cljs.core.first(seq26628);
var seq26628__$1 = cljs.core.next(seq26628);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__26629,seq26628__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__26630 = cljs.core.seq(sources);
var chunk__26631 = null;
var count__26632 = (0);
var i__26633 = (0);
while(true){
if((i__26633 < count__26632)){
var map__26640 = chunk__26631.cljs$core$IIndexed$_nth$arity$2(null,i__26633);
var map__26640__$1 = cljs.core.__destructure_map(map__26640);
var src = map__26640__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26640__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26640__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26640__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26640__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e26641){var e_26911 = e26641;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_26911);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_26911.message)].join('')));
}

var G__26912 = seq__26630;
var G__26913 = chunk__26631;
var G__26914 = count__26632;
var G__26915 = (i__26633 + (1));
seq__26630 = G__26912;
chunk__26631 = G__26913;
count__26632 = G__26914;
i__26633 = G__26915;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__26630);
if(temp__5804__auto__){
var seq__26630__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__26630__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__26630__$1);
var G__26916 = cljs.core.chunk_rest(seq__26630__$1);
var G__26917 = c__5525__auto__;
var G__26918 = cljs.core.count(c__5525__auto__);
var G__26919 = (0);
seq__26630 = G__26916;
chunk__26631 = G__26917;
count__26632 = G__26918;
i__26633 = G__26919;
continue;
} else {
var map__26642 = cljs.core.first(seq__26630__$1);
var map__26642__$1 = cljs.core.__destructure_map(map__26642);
var src = map__26642__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26642__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26642__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26642__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26642__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e26643){var e_26920 = e26643;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_26920);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_26920.message)].join('')));
}

var G__26921 = cljs.core.next(seq__26630__$1);
var G__26922 = null;
var G__26923 = (0);
var G__26924 = (0);
seq__26630 = G__26921;
chunk__26631 = G__26922;
count__26632 = G__26923;
i__26633 = G__26924;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__26644 = cljs.core.seq(js_requires);
var chunk__26645 = null;
var count__26646 = (0);
var i__26647 = (0);
while(true){
if((i__26647 < count__26646)){
var js_ns = chunk__26645.cljs$core$IIndexed$_nth$arity$2(null,i__26647);
var require_str_26925 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_26925);


var G__26926 = seq__26644;
var G__26927 = chunk__26645;
var G__26928 = count__26646;
var G__26929 = (i__26647 + (1));
seq__26644 = G__26926;
chunk__26645 = G__26927;
count__26646 = G__26928;
i__26647 = G__26929;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__26644);
if(temp__5804__auto__){
var seq__26644__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__26644__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__26644__$1);
var G__26930 = cljs.core.chunk_rest(seq__26644__$1);
var G__26931 = c__5525__auto__;
var G__26932 = cljs.core.count(c__5525__auto__);
var G__26933 = (0);
seq__26644 = G__26930;
chunk__26645 = G__26931;
count__26646 = G__26932;
i__26647 = G__26933;
continue;
} else {
var js_ns = cljs.core.first(seq__26644__$1);
var require_str_26934 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_26934);


var G__26935 = cljs.core.next(seq__26644__$1);
var G__26936 = null;
var G__26937 = (0);
var G__26938 = (0);
seq__26644 = G__26935;
chunk__26645 = G__26936;
count__26646 = G__26937;
i__26647 = G__26938;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__26649){
var map__26650 = p__26649;
var map__26650__$1 = cljs.core.__destructure_map(map__26650);
var msg = map__26650__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26650__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26650__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__5480__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26651(s__26652){
return (new cljs.core.LazySeq(null,(function (){
var s__26652__$1 = s__26652;
while(true){
var temp__5804__auto__ = cljs.core.seq(s__26652__$1);
if(temp__5804__auto__){
var xs__6360__auto__ = temp__5804__auto__;
var map__26657 = cljs.core.first(xs__6360__auto__);
var map__26657__$1 = cljs.core.__destructure_map(map__26657);
var src = map__26657__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26657__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26657__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__5476__auto__ = ((function (s__26652__$1,map__26657,map__26657__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26650,map__26650__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26651_$_iter__26653(s__26654){
return (new cljs.core.LazySeq(null,((function (s__26652__$1,map__26657,map__26657__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26650,map__26650__$1,msg,info,reload_info){
return (function (){
var s__26654__$1 = s__26654;
while(true){
var temp__5804__auto____$1 = cljs.core.seq(s__26654__$1);
if(temp__5804__auto____$1){
var s__26654__$2 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__26654__$2)){
var c__5478__auto__ = cljs.core.chunk_first(s__26654__$2);
var size__5479__auto__ = cljs.core.count(c__5478__auto__);
var b__26656 = cljs.core.chunk_buffer(size__5479__auto__);
if((function (){var i__26655 = (0);
while(true){
if((i__26655 < size__5479__auto__)){
var warning = cljs.core._nth(c__5478__auto__,i__26655);
cljs.core.chunk_append(b__26656,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__26939 = (i__26655 + (1));
i__26655 = G__26939;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__26656),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26651_$_iter__26653(cljs.core.chunk_rest(s__26654__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__26656),null);
}
} else {
var warning = cljs.core.first(s__26654__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26651_$_iter__26653(cljs.core.rest(s__26654__$2)));
}
} else {
return null;
}
break;
}
});})(s__26652__$1,map__26657,map__26657__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26650,map__26650__$1,msg,info,reload_info))
,null,null));
});})(s__26652__$1,map__26657,map__26657__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__26650,map__26650__$1,msg,info,reload_info))
;
var fs__5477__auto__ = cljs.core.seq(iterys__5476__auto__(warnings));
if(fs__5477__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__5477__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__26651(cljs.core.rest(s__26652__$1)));
} else {
var G__26940 = cljs.core.rest(s__26652__$1);
s__26652__$1 = G__26940;
continue;
}
} else {
var G__26941 = cljs.core.rest(s__26652__$1);
s__26652__$1 = G__26941;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__5480__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__26658_26942 = cljs.core.seq(warnings);
var chunk__26659_26943 = null;
var count__26660_26944 = (0);
var i__26661_26945 = (0);
while(true){
if((i__26661_26945 < count__26660_26944)){
var map__26664_26946 = chunk__26659_26943.cljs$core$IIndexed$_nth$arity$2(null,i__26661_26945);
var map__26664_26947__$1 = cljs.core.__destructure_map(map__26664_26946);
var w_26948 = map__26664_26947__$1;
var msg_26949__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26664_26947__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_26950 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26664_26947__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_26951 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26664_26947__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_26952 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26664_26947__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_26952)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_26950),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_26951),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_26949__$1)].join(''));


var G__26953 = seq__26658_26942;
var G__26954 = chunk__26659_26943;
var G__26955 = count__26660_26944;
var G__26956 = (i__26661_26945 + (1));
seq__26658_26942 = G__26953;
chunk__26659_26943 = G__26954;
count__26660_26944 = G__26955;
i__26661_26945 = G__26956;
continue;
} else {
var temp__5804__auto___26957 = cljs.core.seq(seq__26658_26942);
if(temp__5804__auto___26957){
var seq__26658_26958__$1 = temp__5804__auto___26957;
if(cljs.core.chunked_seq_QMARK_(seq__26658_26958__$1)){
var c__5525__auto___26959 = cljs.core.chunk_first(seq__26658_26958__$1);
var G__26960 = cljs.core.chunk_rest(seq__26658_26958__$1);
var G__26961 = c__5525__auto___26959;
var G__26962 = cljs.core.count(c__5525__auto___26959);
var G__26963 = (0);
seq__26658_26942 = G__26960;
chunk__26659_26943 = G__26961;
count__26660_26944 = G__26962;
i__26661_26945 = G__26963;
continue;
} else {
var map__26665_26964 = cljs.core.first(seq__26658_26958__$1);
var map__26665_26965__$1 = cljs.core.__destructure_map(map__26665_26964);
var w_26966 = map__26665_26965__$1;
var msg_26967__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26665_26965__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_26968 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26665_26965__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_26969 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26665_26965__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_26970 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26665_26965__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_26970)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_26968),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_26969),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_26967__$1)].join(''));


var G__26971 = cljs.core.next(seq__26658_26958__$1);
var G__26972 = null;
var G__26973 = (0);
var G__26974 = (0);
seq__26658_26942 = G__26971;
chunk__26659_26943 = G__26972;
count__26660_26944 = G__26973;
i__26661_26945 = G__26974;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__26648_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__26648_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__5000__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__5000__auto__){
var and__5000__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__5000__auto____$1){
return new$;
} else {
return and__5000__auto____$1;
}
} else {
return and__5000__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__26666){
var map__26667 = p__26666;
var map__26667__$1 = cljs.core.__destructure_map(map__26667);
var msg = map__26667__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26667__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26667__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var seq__26668 = cljs.core.seq(updates);
var chunk__26670 = null;
var count__26671 = (0);
var i__26672 = (0);
while(true){
if((i__26672 < count__26671)){
var path = chunk__26670.cljs$core$IIndexed$_nth$arity$2(null,i__26672);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__26782_26975 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__26786_26976 = null;
var count__26787_26977 = (0);
var i__26788_26978 = (0);
while(true){
if((i__26788_26978 < count__26787_26977)){
var node_26979 = chunk__26786_26976.cljs$core$IIndexed$_nth$arity$2(null,i__26788_26978);
if(cljs.core.not(node_26979.shadow$old)){
var path_match_26980 = shadow.cljs.devtools.client.browser.match_paths(node_26979.getAttribute("href"),path);
if(cljs.core.truth_(path_match_26980)){
var new_link_26981 = (function (){var G__26814 = node_26979.cloneNode(true);
G__26814.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_26980),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__26814;
})();
(node_26979.shadow$old = true);

(new_link_26981.onload = ((function (seq__26782_26975,chunk__26786_26976,count__26787_26977,i__26788_26978,seq__26668,chunk__26670,count__26671,i__26672,new_link_26981,path_match_26980,node_26979,path,map__26667,map__26667__$1,msg,updates,reload_info){
return (function (e){
var seq__26815_26982 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__26817_26983 = null;
var count__26818_26984 = (0);
var i__26819_26985 = (0);
while(true){
if((i__26819_26985 < count__26818_26984)){
var map__26823_26986 = chunk__26817_26983.cljs$core$IIndexed$_nth$arity$2(null,i__26819_26985);
var map__26823_26987__$1 = cljs.core.__destructure_map(map__26823_26986);
var task_26988 = map__26823_26987__$1;
var fn_str_26989 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26823_26987__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_26990 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26823_26987__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_26991 = goog.getObjectByName(fn_str_26989,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_26990)].join(''));

(fn_obj_26991.cljs$core$IFn$_invoke$arity$2 ? fn_obj_26991.cljs$core$IFn$_invoke$arity$2(path,new_link_26981) : fn_obj_26991.call(null,path,new_link_26981));


var G__26992 = seq__26815_26982;
var G__26993 = chunk__26817_26983;
var G__26994 = count__26818_26984;
var G__26995 = (i__26819_26985 + (1));
seq__26815_26982 = G__26992;
chunk__26817_26983 = G__26993;
count__26818_26984 = G__26994;
i__26819_26985 = G__26995;
continue;
} else {
var temp__5804__auto___26996 = cljs.core.seq(seq__26815_26982);
if(temp__5804__auto___26996){
var seq__26815_26997__$1 = temp__5804__auto___26996;
if(cljs.core.chunked_seq_QMARK_(seq__26815_26997__$1)){
var c__5525__auto___26998 = cljs.core.chunk_first(seq__26815_26997__$1);
var G__26999 = cljs.core.chunk_rest(seq__26815_26997__$1);
var G__27000 = c__5525__auto___26998;
var G__27001 = cljs.core.count(c__5525__auto___26998);
var G__27002 = (0);
seq__26815_26982 = G__26999;
chunk__26817_26983 = G__27000;
count__26818_26984 = G__27001;
i__26819_26985 = G__27002;
continue;
} else {
var map__26824_27003 = cljs.core.first(seq__26815_26997__$1);
var map__26824_27004__$1 = cljs.core.__destructure_map(map__26824_27003);
var task_27005 = map__26824_27004__$1;
var fn_str_27006 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26824_27004__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27007 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26824_27004__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27008 = goog.getObjectByName(fn_str_27006,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27007)].join(''));

(fn_obj_27008.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27008.cljs$core$IFn$_invoke$arity$2(path,new_link_26981) : fn_obj_27008.call(null,path,new_link_26981));


var G__27009 = cljs.core.next(seq__26815_26997__$1);
var G__27010 = null;
var G__27011 = (0);
var G__27012 = (0);
seq__26815_26982 = G__27009;
chunk__26817_26983 = G__27010;
count__26818_26984 = G__27011;
i__26819_26985 = G__27012;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_26979);
});})(seq__26782_26975,chunk__26786_26976,count__26787_26977,i__26788_26978,seq__26668,chunk__26670,count__26671,i__26672,new_link_26981,path_match_26980,node_26979,path,map__26667,map__26667__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_26980], 0));

goog.dom.insertSiblingAfter(new_link_26981,node_26979);


var G__27013 = seq__26782_26975;
var G__27014 = chunk__26786_26976;
var G__27015 = count__26787_26977;
var G__27016 = (i__26788_26978 + (1));
seq__26782_26975 = G__27013;
chunk__26786_26976 = G__27014;
count__26787_26977 = G__27015;
i__26788_26978 = G__27016;
continue;
} else {
var G__27017 = seq__26782_26975;
var G__27018 = chunk__26786_26976;
var G__27019 = count__26787_26977;
var G__27020 = (i__26788_26978 + (1));
seq__26782_26975 = G__27017;
chunk__26786_26976 = G__27018;
count__26787_26977 = G__27019;
i__26788_26978 = G__27020;
continue;
}
} else {
var G__27021 = seq__26782_26975;
var G__27022 = chunk__26786_26976;
var G__27023 = count__26787_26977;
var G__27024 = (i__26788_26978 + (1));
seq__26782_26975 = G__27021;
chunk__26786_26976 = G__27022;
count__26787_26977 = G__27023;
i__26788_26978 = G__27024;
continue;
}
} else {
var temp__5804__auto___27025 = cljs.core.seq(seq__26782_26975);
if(temp__5804__auto___27025){
var seq__26782_27026__$1 = temp__5804__auto___27025;
if(cljs.core.chunked_seq_QMARK_(seq__26782_27026__$1)){
var c__5525__auto___27027 = cljs.core.chunk_first(seq__26782_27026__$1);
var G__27028 = cljs.core.chunk_rest(seq__26782_27026__$1);
var G__27029 = c__5525__auto___27027;
var G__27030 = cljs.core.count(c__5525__auto___27027);
var G__27031 = (0);
seq__26782_26975 = G__27028;
chunk__26786_26976 = G__27029;
count__26787_26977 = G__27030;
i__26788_26978 = G__27031;
continue;
} else {
var node_27032 = cljs.core.first(seq__26782_27026__$1);
if(cljs.core.not(node_27032.shadow$old)){
var path_match_27033 = shadow.cljs.devtools.client.browser.match_paths(node_27032.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27033)){
var new_link_27034 = (function (){var G__26825 = node_27032.cloneNode(true);
G__26825.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27033),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__26825;
})();
(node_27032.shadow$old = true);

(new_link_27034.onload = ((function (seq__26782_26975,chunk__26786_26976,count__26787_26977,i__26788_26978,seq__26668,chunk__26670,count__26671,i__26672,new_link_27034,path_match_27033,node_27032,seq__26782_27026__$1,temp__5804__auto___27025,path,map__26667,map__26667__$1,msg,updates,reload_info){
return (function (e){
var seq__26826_27035 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__26828_27036 = null;
var count__26829_27037 = (0);
var i__26830_27038 = (0);
while(true){
if((i__26830_27038 < count__26829_27037)){
var map__26834_27039 = chunk__26828_27036.cljs$core$IIndexed$_nth$arity$2(null,i__26830_27038);
var map__26834_27040__$1 = cljs.core.__destructure_map(map__26834_27039);
var task_27041 = map__26834_27040__$1;
var fn_str_27042 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26834_27040__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27043 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26834_27040__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27044 = goog.getObjectByName(fn_str_27042,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27043)].join(''));

(fn_obj_27044.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27044.cljs$core$IFn$_invoke$arity$2(path,new_link_27034) : fn_obj_27044.call(null,path,new_link_27034));


var G__27045 = seq__26826_27035;
var G__27046 = chunk__26828_27036;
var G__27047 = count__26829_27037;
var G__27048 = (i__26830_27038 + (1));
seq__26826_27035 = G__27045;
chunk__26828_27036 = G__27046;
count__26829_27037 = G__27047;
i__26830_27038 = G__27048;
continue;
} else {
var temp__5804__auto___27049__$1 = cljs.core.seq(seq__26826_27035);
if(temp__5804__auto___27049__$1){
var seq__26826_27050__$1 = temp__5804__auto___27049__$1;
if(cljs.core.chunked_seq_QMARK_(seq__26826_27050__$1)){
var c__5525__auto___27051 = cljs.core.chunk_first(seq__26826_27050__$1);
var G__27052 = cljs.core.chunk_rest(seq__26826_27050__$1);
var G__27053 = c__5525__auto___27051;
var G__27054 = cljs.core.count(c__5525__auto___27051);
var G__27055 = (0);
seq__26826_27035 = G__27052;
chunk__26828_27036 = G__27053;
count__26829_27037 = G__27054;
i__26830_27038 = G__27055;
continue;
} else {
var map__26835_27056 = cljs.core.first(seq__26826_27050__$1);
var map__26835_27057__$1 = cljs.core.__destructure_map(map__26835_27056);
var task_27058 = map__26835_27057__$1;
var fn_str_27059 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26835_27057__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27060 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26835_27057__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27061 = goog.getObjectByName(fn_str_27059,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27060)].join(''));

(fn_obj_27061.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27061.cljs$core$IFn$_invoke$arity$2(path,new_link_27034) : fn_obj_27061.call(null,path,new_link_27034));


var G__27062 = cljs.core.next(seq__26826_27050__$1);
var G__27063 = null;
var G__27064 = (0);
var G__27065 = (0);
seq__26826_27035 = G__27062;
chunk__26828_27036 = G__27063;
count__26829_27037 = G__27064;
i__26830_27038 = G__27065;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27032);
});})(seq__26782_26975,chunk__26786_26976,count__26787_26977,i__26788_26978,seq__26668,chunk__26670,count__26671,i__26672,new_link_27034,path_match_27033,node_27032,seq__26782_27026__$1,temp__5804__auto___27025,path,map__26667,map__26667__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27033], 0));

goog.dom.insertSiblingAfter(new_link_27034,node_27032);


var G__27066 = cljs.core.next(seq__26782_27026__$1);
var G__27067 = null;
var G__27068 = (0);
var G__27069 = (0);
seq__26782_26975 = G__27066;
chunk__26786_26976 = G__27067;
count__26787_26977 = G__27068;
i__26788_26978 = G__27069;
continue;
} else {
var G__27070 = cljs.core.next(seq__26782_27026__$1);
var G__27071 = null;
var G__27072 = (0);
var G__27073 = (0);
seq__26782_26975 = G__27070;
chunk__26786_26976 = G__27071;
count__26787_26977 = G__27072;
i__26788_26978 = G__27073;
continue;
}
} else {
var G__27074 = cljs.core.next(seq__26782_27026__$1);
var G__27075 = null;
var G__27076 = (0);
var G__27077 = (0);
seq__26782_26975 = G__27074;
chunk__26786_26976 = G__27075;
count__26787_26977 = G__27076;
i__26788_26978 = G__27077;
continue;
}
}
} else {
}
}
break;
}


var G__27078 = seq__26668;
var G__27079 = chunk__26670;
var G__27080 = count__26671;
var G__27081 = (i__26672 + (1));
seq__26668 = G__27078;
chunk__26670 = G__27079;
count__26671 = G__27080;
i__26672 = G__27081;
continue;
} else {
var G__27082 = seq__26668;
var G__27083 = chunk__26670;
var G__27084 = count__26671;
var G__27085 = (i__26672 + (1));
seq__26668 = G__27082;
chunk__26670 = G__27083;
count__26671 = G__27084;
i__26672 = G__27085;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__26668);
if(temp__5804__auto__){
var seq__26668__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__26668__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__26668__$1);
var G__27086 = cljs.core.chunk_rest(seq__26668__$1);
var G__27087 = c__5525__auto__;
var G__27088 = cljs.core.count(c__5525__auto__);
var G__27089 = (0);
seq__26668 = G__27086;
chunk__26670 = G__27087;
count__26671 = G__27088;
i__26672 = G__27089;
continue;
} else {
var path = cljs.core.first(seq__26668__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__26836_27090 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__26840_27091 = null;
var count__26841_27092 = (0);
var i__26842_27093 = (0);
while(true){
if((i__26842_27093 < count__26841_27092)){
var node_27094 = chunk__26840_27091.cljs$core$IIndexed$_nth$arity$2(null,i__26842_27093);
if(cljs.core.not(node_27094.shadow$old)){
var path_match_27095 = shadow.cljs.devtools.client.browser.match_paths(node_27094.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27095)){
var new_link_27096 = (function (){var G__26868 = node_27094.cloneNode(true);
G__26868.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27095),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__26868;
})();
(node_27094.shadow$old = true);

(new_link_27096.onload = ((function (seq__26836_27090,chunk__26840_27091,count__26841_27092,i__26842_27093,seq__26668,chunk__26670,count__26671,i__26672,new_link_27096,path_match_27095,node_27094,path,seq__26668__$1,temp__5804__auto__,map__26667,map__26667__$1,msg,updates,reload_info){
return (function (e){
var seq__26869_27097 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__26871_27098 = null;
var count__26872_27099 = (0);
var i__26873_27100 = (0);
while(true){
if((i__26873_27100 < count__26872_27099)){
var map__26877_27101 = chunk__26871_27098.cljs$core$IIndexed$_nth$arity$2(null,i__26873_27100);
var map__26877_27102__$1 = cljs.core.__destructure_map(map__26877_27101);
var task_27103 = map__26877_27102__$1;
var fn_str_27104 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26877_27102__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27105 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26877_27102__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27106 = goog.getObjectByName(fn_str_27104,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27105)].join(''));

(fn_obj_27106.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27106.cljs$core$IFn$_invoke$arity$2(path,new_link_27096) : fn_obj_27106.call(null,path,new_link_27096));


var G__27107 = seq__26869_27097;
var G__27108 = chunk__26871_27098;
var G__27109 = count__26872_27099;
var G__27110 = (i__26873_27100 + (1));
seq__26869_27097 = G__27107;
chunk__26871_27098 = G__27108;
count__26872_27099 = G__27109;
i__26873_27100 = G__27110;
continue;
} else {
var temp__5804__auto___27111__$1 = cljs.core.seq(seq__26869_27097);
if(temp__5804__auto___27111__$1){
var seq__26869_27112__$1 = temp__5804__auto___27111__$1;
if(cljs.core.chunked_seq_QMARK_(seq__26869_27112__$1)){
var c__5525__auto___27113 = cljs.core.chunk_first(seq__26869_27112__$1);
var G__27114 = cljs.core.chunk_rest(seq__26869_27112__$1);
var G__27115 = c__5525__auto___27113;
var G__27116 = cljs.core.count(c__5525__auto___27113);
var G__27117 = (0);
seq__26869_27097 = G__27114;
chunk__26871_27098 = G__27115;
count__26872_27099 = G__27116;
i__26873_27100 = G__27117;
continue;
} else {
var map__26878_27118 = cljs.core.first(seq__26869_27112__$1);
var map__26878_27119__$1 = cljs.core.__destructure_map(map__26878_27118);
var task_27120 = map__26878_27119__$1;
var fn_str_27121 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26878_27119__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27122 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26878_27119__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27123 = goog.getObjectByName(fn_str_27121,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27122)].join(''));

(fn_obj_27123.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27123.cljs$core$IFn$_invoke$arity$2(path,new_link_27096) : fn_obj_27123.call(null,path,new_link_27096));


var G__27124 = cljs.core.next(seq__26869_27112__$1);
var G__27125 = null;
var G__27126 = (0);
var G__27127 = (0);
seq__26869_27097 = G__27124;
chunk__26871_27098 = G__27125;
count__26872_27099 = G__27126;
i__26873_27100 = G__27127;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27094);
});})(seq__26836_27090,chunk__26840_27091,count__26841_27092,i__26842_27093,seq__26668,chunk__26670,count__26671,i__26672,new_link_27096,path_match_27095,node_27094,path,seq__26668__$1,temp__5804__auto__,map__26667,map__26667__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27095], 0));

goog.dom.insertSiblingAfter(new_link_27096,node_27094);


var G__27128 = seq__26836_27090;
var G__27129 = chunk__26840_27091;
var G__27130 = count__26841_27092;
var G__27131 = (i__26842_27093 + (1));
seq__26836_27090 = G__27128;
chunk__26840_27091 = G__27129;
count__26841_27092 = G__27130;
i__26842_27093 = G__27131;
continue;
} else {
var G__27132 = seq__26836_27090;
var G__27133 = chunk__26840_27091;
var G__27134 = count__26841_27092;
var G__27135 = (i__26842_27093 + (1));
seq__26836_27090 = G__27132;
chunk__26840_27091 = G__27133;
count__26841_27092 = G__27134;
i__26842_27093 = G__27135;
continue;
}
} else {
var G__27136 = seq__26836_27090;
var G__27137 = chunk__26840_27091;
var G__27138 = count__26841_27092;
var G__27139 = (i__26842_27093 + (1));
seq__26836_27090 = G__27136;
chunk__26840_27091 = G__27137;
count__26841_27092 = G__27138;
i__26842_27093 = G__27139;
continue;
}
} else {
var temp__5804__auto___27140__$1 = cljs.core.seq(seq__26836_27090);
if(temp__5804__auto___27140__$1){
var seq__26836_27141__$1 = temp__5804__auto___27140__$1;
if(cljs.core.chunked_seq_QMARK_(seq__26836_27141__$1)){
var c__5525__auto___27142 = cljs.core.chunk_first(seq__26836_27141__$1);
var G__27143 = cljs.core.chunk_rest(seq__26836_27141__$1);
var G__27144 = c__5525__auto___27142;
var G__27145 = cljs.core.count(c__5525__auto___27142);
var G__27146 = (0);
seq__26836_27090 = G__27143;
chunk__26840_27091 = G__27144;
count__26841_27092 = G__27145;
i__26842_27093 = G__27146;
continue;
} else {
var node_27147 = cljs.core.first(seq__26836_27141__$1);
if(cljs.core.not(node_27147.shadow$old)){
var path_match_27148 = shadow.cljs.devtools.client.browser.match_paths(node_27147.getAttribute("href"),path);
if(cljs.core.truth_(path_match_27148)){
var new_link_27149 = (function (){var G__26879 = node_27147.cloneNode(true);
G__26879.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_27148),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__26879;
})();
(node_27147.shadow$old = true);

(new_link_27149.onload = ((function (seq__26836_27090,chunk__26840_27091,count__26841_27092,i__26842_27093,seq__26668,chunk__26670,count__26671,i__26672,new_link_27149,path_match_27148,node_27147,seq__26836_27141__$1,temp__5804__auto___27140__$1,path,seq__26668__$1,temp__5804__auto__,map__26667,map__26667__$1,msg,updates,reload_info){
return (function (e){
var seq__26880_27150 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__26882_27151 = null;
var count__26883_27152 = (0);
var i__26884_27153 = (0);
while(true){
if((i__26884_27153 < count__26883_27152)){
var map__26888_27154 = chunk__26882_27151.cljs$core$IIndexed$_nth$arity$2(null,i__26884_27153);
var map__26888_27155__$1 = cljs.core.__destructure_map(map__26888_27154);
var task_27156 = map__26888_27155__$1;
var fn_str_27157 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26888_27155__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27158 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26888_27155__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27159 = goog.getObjectByName(fn_str_27157,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27158)].join(''));

(fn_obj_27159.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27159.cljs$core$IFn$_invoke$arity$2(path,new_link_27149) : fn_obj_27159.call(null,path,new_link_27149));


var G__27160 = seq__26880_27150;
var G__27161 = chunk__26882_27151;
var G__27162 = count__26883_27152;
var G__27163 = (i__26884_27153 + (1));
seq__26880_27150 = G__27160;
chunk__26882_27151 = G__27161;
count__26883_27152 = G__27162;
i__26884_27153 = G__27163;
continue;
} else {
var temp__5804__auto___27164__$2 = cljs.core.seq(seq__26880_27150);
if(temp__5804__auto___27164__$2){
var seq__26880_27165__$1 = temp__5804__auto___27164__$2;
if(cljs.core.chunked_seq_QMARK_(seq__26880_27165__$1)){
var c__5525__auto___27166 = cljs.core.chunk_first(seq__26880_27165__$1);
var G__27167 = cljs.core.chunk_rest(seq__26880_27165__$1);
var G__27168 = c__5525__auto___27166;
var G__27169 = cljs.core.count(c__5525__auto___27166);
var G__27170 = (0);
seq__26880_27150 = G__27167;
chunk__26882_27151 = G__27168;
count__26883_27152 = G__27169;
i__26884_27153 = G__27170;
continue;
} else {
var map__26889_27171 = cljs.core.first(seq__26880_27165__$1);
var map__26889_27172__$1 = cljs.core.__destructure_map(map__26889_27171);
var task_27173 = map__26889_27172__$1;
var fn_str_27174 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26889_27172__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_27175 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26889_27172__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_27176 = goog.getObjectByName(fn_str_27174,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_27175)].join(''));

(fn_obj_27176.cljs$core$IFn$_invoke$arity$2 ? fn_obj_27176.cljs$core$IFn$_invoke$arity$2(path,new_link_27149) : fn_obj_27176.call(null,path,new_link_27149));


var G__27177 = cljs.core.next(seq__26880_27165__$1);
var G__27178 = null;
var G__27179 = (0);
var G__27180 = (0);
seq__26880_27150 = G__27177;
chunk__26882_27151 = G__27178;
count__26883_27152 = G__27179;
i__26884_27153 = G__27180;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_27147);
});})(seq__26836_27090,chunk__26840_27091,count__26841_27092,i__26842_27093,seq__26668,chunk__26670,count__26671,i__26672,new_link_27149,path_match_27148,node_27147,seq__26836_27141__$1,temp__5804__auto___27140__$1,path,seq__26668__$1,temp__5804__auto__,map__26667,map__26667__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_27148], 0));

goog.dom.insertSiblingAfter(new_link_27149,node_27147);


var G__27181 = cljs.core.next(seq__26836_27141__$1);
var G__27182 = null;
var G__27183 = (0);
var G__27184 = (0);
seq__26836_27090 = G__27181;
chunk__26840_27091 = G__27182;
count__26841_27092 = G__27183;
i__26842_27093 = G__27184;
continue;
} else {
var G__27185 = cljs.core.next(seq__26836_27141__$1);
var G__27186 = null;
var G__27187 = (0);
var G__27188 = (0);
seq__26836_27090 = G__27185;
chunk__26840_27091 = G__27186;
count__26841_27092 = G__27187;
i__26842_27093 = G__27188;
continue;
}
} else {
var G__27189 = cljs.core.next(seq__26836_27141__$1);
var G__27190 = null;
var G__27191 = (0);
var G__27192 = (0);
seq__26836_27090 = G__27189;
chunk__26840_27091 = G__27190;
count__26841_27092 = G__27191;
i__26842_27093 = G__27192;
continue;
}
}
} else {
}
}
break;
}


var G__27193 = cljs.core.next(seq__26668__$1);
var G__27194 = null;
var G__27195 = (0);
var G__27196 = (0);
seq__26668 = G__27193;
chunk__26670 = G__27194;
count__26671 = G__27195;
i__26672 = G__27196;
continue;
} else {
var G__27197 = cljs.core.next(seq__26668__$1);
var G__27198 = null;
var G__27199 = (0);
var G__27200 = (0);
seq__26668 = G__27197;
chunk__26670 = G__27198;
count__26671 = G__27199;
i__26672 = G__27200;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.runtime_info = (((typeof SHADOW_CONFIG !== 'undefined'))?shadow.json.to_clj.cljs$core$IFn$_invoke$arity$1(SHADOW_CONFIG):null);
shadow.cljs.devtools.client.browser.client_info = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([shadow.cljs.devtools.client.browser.runtime_info,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null)], 0));
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$3 = (function (this$,ns,p__26890){
var map__26891 = p__26890;
var map__26891__$1 = cljs.core.__destructure_map(map__26891);
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26891__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__26892,done,error){
var map__26893 = p__26892;
var map__26893__$1 = cljs.core.__destructure_map(map__26893);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26893__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__26894,done,error){
var map__26895 = p__26894;
var map__26895__$1 = cljs.core.__destructure_map(map__26895);
var msg = map__26895__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26895__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26895__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26895__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__26896){
var map__26897 = p__26896;
var map__26897__$1 = cljs.core.__destructure_map(map__26897);
var src = map__26897__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26897__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__5000__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__5000__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__5000__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__26898 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__26898) : done.call(null,G__26898));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__26899){
var map__26900 = p__26899;
var map__26900__$1 = cljs.core.__destructure_map(map__26900);
var msg__$1 = map__26900__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26900__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e26901){var ex = e26901;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__26902){
var map__26903 = p__26902;
var map__26903__$1 = cljs.core.__destructure_map(map__26903);
var env = map__26903__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26903__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (msg){
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__26904){
var map__26905 = p__26904;
var map__26905__$1 = cljs.core.__destructure_map(map__26905);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26905__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26905__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__26906){
var map__26907 = p__26906;
var map__26907__$1 = cljs.core.__destructure_map(map__26907);
var svc = map__26907__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__26907__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
